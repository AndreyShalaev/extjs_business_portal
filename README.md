## The second version of VP

### Features

* Ruby on Rails framework as backend application;
* Frontend based on ExtJs v4.0.2 (or higher)
* Postgresql database

### Quick start

#### Requirements

* Installed Postgresql v9.1 (or higher)
* Installed Ruby v1.9.3, RVM
* Installed bundler gem

#### Step 1. Git clone and bundle install

``` bash

$ git clone git@git.deeprosoft.com:kb7/vp.git project_folder
$ cd project_folder
$ bundle install --path vendor/bundle

```

#### Step 2. Import database

Dump for import is place in db/ip_development.sql file

#### Step 3. Run application

```bash

# Development environment
$ bundle exec rails s webrick

```

