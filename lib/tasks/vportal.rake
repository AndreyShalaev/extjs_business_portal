# encode: utf-8
namespace :vportal do
  desc "Setup permissions to database"

  task :setup_permissions => :environment do
    Permission.find_each do |p|
      p.destroy
    end

    setup_actions_controllers_db
  end

  def setup_actions_controllers_db

    write_permission('all', 'manage', 'Everything', 'All operations', true)

    arr = []

    controllers = Dir.new("#{Rails.root}/app/controllers").entries

    controllers.each do |controller|
      if controller =~ /_controller/
        arr << controller.camelize.gsub('.rb', '').constantize
      elsif controller =~ /^[a-z]*$/
        Dir.new("#{Rails.root}/app/controllers/#{controller}").entries.each do |c|
          if c =~ /_controller/
            arr << "#{controller.titleize}::#{c.camelize.gsub('.rb', '')}".constantize
          end
        end
      end
    end

    arr.each do |controller|
      if controller.respond_to?(:permission)
        write_permission(controller.permission, 'manage', 'Everything', 'All operations', false)
        controller.action_methods.each do |action|
          if action =~ /^([A-Za-z\d*]+)+([\w]*)+([A-Za-z\d*]+)$/
            action_desc, cancan_action = eval_cancan_action(action)
            write_permission(controller.permission, cancan_action, '', action_desc, false)
          end
        end
      end
    end

  end

  def eval_cancan_action(action)
    case action.to_s
      when "index", "show", "search"
        cancan_action = "read"
        action_desc = I18n.t :read
      when "create", "new"
        cancan_action = "create"
        action_desc = I18n.t :create
      when "edit", "update"
        cancan_action = "update"
        action_desc = I18n.t :edit
      when "delete", "destroy"
        cancan_action = "delete"
        action_desc = I18n.t :delete
      else
        cancan_action = action.to_s
        action_desc = "Other: " << cancan_action
    end
    return action_desc, cancan_action
  end

  # todo: Дописать задачу
  def write_permission(class_name, cancan_action, name, description, force_id_1 = false)

    permission = Permission.by_subject(class_name).by_action(cancan_action).first

    params = {
        :name => name,
        :description => description
    }

    if permission.nil?
      params.merge!({
          :subject_class => class_name,
          :action => cancan_action
      })

      params.merge!(:id => 1) if force_id_1
    end

    Permission.create!(params)
  end
end
