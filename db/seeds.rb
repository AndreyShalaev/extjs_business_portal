# encoding: utf-8
# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Permission.find_each do |p|
  p.destroy
end

# Привилегия комментирования
Permission.create(:name => '', :subject_class => 'Comment', :action => 'manage', :description => 'Возможность оставлять комментарии')
# Привилегия на чтение комментариев
Permission.create(:name => '', :subject_class => 'Comment', :action => 'read', :description => 'Возможность читать комментарии')
# Привилегия на кнопку просмотр всех конкурсов в работе
Permission.create(:name => '', :subject_class => 'Tender', :action => 'show_all', :description => 'Возможность просмотра всех конкурсов в работе')
# Привиления на управление справочниками
Permission.create(:name => '', :subject_class => 'Company', :action => 'manage', :description => 'Возможность управления справочниками')
# Привилегия на создание/удаление/редактирование конкурса
Permission.create(:name => '', :subject_class => 'Tender', :action => 'manage', :description => 'Возможность управления конкурсами (создание, удаление, изменение)')
# Привилегия на принятие/отказ участия в конкурсе
Permission.create(:name => '', :subject_class => 'Tender', :action => 'participiant', :description => 'Возможность участвовать в конкурсе, если компания пользователя ХК')
Permission.create(:name => '', :subject_class => 'Tender', :action => 'move_archive', :description => 'Возможность перемещения конкурса в архив из представления в работе')

