# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20130812101756) do

  create_table "attachments", :force => true do |t|
    t.string   "name"
    t.string   "file_file_name"
    t.string   "file_content_type"
    t.integer  "file_file_size"
    t.datetime "file_updated_at"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
  end

  create_table "audits", :force => true do |t|
    t.integer  "auditable_id"
    t.string   "auditable_type"
    t.integer  "associated_id"
    t.string   "associated_type"
    t.integer  "user_id"
    t.string   "user_type"
    t.string   "username"
    t.string   "action"
    t.text     "audited_changes"
    t.integer  "version",         :default => 0
    t.string   "comment"
    t.string   "remote_address"
    t.datetime "created_at"
  end

  add_index "audits", ["associated_id", "associated_type"], :name => "associated_index"
  add_index "audits", ["auditable_id", "auditable_type"], :name => "auditable_index"
  add_index "audits", ["created_at"], :name => "index_audits_on_created_at"
  add_index "audits", ["user_id", "user_type"], :name => "user_index"

  create_table "comments", :force => true do |t|
    t.string   "title",            :limit => 50, :default => ""
    t.text     "comment"
    t.integer  "commentable_id"
    t.string   "commentable_type"
    t.integer  "user_id"
    t.datetime "created_at",                                     :null => false
    t.datetime "updated_at",                                     :null => false
  end

  add_index "comments", ["commentable_id"], :name => "index_comments_on_commentable_id"
  add_index "comments", ["commentable_type"], :name => "index_comments_on_commentable_type"
  add_index "comments", ["user_id"], :name => "index_comments_on_user_id"

  create_table "companies", :force => true do |t|
    t.string  "name",                                                                        :null => false
    t.string  "short_name", :limit => 140
    t.string  "address"
    t.string  "site"
    t.string  "email"
    t.string  "phone",      :limit => 20
    t.string  "type",       :limit => 15
    t.string  "area_link"
    t.boolean "hk",                                                       :default => false, :null => false
    t.integer "region_id"
    t.text    "activities"
    t.integer "parent_id"
    t.integer "lft"
    t.integer "rgt"
    t.integer "depth"
    t.string  "ts_name",                                                  :default => "",    :null => false
    t.decimal "inn",                       :precision => 15, :scale => 0
    t.integer "org_level",  :limit => 2
    t.boolean "deleted",                                                  :default => false, :null => false
    t.boolean "delta",                                                    :default => true,  :null => false
  end

  add_index "companies", ["deleted"], :name => "index_companies_on_deleted"
  add_index "companies", ["inn"], :name => "index_companies_on_inn"
  add_index "companies", ["lft"], :name => "index_companies_on_lft"
  add_index "companies", ["parent_id"], :name => "parent_id"
  add_index "companies", ["region_id"], :name => "IDX_427C1C7F98260155"
  add_index "companies", ["rgt"], :name => "index_companies_on_rgt"
  add_index "companies", ["ts_name"], :name => "companies_ts_name_index"
  add_index "companies", ["type"], :name => "index_companies_on_type"

  create_table "parties", :force => true do |t|
    t.integer  "tender_id"
    t.integer  "company_id"
    t.decimal  "price",        :precision => 26, :scale => 2
    t.datetime "created_at",                                  :null => false
    t.datetime "updated_at",                                  :null => false
    t.decimal  "price_finish", :precision => 26, :scale => 2
  end

  add_index "parties", ["company_id"], :name => "index_parties_on_company_id"
  add_index "parties", ["tender_id"], :name => "index_parties_on_tender_id"

  create_table "permissions", :force => true do |t|
    t.string  "name"
    t.string  "subject_class"
    t.integer "subject_id"
    t.string  "action"
    t.text    "description"
  end

  add_index "permissions", ["subject_id"], :name => "index_permissions_on_subject_id"

  create_table "provisions", :force => true do |t|
    t.string "name", :null => false
  end

  add_index "provisions", ["name"], :name => "UNIQ_EBBBC70D5E237E06", :unique => true

  create_table "regions", :force => true do |t|
    t.string  "name", :limit => 140, :null => false
    t.integer "code",                :null => false
  end

  create_table "roles", :force => true do |t|
    t.string  "name",        :limit => 140, :null => false
    t.string  "description"
    t.integer "parent_id"
  end

  add_index "roles", ["parent_id"], :name => "IDX_B63E2EC7727ACA70"

  create_table "tender_attachments", :force => true do |t|
    t.integer "tender_id"
    t.integer "attachment_id"
  end

  add_index "tender_attachments", ["attachment_id"], :name => "index_tender_attachments_on_attachment_id"
  add_index "tender_attachments", ["tender_id"], :name => "index_tender_attachments_on_tender_id"

  create_table "tender_contractors", :force => true do |t|
    t.integer  "tender_id"
    t.integer  "company_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "tender_contractors", ["company_id"], :name => "index_tender_contractors_on_contractor_id"
  add_index "tender_contractors", ["tender_id"], :name => "index_tender_contractors_on_tender_id"

  create_table "tenders", :force => true do |t|
    t.integer  "customer_id"
    t.integer  "provision_id"
    t.integer  "winner_id"
    t.text     "subject",                                                                                   :null => false
    t.decimal  "price_limit",                             :precision => 26, :scale => 2
    t.string   "turnaround_time",          :limit => 140
    t.date     "declared_at"
    t.date     "opening_at"
    t.string   "documentation",            :limit => 140
    t.string   "estimates",                :limit => 140
    t.decimal  "price_provision",                         :precision => 26, :scale => 2
    t.string   "requirements"
    t.string   "link"
    t.decimal  "price_cost",                              :precision => 26, :scale => 2
    t.decimal  "price_our",                               :precision => 26, :scale => 2
    t.date     "cost_at"
    t.boolean  "cost_at_green",                                                          :default => false, :null => false
    t.date     "gpr_at"
    t.boolean  "gpr_at_green",                                                           :default => false, :null => false
    t.date     "estimates_at"
    t.boolean  "estimates_at_green",                                                     :default => false, :null => false
    t.date     "ready_at"
    t.boolean  "ready_at_green",                                                         :default => false, :null => false
    t.decimal  "price_winner",                            :precision => 26, :scale => 2
    t.datetime "created_at",                                                                                :null => false
    t.datetime "updated_at",                                                                                :null => false
    t.boolean  "archive",                                                                :default => false, :null => false
    t.integer  "tender_contractors_count",                                               :default => 0,     :null => false
    t.tsvector "fts_subject"
  end

  add_index "tenders", ["archive"], :name => "index_tenders_on_archive"
  add_index "tenders", ["customer_id"], :name => "IDX_D52D2EEE9395C3F3"
  add_index "tenders", ["fts_subject"], :name => "tenders_fts_subject_idx"
  add_index "tenders", ["opening_at"], :name => "index_tenders_on_opening_at"
  add_index "tenders", ["provision_id"], :name => "IDX_D52D2EEE3EC01A31"
  add_index "tenders", ["tender_contractors_count"], :name => "index_tenders_on_tender_contractors_count"
  add_index "tenders", ["winner_id"], :name => "IDX_D52D2EEE5DFCD4B8"

  create_table "user_permissions", :force => true do |t|
    t.integer  "permission_id"
    t.integer  "user_id"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  add_index "user_permissions", ["permission_id"], :name => "index_user_permissions_on_permission_id"
  add_index "user_permissions", ["user_id"], :name => "index_user_permissions_on_user_id"

  create_table "users", :force => true do |t|
    t.string   "username",               :limit => 140,                    :null => false
    t.string   "encrypted_password",                    :default => "",    :null => false
    t.string   "email"
    t.string   "post",                   :limit => 140
    t.string   "phone",                  :limit => 20
    t.string   "own_phone",              :limit => 20
    t.text     "note"
    t.integer  "company_id"
    t.string   "name",                   :limit => 140,                    :null => false
    t.integer  "role_id"
    t.string   "middlename",             :limit => 140,                    :null => false
    t.string   "surname",                :limit => 140,                    :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                         :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.integer  "failed_attempts",                       :default => 0
    t.string   "unlock_token"
    t.datetime "locked_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "legacy_password",                       :default => false, :null => false
    t.datetime "deleted_at"
  end

  add_index "users", ["company_id"], :name => "index_users_on_organization_id"
  add_index "users", ["reset_password_token"], :name => "index_users_on_reset_password_token", :unique => true
  add_index "users", ["role_id"], :name => "index_users_on_role_id"
  add_index "users", ["username"], :name => "index_users_on_username", :unique => true

end
