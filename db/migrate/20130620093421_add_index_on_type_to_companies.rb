class AddIndexOnTypeToCompanies < ActiveRecord::Migration
  def change
    add_index :companies, :type
  end
end
