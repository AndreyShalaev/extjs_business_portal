class CreateTenderContractors < ActiveRecord::Migration
  def self.up
    create_table :tender_contractors do |t|
      t.integer :tender_id
      t.integer :contractor_id

      t.timestamps
    end

    add_index :tender_contractors, :tender_id
    add_index :tender_contractors, :contractor_id
  end

  def self.down
  end
end
