class Cleanup < ActiveRecord::Migration
  def self.up
    drop_table :tender_organization
    drop_table :tenders_team
    drop_table :teams
    drop_table :contact_organizations
    drop_table :ext_log_entries
    drop_table :ext_translations
    drop_table :history
    drop_table :privileges
    drop_table :privileges_groups
    drop_table :privileges_roles
    drop_table :responsible_organizations
    drop_table :Task
    drop_table :Task_ControllerUser
    drop_table :TaskComment
    drop_table :TaskList
    drop_table :TaskState
    drop_table :TaskList_User
    drop_table :files
    drop_table :groups
    drop_table :groups_roles
    drop_table :know_categories
    drop_table :know_files
    drop_table :resources
    drop_table :tenders_comments
    drop_table :tenders_contractors
    drop_table :tenders_files
    drop_table :tenders_journal
    drop_table :tenders_settings
    drop_table :wbs_activity
    drop_table :wbs_codes
    drop_table :wbs_eps
    drop_table :wbs_project
    #drop_table :wbs_activity
    drop_table :wbs_udfvalues
  end

  def self.down
  end
end
