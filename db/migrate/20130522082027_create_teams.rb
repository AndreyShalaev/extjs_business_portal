class CreateTeams < ActiveRecord::Migration
  def self.up
    create_table :teams do |t|
      t.references :tender
      t.references :user
      t.boolean :is_manager, :default => false, :null => false

      t.timestamps
    end

    add_index :teams, :tender_id
    add_index :teams, :user_id
    #add_index :teams, [:tender_id, :is_manager], :unique => true
  end

  def self.down
    drop_table :teams
  end
end
