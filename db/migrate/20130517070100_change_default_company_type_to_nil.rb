class ChangeDefaultCompanyTypeToNil < ActiveRecord::Migration
  def self.up
    change_column :companies, :type, :string, :null => true, :default => nil
  end

  def self.down
    change_column :companies, :type, :string, :null => false, :default => ''
  end
end
