class ChangeDefaultSignInField < ActiveRecord::Migration
  def self.up
    change_column :users, :email, :string, :null => true, :default => nil
  end

  def self.down
    change_column :users, :email, :string, :null => false, :default => ''
  end
end
