class ChangeCompanyHkDefaultValue < ActiveRecord::Migration
  def self.up
    change_column :companies, :hk, :boolean, :null => false, :default => false
  end

  def self.down
  end
end
