class ConvertTenderDatetimeToDateType < ActiveRecord::Migration
  def self.up
    change_column :tenders, :opening_at, :date
    change_column :tenders, :cost_at, :date
    change_column :tenders, :gpr_at, :date
    change_column :tenders, :estimates_at, :date
    change_column :tenders, :ready_at, :date
    execute %q{UPDATE tenders SET opening_at = opening_at + interval '1 day',
              cost_at = cost_at + interval '1 day',
              gpr_at = gpr_at + interval '1 day',
              estimates_at = estimates_at + interval '1 day',
              ready_at = ready_at + interval '1 day';}
  end

  def self.down
    change_column :tenders, :opening_at, :datetime
    change_column :tenders, :cost_at, :datetime
    change_column :tenders, :gpr_at, :datetime
    change_column :tenders, :estimates_at, :datetime
    change_column :tenders, :ready_at, :datetime
    execute %q{UPDATE tenders SET opening_at = opening_at - interval '6 hour',
              cost_at = cost_at - interval '6 hour',
              gpr_at = gpr_at - interval '6 hour',
              estimates_at = estimates_at - interval '6 hour',
              ready_at = ready_at - interval '6 hour';}
  end
end
