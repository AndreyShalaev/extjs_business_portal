class ChangeTenderFields < ActiveRecord::Migration
  def self.up
    change_column :tenders, :cost_at_green, :boolean, :default => false, :null => false
    change_column :tenders, :gpr_at_green, :boolean, :default => false, :null => false
    change_column :tenders, :estimates_at_green, :boolean, :default => false, :null => false
    change_column :tenders, :ready_at_green, :boolean, :default => false, :null => false
  end

  def self.down
  end
end
