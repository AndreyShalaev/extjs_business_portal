class DropTsvectorAndTrigramIndexFromCompanies < ActiveRecord::Migration
  def self.up
    execute %q{ALTER TABLE "companies" DROP COLUMN fts_name CASCADE;}
    execute %q{DROP FUNCTION IF EXISTS companies_vector_update () CASCADE;}
    execute %q{DROP INDEX companies_name_trgm_idx;}
  end

  def self.down
    execute %q{ALTER TABLE "companies" ADD COLUMN fts_name tsvector;}
    execute %{UPDATE "companies" SET fts_name = to_tsvector('ru', COALESCE(name, ''));}
    execute %q{CREATE INDEX companies_fts_name_idx ON "companies" USING gin(fts_name)}
    execute <<-_SQL
      DROP FUNCTION IF EXISTS companies_vector_update () CASCADE;
      CREATE FUNCTION companies_vector_update () RETURNS TRIGGER AS $$
        BEGIN
          IF TG_OP = 'INSERT' THEN
            new.fts_name = to_tsvector('ru', COALESCE(new.name, ''));
          END IF;
          IF TG_OP = 'UPDATE' THEN
            IF NEW.name <> OLD.name THEN
              new.fts_name = to_tsvector('ru', COALESCE(new.name, ''));
            END IF;
          END IF;
          RETURN NEW;
        END
      $$ LANGUAGE 'plpgsql';
    _SQL
    execute <<-_SQL
      CREATE TRIGGER companies_ts_vector_update AFTER INSERT OR UPDATE ON companies FOR EACH ROW
      EXECUTE PROCEDURE companies_vector_update ();
    _SQL
    execute %q{CREATE INDEX companies_name_trgm_idx ON "companies" USING gin(name gin_trgm_ops);}
  end
end
