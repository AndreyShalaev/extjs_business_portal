class DeviseCreateUsers < ActiveRecord::Migration
  def self.up
    rename_column :users, :password, :encrypted_password
    change_column :users, :encrypted_password, :string, :null => false, :default => ''

    change_column :users, :email, :string, :null => false, :default => ''

    add_column :users, :reset_password_token, :string
    add_column :users, :reset_password_sent_at, :datetime

    add_column :users, :remember_created_at, :datetime

    add_column :users, :sign_in_count, :integer, :default => 0
    add_column :users, :current_sign_in_at, :datetime
    add_column :users, :last_sign_in_at, :datetime
    add_column :users, :current_sign_in_ip, :string
    add_column :users, :last_sign_in_ip, :string

    ## Lockable
    add_column :users, :failed_attempts, :integer, :default => 0
    add_column :users, :unlock_token, :string
    add_column :users, :locked_at, :datetime

    add_column :users, :created_at, :datetime
    add_column :users, :updated_at, :datetime

    add_index :users, :reset_password_token, :unique => true

    remove_index :users, :name => 'UNIQ_1483A5E9F85E0677'
    add_index :users, :username, :unique => true

    remove_index :users, :name => 'IDX_1483A5E932C8A3DE'
    add_index :users, :organization_id

    remove_index :users, :name => 'role_id'
    add_index :users, :role_id
  end

  def self.down
    rename_column :users, :encrypted_password, :password
    change_column :users, :password, :string, :null => true, :default => nil

    change_column :users, :email, :string, :null => true, :default => nil

    remove_column :users, :reset_password_token
    remove_column :users, :reset_password_sent_at

    remove_column :users, :remember_created_at

    remove_column :users, :sign_in_count
    remove_column :users, :current_sign_in_at
    remove_column :users, :last_sign_in_at
    remove_column :users, :current_sign_in_ip
    remove_column :users, :last_sign_in_ip

    ## Lockable
    remove_column :users, :failed_attempts
    remove_column :users, :unlock_token
    remove_column :users, :locked_at

    remove_column :users, :created_at
    remove_column :users, :updated_at
  end
end
