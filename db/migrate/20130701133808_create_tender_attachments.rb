class CreateTenderAttachments < ActiveRecord::Migration
  def self.up
    create_table :tender_attachments do |t|
      t.references :tender
      t.references :attachment
    end

    add_index :tender_attachments, :tender_id
    add_index :tender_attachments, :attachment_id
  end

  def self.down
    drop_table :tender_attachments
  end
end
