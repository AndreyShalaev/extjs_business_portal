class AddContractorsCounterToTenders < ActiveRecord::Migration
  def self.up
    add_column :tenders, :tender_contractors_count, :integer, :default => 0, :null => false

    Tender.reset_column_information
    Tender.find_each do |t|
      Tender.update_counters t.id, :tender_contractors_count => t.tender_contractors.length
    end
  end

  def self.down
    remove_column :tenders, :tender_contractors_count
  end
end
