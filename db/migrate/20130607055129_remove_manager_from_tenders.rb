class RemoveManagerFromTenders < ActiveRecord::Migration
  def self.up
    remove_column :tenders, :manager_id
  end

  def self.down
    add_column :tenders, :manager_id, :integer
  end
end
