class RenameOrganizationsToCompany < ActiveRecord::Migration
  def self.up
    rename_table :organizations, :companies
  end

  def self.down
    rename_table :companies, :organizations
  end
end
