class TenderReasonsMoveToComments < ActiveRecord::Migration
  def self.up
    default_user = User.find_by_username('admin')

    if default_user.present?
      execute <<-_SQL
        INSERT INTO comments (comment, commentable_type, commentable_id, user_id, created_at, updated_at)
        SELECT reasons AS comment,
              'Tender' AS commentable_type,
              id AS commentable_id,
              '#{default_user.id}' AS user_id,
              created_at,
              updated_at
        FROM tenders WHERE reasons IS NOT NULL AND reasons <> '';
      _SQL
    end

    remove_column :tenders, :reasons
  end

  def self.down
    add_column :tenders, :reasons, :text
  end
end
