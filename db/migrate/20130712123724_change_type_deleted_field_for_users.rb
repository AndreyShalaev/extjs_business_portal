class ChangeTypeDeletedFieldForUsers < ActiveRecord::Migration
  def self.up
    remove_column :users, :deleted
    add_column :users, :deleted_at, :datetime, :default => nil
  end

  def self.down
    remove_column :users, :deleted_at
    add_column :users, :deleted, :boolean, :default => false, :null => false
  end
end
