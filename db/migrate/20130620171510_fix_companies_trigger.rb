class FixCompaniesTrigger < ActiveRecord::Migration
  def self.up
    execute <<-_SQL
      DROP FUNCTION IF EXISTS companies_ts_name_update () CASCADE;
      CREATE FUNCTION companies_ts_name_update () RETURNS TRIGGER AS $$
        BEGIN
          IF TG_OP = 'INSERT' THEN
            UPDATE "companies" SET ts_name = regexp_replace(new.name, '["«»\”\“]', '', 'g') WHERE id = new.id;
          END IF;
          IF TG_OP = 'UPDATE' THEN
            IF NEW.name <> OLD.name THEN
              UPDATE "companies" SET ts_name = regexp_replace(new.name, '["«»\”\“]', '', 'g') WHERE id = new.id;
            END IF;
          END IF;
          RETURN NEW;
        END
      $$ LANGUAGE 'plpgsql';
    _SQL
    execute <<-_SQL
      CREATE TRIGGER companies_ts_name_update AFTER INSERT OR UPDATE ON companies FOR EACH ROW
      EXECUTE PROCEDURE companies_ts_name_update ();
    _SQL
    execute %q{UPDATE "companies" SET ts_name = regexp_replace(name, '["«»\”\“]', '', 'g');}
  end

  def down
  end
end
