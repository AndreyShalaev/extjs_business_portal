class AddLegacyPasswordToUsers < ActiveRecord::Migration
  def self.up
    add_column :users, :legacy_password, :boolean, :null => false, :default => false

    execute %q{UPDATE "users" SET legacy_password = '1';}
  end

  def self.down
    remove_column :users, :legacy_password
  end
end
