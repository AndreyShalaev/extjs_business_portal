class ChangeTenderContractors < ActiveRecord::Migration
  def self.up
    rename_column :tender_contractors, :contractor_id, :company_id
  end

  def self.down
    rename_column :tender_contractors, :company_id, :contractor_id
  end
end
