class AddLevelColumnToCompanies < ActiveRecord::Migration
  def self.up

    execute %q{UPDATE "companies" SET inn = '0' WHERE inn IS NULL;}
    execute %q{UPDATE "companies" SET inn = '0' WHERE inn = '';}
    execute %q{UPDATE "companies" SET inn = regexp_replace(inn, '[^0-9]', '', 'g');}

    add_column :companies, :inn_new, :decimal, :null => false, :default => 0, :precision => 15, :scale => 0

    execute %q{UPDATE companies SET type = 'Customer' WHERE type = 'Заказчик';}
    execute %q{UPDATE companies SET type = 'Contractor' WHERE type = 'Подрядчик';}
    execute %q{UPDATE companies SET type = NULL WHERE type = 'Заказчик, Подрядчик';}

    Company.reset_column_information
    Company.find_each do |c|
      inn_new = 0
      inn_new = c.inn.to_i unless c.inn == ''
      execute "UPDATE companies SET inn_new = #{inn_new} WHERE id = #{c.id};"
    end

    remove_column :companies, :inn
    rename_column :companies, :inn_new, :inn
    add_index :companies, :inn

    change_column :companies, :type, :string, :limit => 15
    add_column :companies, :level, :integer, :null => false, :default => 0, :limit => 1

    execute "UPDATE companies SET level = 1 WHERE organization_structure = 'Level 1';"
    execute "UPDATE companies SET level = 2 WHERE organization_structure = 'Level 2';"

    remove_column :companies, :organization_structure
  end

  def self.down
    change_column :companies, :inn, :string, :null => false, :default => '', :limit => 15
    change_column :companies, :type, :string, :limit => 255

    add_column :companies, :organization_structure, :string, :null => false, :default => ''

    execute "UPDATE companies SET organization_structure = 'Level 1' WHERE level = 1;"
    execute "UPDATE companies SET organization_structure = 'Level 2' WHERE level = 2;"

    remove_column :companies, :level
  end
end
