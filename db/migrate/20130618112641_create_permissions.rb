class CreatePermissions < ActiveRecord::Migration
  def self.up
    create_table :permissions do |t|
      t.integer :user_id
      t.string :name
      t.string :subject_class
      t.integer :subject_id
      t.string :action
      t.text :description
    end

    add_index :permissions, :user_id
    add_index :permissions, :subject_id
  end

  def self.down
    drop_table :permissions
  end
end
