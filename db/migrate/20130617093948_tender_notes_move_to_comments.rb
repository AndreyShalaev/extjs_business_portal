class TenderNotesMoveToComments < ActiveRecord::Migration
  def self.up
    default_user = User.find_by_username('admin')

    if default_user.present?
      execute <<-_SQL
        INSERT INTO comments (comment, commentable_type, commentable_id, user_id, created_at, updated_at)
        SELECT note AS comment,
              'Tender' AS commentable_type,
              id AS commentable_id,
              '#{default_user.id}' AS user_id, created_at, updated_at
        FROM tenders WHERE note IS NOT NULL AND note <> '';
      _SQL
    end
  end

  def self.down
    execute %q{DELETE FROM comments WHERE commentable_type = 'Tender';}
  end
end
