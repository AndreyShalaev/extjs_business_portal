class RemoveNoteFromTenders < ActiveRecord::Migration
  def self.up
    remove_column :tenders, :note
  end

  def self.down
    add_column :tenders, :note, :text
    Tender.reset_column_information
    Tender.find_each do |t|
      comment = t.comments.last
      if comment
        t.update_attributes!(:note => comment.comment)
        comment.destroy
      end
    end
  end
end
