class ChangeDeclaredAtToDateForTenders < ActiveRecord::Migration
  def self.up
    change_column :tenders, :declared_at, :date
    execute %q{UPDATE tenders SET declared_at = declared_at + interval '1 day';}
  end

  def self.down
    change_column :tenders, :declared_at, :datetime
    execute %q{UPDATE tenders SET declared_at = declared_at - interval '6 hour';}
  end
end
