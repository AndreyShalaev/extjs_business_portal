class AddTsNameToCompanies < ActiveRecord::Migration
  def self.up
    add_column :companies, :ts_name, :string, :null => false, :default => ''

    execute <<-_SQL
      DROP FUNCTION IF EXISTS companies_ts_name_update () CASCADE;
      CREATE FUNCTION companies_ts_name_update () RETURNS TRIGGER AS $$
        BEGIN
          IF TG_OP = 'INSERT' THEN
            new.ts_name = regexp_replace(new.name, '["«»\”\“]', '', 'g');
          END IF;
          IF TG_OP = 'UPDATE' THEN
            IF NEW.name <> OLD.name THEN
              new.ts_name = regexp_replace(new.name, '["«»\”\“]', '', 'g');
            END IF;
          END IF;
          RETURN NEW;
        END
      $$ LANGUAGE 'plpgsql';
    _SQL
    execute <<-_SQL
      CREATE TRIGGER companies_ts_name_update AFTER INSERT OR UPDATE ON companies FOR EACH ROW
      EXECUTE PROCEDURE companies_ts_name_update ();
    _SQL
    execute %q{CREATE INDEX companies_ts_name_index ON "companies" USING gist(ts_name gist_trgm_ops);}
    execute %q{UPDATE "companies" SET ts_name = regexp_replace(name, '["«»\”\“]', '', 'g');}
  end

  def self.down
    execute %q{DROP FUNCTION IF EXISTS companies_ts_name_update () CASCADE;}
    remove_column :companies, :ts_name
  end
end
