class AddArchiveIndexToTenders < ActiveRecord::Migration
  def self.up
    add_index :tenders, :archive
    add_index :tenders, :tender_contractors_count
  end

  def self.down
    remove_index :tenders, :archive
    remove_index :tenders, :tender_contractors_count
  end
end
