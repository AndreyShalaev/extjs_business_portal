class FixTendersTrigger < ActiveRecord::Migration
  def self.up
    execute %q{DROP FUNCTION IF EXISTS tenders_vector_update () CASCADE;}
    execute <<-_SQL
      DROP FUNCTION IF EXISTS tenders_vector_update () CASCADE;
      CREATE FUNCTION tenders_vector_update () RETURNS TRIGGER AS $$
        BEGIN
          IF TG_OP = 'INSERT' THEN
            new.fts_subject = to_tsvector('ru', COALESCE(new.subject, ''));
          END IF;
          IF TG_OP = 'UPDATE' THEN
            IF NEW.subject <> OLD.subject THEN
              new.fts_subject = to_tsvector('ru', COALESCE(new.subject, ''));
            END IF;
          END IF;
          RETURN NEW;
        END
      $$ LANGUAGE 'plpgsql';
    _SQL
    execute <<-_SQL
      CREATE TRIGGER tenders_ts_vector_update AFTER INSERT OR UPDATE ON tenders FOR EACH ROW
      EXECUTE PROCEDURE tenders_vector_update ();
    _SQL
  end

  def self.down
  end
end
