class AddTsvectorToTenders < ActiveRecord::Migration
  def self.up
    execute %q{ALTER TABLE "tenders" ADD COLUMN fts_subject tsvector;}
    execute %{UPDATE "tenders" SET fts_subject = to_tsvector('ru', COALESCE(subject, ''));}
    execute %q{CREATE INDEX tenders_fts_subject_idx ON "tenders" USING gist(fts_subject)}
    execute <<-_SQL
      DROP FUNCTION IF EXISTS tenders_vector_update () CASCADE;
      CREATE FUNCTION tenders_vector_update () RETURNS TRIGGER AS $$
        BEGIN
          IF TG_OP = 'INSERT' THEN
            new.fts_subject = to_tsvector('ru', COALESCE(new.subject, ''));
          END IF;
          IF TG_OP = 'UPDATE' THEN
            IF NEW.name <> OLD.name THEN
              new.fts_subject = to_tsvector('ru', COALESCE(new.subject, ''));
            END IF;
          END IF;
          RETURN NEW;
        END
      $$ LANGUAGE 'plpgsql';
    _SQL
    execute <<-_SQL
      CREATE TRIGGER tenders_ts_vector_update AFTER INSERT OR UPDATE ON tenders FOR EACH ROW
      EXECUTE PROCEDURE tenders_vector_update ();
    _SQL
  end

  def self.down
    execute %q{ALTER TABLE "tenders" DROP COLUMN fts_subject CASCADE;}
    execute %q{DROP FUNCTION IF EXISTS tenders_vector_update () CASCADE;}
  end
end
