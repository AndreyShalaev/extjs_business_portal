class AddTrigramSearchToCompanies < ActiveRecord::Migration
  def self.up
    execute %q{CREATE EXTENSION pg_trgm;}
    execute %q{CREATE INDEX companies_name_trgm_idx ON "companies" USING gin(name gin_trgm_ops);}
  end

  def self.down
    execute %q{DROP INDEX companies_name_trgm_idx;}
    execute %q{DROP EXTENSION pg_trgm;}
  end
end
