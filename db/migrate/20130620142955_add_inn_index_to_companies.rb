class AddInnIndexToCompanies < ActiveRecord::Migration
  def change
    add_index :companies, :inn
  end
end
