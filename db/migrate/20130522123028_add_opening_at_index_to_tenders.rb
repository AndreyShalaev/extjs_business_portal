class AddOpeningAtIndexToTenders < ActiveRecord::Migration
  def self.up
    add_index :tenders, :opening_at
  end

  def self.down
    remove_index :tenders, :opening_at
  end
end
