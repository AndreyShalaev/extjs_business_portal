class ChangeCompanyColumns < ActiveRecord::Migration
  def self.up
    remove_column :companies, :group_id
    drop_table :group_organizations
  end

  def self.down
    add_column :companies, :group_id, :integer
    create_table :group_organizations do |t|
      t.string :name
    end
  end
end
