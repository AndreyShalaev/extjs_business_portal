class CreateDatabase < ActiveRecord::Migration
  def self.up
  	create_table "Task", :force => true do |t|
	    t.integer  "executant_user_id"
	    t.datetime "create_date",                              :null => false
	    t.text     "title",              :limit => 2147483647, :null => false
	    t.text     "result",             :limit => 2147483647, :null => false
	    t.integer  "list_id"
	    t.datetime "plan_date"
	    t.datetime "fact_date"
	    t.datetime "reminder"
	    t.integer  "deviation"
	    t.integer  "task_state_id"
	    t.integer  "producer_user_id"
	    t.integer  "controller_user_id"
	  end

	  add_index "Task", ["controller_user_id"], :name => "IDX_F24C741B4059EE54"
	  add_index "Task", ["executant_user_id"], :name => "IDX_F24C741BA67C408C"
	  add_index "Task", ["list_id"], :name => "IDX_F24C741B3DAE168B"
	  add_index "Task", ["producer_user_id"], :name => "IDX_F24C741BA344D75D"
	  add_index "Task", ["task_state_id"], :name => "IDX_F24C741B4518D68D"

	  create_table "TaskComment", :force => true do |t|
	    t.integer  "task_id"
	    t.integer  "user_id"
	    t.datetime "create_date"
	    t.text     "note",        :limit => 2147483647, :null => false
	    t.text     "file",        :limit => 2147483647
	  end

	  add_index "TaskComment", ["task_id"], :name => "IDX_1FCF87778DB60186"
	  add_index "TaskComment", ["user_id"], :name => "IDX_1FCF8777A76ED395"

	  create_table "TaskList", :force => true do |t|
	    t.integer "parentId"
	    t.string  "name",     :null => false
	    t.boolean "leaf",     :null => false
	  end

	  add_index "TaskList", ["parentId"], :name => "IDX_3F65D40D10EE4CEE"

	  create_table "TaskList_User", :id => false, :force => true do |t|
	    t.integer "userId",     :null => false
	    t.integer "taskListId", :null => false
	  end

	  add_index "TaskList_User", ["taskListId"], :name => "IDX_37E0C0E451F017C7"
	  add_index "TaskList_User", ["userId"], :name => "IDX_37E0C0E464B64DCC"

	  create_table "TaskState", :force => true do |t|
	    t.string "name", :null => false
	  end

	  create_table "Task_ControllerUser", :id => false, :force => true do |t|
	    t.integer "task_id", :null => false
	    t.integer "user_id", :null => false
	  end

	  add_index "Task_ControllerUser", ["task_id"], :name => "IDX_1D2F39C28DB60186"
	  add_index "Task_ControllerUser", ["user_id"], :name => "IDX_1D2F39C2A76ED395"

	  create_table "contact_organizations", :id => false, :force => true do |t|
	    t.integer "user_id",         :null => false
	    t.integer "organization_id", :null => false
	  end

	  add_index "contact_organizations", ["organization_id"], :name => "IDX_63E45FF832C8A3DE"
	  add_index "contact_organizations", ["user_id"], :name => "IDX_63E45FF8A76ED395"

	  create_table "ext_log_entries", :force => true do |t|
	    t.string   "action",       :limit => 8,          :null => false
	    t.datetime "logged_at",                          :null => false
	    t.string   "object_id",    :limit => 32
	    t.string   "object_class",                       :null => false
	    t.integer  "version",                            :null => false
	    t.text     "data",         :limit => 2147483647
	    t.string   "username"
	  end

	  add_index "ext_log_entries", ["logged_at"], :name => "log_date_lookup_idx"
	  add_index "ext_log_entries", ["object_class"], :name => "log_class_lookup_idx"
	  add_index "ext_log_entries", ["username"], :name => "log_user_lookup_idx"

	  create_table "ext_translations", :force => true do |t|
	    t.string "locale",       :limit => 8,          :null => false
	    t.string "object_class",                       :null => false
	    t.string "field",        :limit => 32,         :null => false
	    t.string "foreign_key",  :limit => 64,         :null => false
	    t.text   "content",      :limit => 2147483647
	  end

	  add_index "ext_translations", ["locale", "object_class", "foreign_key", "field"], :name => "lookup_unique_idx", :unique => true
	  add_index "ext_translations", ["locale", "object_class", "foreign_key"], :name => "translations_lookup_idx"

	  create_table "files", :force => true do |t|
	    t.string "name", :null => false
	    t.string "path"
	  end

	  create_table "group_organizations", :force => true do |t|
	    t.string "name", :limit => 140, :null => false
	  end

	  create_table "groups", :force => true do |t|
	    t.string "name", :limit => 140, :null => false
	  end

	  create_table "groups_roles", :id => false, :force => true do |t|
	    t.integer "group_id", :null => false
	    t.integer "role_id",  :null => false
	  end

	  add_index "groups_roles", ["group_id"], :name => "IDX_E79D4963FE54D947"
	  add_index "groups_roles", ["role_id"], :name => "IDX_E79D4963D60322AC"

	  create_table "history", :force => true do |t|
	    t.integer  "user_id"
	    t.integer  "organization_id"
	    t.datetime "created",                               :null => false
	    t.datetime "updated",                               :null => false
	    t.string   "communication",                         :null => false
	    t.text     "description",     :limit => 2147483647, :null => false
	    t.text     "result",          :limit => 2147483647
	    t.text     "note",            :limit => 2147483647
	  end

	  add_index "history", ["organization_id"], :name => "IDX_27BA704B32C8A3DE"
	  add_index "history", ["user_id"], :name => "IDX_27BA704BA76ED395"

	  create_table "know_categories", :force => true do |t|
	    t.integer "parent_id"
	    t.string  "name",      :limit => 64,  :null => false
	    t.integer "lft",                      :null => false
	    t.integer "rgt",                      :null => false
	    t.integer "root"
	    t.integer "lvl",                      :null => false
	    t.text    "path",      :limit => 255, :null => false
	  end

	  add_index "know_categories", ["parent_id"], :name => "IDX_47315ABF727ACA70"

	  create_table "know_files", :force => true do |t|
	    t.integer "category_id"
	    t.string  "name",        :limit => 64, :null => false
	    t.string  "link",        :limit => 64, :null => false
	    t.integer "size",                      :null => false
	  end

	  add_index "know_files", ["category_id"], :name => "IDX_43E79C1312469DE2"

	  create_table "organizations", :force => true do |t|
	    t.string  "name",                                         :null => false
	    t.string  "short_name",             :limit => 140
	    t.string  "address"
	    t.string  "site"
	    t.string  "email"
	    t.string  "phone",                  :limit => 12
	    t.string  "inn"
	    t.string  "type"
	    t.string  "area_link"
	    t.boolean "hk",                                           :null => false
	    t.integer "group_id"
	    t.integer "region_id"
	    t.text    "activities",             :limit => 2147483647
	    t.integer "parent_id"
	    t.string  "organization_structure"
	  end

	  add_index "organizations", ["group_id"], :name => "IDX_427C1C7FFE54D947"
	  add_index "organizations", ["parent_id"], :name => "parent_id"
	  add_index "organizations", ["region_id"], :name => "IDX_427C1C7F98260155"

	  create_table "party", :id => false, :force => true do |t|
	    t.integer "tender_id",                                      :null => false
	    t.integer "organization_id",                                :null => false
	    t.decimal "price",           :precision => 26, :scale => 2
	  end

	  add_index "party", ["organization_id"], :name => "IDX_89954EE032C8A3DE"
	  add_index "party", ["tender_id"], :name => "IDX_89954EE09245DE54"

	  create_table "privileges", :force => true do |t|
	    t.integer "resource_id"
	    t.string  "name",        :limit => 140, :null => false
	    t.string  "description"
	  end

	  add_index "privileges", ["resource_id"], :name => "IDX_6855F91289329D25"

	  create_table "privileges_groups", :id => false, :force => true do |t|
	    t.integer "privilege_id", :null => false
	    t.integer "group_id",     :null => false
	  end

	  add_index "privileges_groups", ["group_id"], :name => "IDX_C5E38492FE54D947"
	  add_index "privileges_groups", ["privilege_id"], :name => "IDX_C5E3849232FB8AEA"

	  create_table "privileges_roles", :id => false, :force => true do |t|
	    t.integer "privilege_id", :null => false
	    t.integer "role_id",      :null => false
	  end

	  add_index "privileges_roles", ["privilege_id"], :name => "IDX_8D2B36EF32FB8AEA"
	  add_index "privileges_roles", ["role_id"], :name => "IDX_8D2B36EFD60322AC"

	  create_table "provisions", :force => true do |t|
	    t.string "name", :null => false
	  end

	  add_index "provisions", ["name"], :name => "UNIQ_EBBBC70D5E237E06", :unique => true

	  create_table "regions", :force => true do |t|
	    t.string  "name", :limit => 140, :null => false
	    t.integer "code",                :null => false
	  end

	  create_table "resources", :force => true do |t|
	    t.string "name",        :limit => 140, :null => false
	    t.string "description"
	  end

	  create_table "responsible_organizations", :id => false, :force => true do |t|
	    t.integer "user_id",         :null => false
	    t.integer "organization_id", :null => false
	  end

	  add_index "responsible_organizations", ["organization_id"], :name => "IDX_280E989532C8A3DE"
	  add_index "responsible_organizations", ["user_id"], :name => "IDX_280E9895A76ED395"

	  create_table "roles", :force => true do |t|
	    t.string  "name",        :limit => 140, :null => false
	    t.string  "description"
	    t.integer "parent_id"
	  end

	  add_index "roles", ["parent_id"], :name => "IDX_B63E2EC7727ACA70"

	  create_table "tender_organization", :id => false, :force => true do |t|
	    t.integer "tender_id",       :null => false
	    t.integer "organization_id", :null => false
	  end

	  add_index "tender_organization", ["organization_id"], :name => "IDX_7957694332C8A3DE"
	  add_index "tender_organization", ["tender_id"], :name => "IDX_795769439245DE54"

	  create_table "tenders", :force => true do |t|
	    t.integer  "customer_id"
	    t.integer  "provision_id"
	    t.integer  "winner_id"
	    t.integer  "manager_id"
	    t.text     "subject",                                                                 :null => false
	    t.decimal  "price_limit",                              :precision => 26, :scale => 2
	    t.string   "turnaround_time",    :limit => 140
	    t.datetime "declared_at"
	    t.datetime "opening_at"
	    t.string   "documentation",      :limit => 140
	    t.string   "estimates",          :limit => 140
	    t.decimal  "price_provision",                          :precision => 26, :scale => 2
	    t.string   "requirements"
	    t.string   "link"
	    t.text     "reasons",            :limit => 2147483647
	    t.text     "note",               :limit => 2147483647
	    t.decimal  "price_cost",                               :precision => 26, :scale => 2
	    t.decimal  "price_our",                                :precision => 26, :scale => 2
	    t.datetime "cost_at"
	    t.boolean  "cost_at_green",                                                           :null => false
	    t.datetime "gpr_at"
	    t.boolean  "gpr_at_green",                                                            :null => false
	    t.datetime "estimates_at"
	    t.boolean  "estimates_at_green",                                                      :null => false
	    t.datetime "ready_at"
	    t.boolean  "ready_at_green",                                                          :null => false
	    t.decimal  "price_winner",                             :precision => 26, :scale => 2
	    t.datetime "created_at",                                                              :null => false
	    t.datetime "updated_at",                                                              :null => false
	  end

	  add_index "tenders", ["customer_id"], :name => "IDX_D52D2EEE9395C3F3"
	  add_index "tenders", ["manager_id"], :name => "IDX_D52D2EEE783E3463"
	  add_index "tenders", ["provision_id"], :name => "IDX_D52D2EEE3EC01A31"
	  add_index "tenders", ["winner_id"], :name => "IDX_D52D2EEE5DFCD4B8"

	  create_table "tenders_comments", :force => true do |t|
	    t.integer  "tender_id"
	    t.integer  "user_id"
	    t.text     "body",       :null => false
	    t.datetime "created_at", :null => false
	  end

	  add_index "tenders_comments", ["tender_id"], :name => "tenders_comments_tender_id"
	  add_index "tenders_comments", ["user_id"], :name => "tenders_comments_user_id"

	  create_table "tenders_contractors", :id => false, :force => true do |t|
	    t.integer "tender_id",       :null => false
	    t.integer "organization_id", :null => false
	  end

	  add_index "tenders_contractors", ["organization_id"], :name => "IDX_E43801FF32C8A3DE"
	  add_index "tenders_contractors", ["tender_id"], :name => "IDX_E43801FF9245DE54"

	  create_table "tenders_files", :id => false, :force => true do |t|
	    t.integer "tender_id",   :null => false
	    t.integer "document_id", :null => false
	  end

	  add_index "tenders_files", ["document_id"], :name => "IDX_7A8C76AC33F7837"
	  add_index "tenders_files", ["tender_id"], :name => "IDX_7A8C76A9245DE54"

	  create_table "tenders_journal", :force => true do |t|
	    t.integer  "user_id"
	    t.integer  "tender_id"
	    t.string   "event",     :null => false
	    t.datetime "created",   :null => false
	  end

	  add_index "tenders_journal", ["tender_id"], :name => "IDX_461095639245DE54"
	  add_index "tenders_journal", ["user_id"], :name => "IDX_46109563A76ED395"

	  create_table "tenders_settings", :force => true do |t|
	    t.string "set_key",     :null => false
	    t.string "set_value",   :null => false
	    t.string "description"
	  end

	  create_table "tenders_team", :id => false, :force => true do |t|
	    t.integer "tender_id", :null => false
	    t.integer "user_id",   :null => false
	  end

	  add_index "tenders_team", ["tender_id"], :name => "IDX_8216935E9245DE54"
	  add_index "tenders_team", ["user_id"], :name => "IDX_8216935EA76ED395"

	  create_table "users", :force => true do |t|
	    t.string  "username",        :limit => 140,        :null => false
	    t.string  "password",                              :null => false
	    t.string  "email",           :limit => 140
	    t.string  "post",            :limit => 140
	    t.string  "phone",           :limit => 12
	    t.string  "own_phone",       :limit => 12
	    t.text    "note",            :limit => 2147483647
	    t.integer "organization_id"
	    t.string  "name",            :limit => 140,        :null => false
	    t.integer "role_id"
	    t.string  "middlename",      :limit => 140,        :null => false
	    t.string  "surname",         :limit => 140,        :null => false
	  end

	  add_index "users", ["organization_id"], :name => "IDX_1483A5E932C8A3DE"
	  add_index "users", ["role_id"], :name => "role_id"
	  add_index "users", ["username"], :name => "UNIQ_1483A5E9F85E0677", :unique => true

	  create_table "wbs_activity", :id => false, :force => true do |t|
	    t.string "id",            :limit => 50,  :null => false
	    t.string "obj_id",        :limit => 50,  :null => false
	    t.string "proj_obj_id",   :limit => 50,  :null => false
	    t.string "name",          :limit => 120, :null => false
	    t.string "start",         :limit => 50
	    t.string "finish",        :limit => 50
	    t.string "status",        :limit => 50
	    t.string "phys_complite", :limit => 50
	    t.string "bl_start",      :limit => 50
	    t.string "bl_finish",     :limit => 50
	  end

	  create_table "wbs_codes", :id => false, :force => true do |t|
	    t.string "act_obj_id", :limit => 50, :null => false
	    t.string "col_name",   :limit => 50, :null => false
	    t.string "value",      :limit => 50, :null => false
	  end

	  create_table "wbs_eps", :id => false, :force => true do |t|
	    t.string "id",         :limit => 50
	    t.string "name",       :limit => 50
	    t.string "obj_id",     :limit => 50, :null => false
	    t.string "par_obj_id", :limit => 50
	  end

	  create_table "wbs_project", :id => false, :force => true do |t|
	    t.string "id",          :limit => 50,  :null => false
	    t.string "obj_id",      :limit => 50,  :null => false
	    t.string "name",        :limit => 120
	    t.string "start",       :limit => 50
	    t.string "finish",      :limit => 50
	    t.string "must_finish", :limit => 50
	    t.string "data_date",   :limit => 50
	    t.string "obs_name",    :limit => 50
	    t.string "status",      :limit => 50
	    t.string "eps_id",      :limit => 50
	  end

	  create_table "wbs_udfvalues", :id => false, :force => true do |t|
	    t.string "value",          :limit => 50
	    t.string "col_name",       :limit => 50
	    t.string "subj_name",      :limit => 50
	    t.string "foreing_obj_id", :limit => 50
	    t.string "type_obj_id",    :limit => 50
    end

    # Создаем словари
    execute <<-_SQL
      CREATE TEXT SEARCH DICTIONARY russian_ispell (
        TEMPLATE = ispell,
        DictFile = russian,
        AffFile = russian,
        StopWords = russian
      );

      CREATE TEXT SEARCH CONFIGURATION ru ( COPY = russian );
      ALTER TEXT SEARCH CONFIGURATION ru ALTER MAPPING FOR hword, hword_part, word WITH russian_ispell, russian_stem;

    _SQL
  end

  def self.down
  end
end
