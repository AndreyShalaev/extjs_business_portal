class AddStiToCompanies < ActiveRecord::Migration
  def self.up
    change_column :companies, :type, :string, :null => false, :default => ''
  end

  def self.down
    change_column :companies, :type, :string, :null => true, :default => nil
  end
end
