class CreateParties < ActiveRecord::Migration
  def self.up
    drop_table :party

    create_table :parties do |t|

      t.references :tender
      t.references :company
      t.decimal :price, :precision => 26, :scale => 2
      t.timestamps
    end

    add_index :parties, :company_id
    add_index :parties, :tender_id
  end

  def self.down
    drop_table :parties
  end
end
