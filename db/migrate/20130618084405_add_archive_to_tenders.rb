class AddArchiveToTenders < ActiveRecord::Migration
  def self.up
    add_column :tenders, :archive, :boolean, :null => false, :default => false
  end

  def self.down
    remove_column :tenders, :archive
  end
end
