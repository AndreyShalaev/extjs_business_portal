class ChangeOrgLevelColumnInCompanies < ActiveRecord::Migration
  def self.up
    change_column :companies, :org_level, :integer, :null => true, :default => nil, :limit => 1
    execute %q{UPDATE "companies" SET org_level = NULL WHERE org_level = 0;}
  end

  def self.down
    execute %q{UPDATE "companies" SET org_level = 0 WHERE org_level IS NULL;}
    change_column :companies, :org_level, :integer, :null => false, :default => 0, :limit => 1
  end
end
