class AddNestedToCompanies < ActiveRecord::Migration
  def self.up
    add_column :companies, :lft, :integer
    add_column :companies, :rgt, :integer
    add_column :companies, :depth, :integer

    add_index :companies, :lft
    add_index :companies, :rgt
  end

  def self.down
    remove_column :companies, :lft
    remove_column :companies, :rgt
    remove_column :companies, :depth
  end
end
