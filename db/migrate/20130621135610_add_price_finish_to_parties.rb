class AddPriceFinishToParties < ActiveRecord::Migration
  def self.up
    add_column :parties, :price_finish, :decimal, :precision => 26, :scale => 2
  end

  def self.down
    remove_column :parties, :price_finish
  end
end
