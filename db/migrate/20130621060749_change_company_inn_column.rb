class ChangeCompanyInnColumn < ActiveRecord::Migration
  def self.up
    change_column :companies, :inn, :decimal, :null => true, :default => nil, :precision => 15, :scale => 0
    execute %q{UPDATE "companies" SET inn = NULL WHERE inn = 0;}
  end

  def self.down
    execute %q{UPDATE "companies" SET inn = 0 WHERE inn IS NULL;}
    change_column :companies, :inn, :decimal, :null => false, :default => 0, :precision => 15, :scale => 0
  end
end
