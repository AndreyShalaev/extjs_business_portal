class RemoveUserFromPermissions < ActiveRecord::Migration
  def self.up
    remove_column :permissions, :user_id
  end

  def self.down
    add_column :permissions, :user_id, :integer
    add_index :permissions, :user_id
  end
end
