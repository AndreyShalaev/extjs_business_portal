class ChangeLimitCompanyPhone < ActiveRecord::Migration
  def self.up
    change_column :companies, :phone, :string, :null => true, :default => nil, :limit => 20
    change_column :users, :phone,  :string, :null => true, :default => nil, :limit => 20
    change_column :users, :own_phone,  :string, :null => true, :default => nil, :limit => 20
  end

  def self.down
  end
end
