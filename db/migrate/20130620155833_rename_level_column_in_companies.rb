class RenameLevelColumnInCompanies < ActiveRecord::Migration
  def self.up
    rename_column :companies, :level, :org_level
  end

  def self.down
    rename_column :companies, :org_level, :level
  end
end
