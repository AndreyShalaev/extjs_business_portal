class ChangeUserOrganizationToCompany < ActiveRecord::Migration
  def self.up
    rename_column :users, :organization_id, :company_id
  end

  def self.down
    rename_column :users, :company_id, :organization_id
  end
end
