class AddDeletedToUsersAndCompanies < ActiveRecord::Migration
  def self.up
    add_column :companies, :deleted, :boolean, :null => false, :default => false
    add_column :users, :deleted, :boolean, :null => false, :default => false

    add_index :companies, :deleted
    add_index :users, :deleted
  end

  def self.down
    remove_column :companies, :deleted
    remove_column :users, :deleted
  end
end
