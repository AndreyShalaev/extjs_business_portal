class CreateUserPermissions < ActiveRecord::Migration
  def change
    create_table :user_permissions do |t|

      t.references :permission
      t.references :user

      t.timestamps
    end

    add_index :user_permissions, :permission_id
    add_index :user_permissions, :user_id

  end
end
