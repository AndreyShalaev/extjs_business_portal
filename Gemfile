source 'https://rubygems.org'

gem 'rails', '3.2.13'

gem 'mysql2', '~> 0.3.11'
gem 'pg', '~> 0.15.1'


# Auth
gem 'devise', '~> 2.2.3'

# Nested sets
gem 'nested_set', '~> 1.7.1'

# Russian I18n
gem 'russian', '~> 0.6.0'
# I18n
gem 'i18n-js', '~> 3.0.0.rc5'

# API
gem 'acts_as_api', '~> 0.4.1'

# Paginate
gem 'will_paginate', '~> 3.0'

# Comments engine
gem 'acts_as_commentable', '~> 3.0.1'

# File attachment
gem 'paperclip', '3.4.2'

# User abilities
gem 'cancan', '~> 1.6.10'

# Xlsx renderer
gem 'acts_as_xlsx', '~> 1.0.6'
gem 'axlsx_rails', '~> 0.1.4'

# Soft delete
gem 'acts_as_paranoid', '~> 0.4.2'

# Search engine
gem 'thinking-sphinx', '~> 3.0.4'

# Audit activerecord
gem 'audited-activerecord', '~> 3.0'

# Forms
gem 'simple_form', '~> 2.1.0'

# Resque
gem 'resque', '~> 1.24.1'
gem 'resque_mailer', '~> 2.2.4'

# Cache redis store
gem 'redis-rails', '~> 3.2.3'


group :test, :development do
  gem 'rspec-rails', '~> 2.12.0'
end

group :test do
  gem 'database_cleaner', '~> 0.9.1'
  gem 'factory_girl_rails', '~> 4.1.0'
  gem 'spork', '~> 1.0rc'
end

group :development do
  # Deploy with Capistrano
  gem 'capistrano', '~> 2.15.4'
  gem 'annotate', '~> 2.5.0'
  gem 'rvm-capistrano', '~> 1.3.0'
  gem 'capistrano-resque', '~> 0.1.0'
  gem 'letter_opener', '~> 1.1.2'
  gem 'rails_best_practices', '~> 1.14.0'
  # To order to convert from any SQL database to Postgresql use follow gems:
  #gem 'taps', '~> 0.3.24'
  #gem 'sqlite3', '~> 1.3.7'
  #gem 'mysql2', '~> 0.3.11'
end

group :production do
  # Use unicorn as the app server
  gem 'unicorn', '~> 4.6.2'
end


# Gems used only for assets and not required
# in production environments by default.
group :assets do
  gem 'sass-rails',   '~> 3.2.3'
  gem 'coffee-rails', '~> 3.2.1'

  # See https://github.com/sstephenson/execjs#readme for more supported runtimes
  gem 'therubyracer', '~> 0.10.1', :platforms => :ruby

  gem 'uglifier', '~> 2.0.1'
  gem 'compass-rails', '~> 1.0.3'
  gem 'bootstrap-sass', '~> 2.3.2.1'
  # Unnecessarry
  #gem 'jquery-rails', '~> 2.1.4'
end
