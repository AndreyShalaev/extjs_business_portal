# == Schema Information
#
# Table name: tender_attachments
#
#  id            :integer          not null, primary key
#  tender_id     :integer
#  attachment_id :integer
#

class TenderAttachment < ActiveRecord::Base
  attr_accessible :attachment_id, :tender_id, :attachment

  belongs_to :attachment
  belongs_to :tender
end
