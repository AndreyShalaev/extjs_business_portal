# == Schema Information
#
# Table name: roles
#
#  id          :integer          not null, primary key
#  name        :string(140)      not null
#  description :string(255)
#  parent_id   :integer
#

class Role < ActiveRecord::Base
  has_many :users, :dependent => :destroy

  acts_as_api

  api_accessible :extjs do |t|
    t.add :id
    t.add :name
    t.add :description
  end
end
