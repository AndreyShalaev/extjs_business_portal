# == Schema Information
#
# Table name: tender_contractors
#
#  id         :integer          not null, primary key
#  tender_id  :integer
#  company_id :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class TenderContractor < ActiveRecord::Base
  attr_accessible :company_id, :tender_id

  belongs_to :company
  belongs_to :tender, :counter_cache => true

  validates :company_id, :presence => true, :uniqueness => { :scope => :tender_id }

  acts_as_api

  api_accessible :for_tenders do |t|
    t.add :id
    t.add :company_id
    t.add lambda { |c| 0 }, :as => :'_destroy'
  end
end
