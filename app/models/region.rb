# == Schema Information
#
# Table name: regions
#
#  id   :integer          not null, primary key
#  name :string(140)      not null
#  code :integer          not null
#

class Region < ActiveRecord::Base
  include Concerns::Searchable

  has_many :companies, :dependent => :nullify

  acts_as_api
  api_accessible :extjs do |t|
    t.add :id
    t.add :name
    t.add :code
  end

  self.per_page = 50
end
