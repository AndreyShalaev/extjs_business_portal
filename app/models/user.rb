# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  username               :string(140)      not null
#  encrypted_password     :string(255)      not null
#  email                  :string(255)
#  post                   :string(140)
#  phone                  :string(20)
#  own_phone              :string(20)
#  note                   :text
#  company_id             :integer
#  name                   :string(140)      not null
#  role_id                :integer
#  middlename             :string(140)      not null
#  surname                :string(140)      not null
#  reset_password_token   :string(255)
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0)
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :string(255)
#  last_sign_in_ip        :string(255)
#  failed_attempts        :integer          default(0)
#  unlock_token           :string(255)
#  locked_at              :datetime
#  created_at             :datetime
#  updated_at             :datetime
#  legacy_password        :boolean          default(FALSE), not null
#  deleted_at             :datetime
#

class User < ActiveRecord::Base
  include Concerns::Searchable

  PHONE_REGEXP = Regexp.new([
                                '^(?:8(?:(?:21|22|23|24|51|52|53|54|55)',
                                '|(?:15\d\d))?|\+?7)?',
                                '((\((?:(?:3[04589]|4[012789]|8[^89\D]|9\d)\d)\))',
                                '|((?:(?:3[04589]|4[012789]|8[^89\D]|9\d)\d)))?\d{7}$'
                            ].join(''))

  EMAIL_REGEXP = /\A[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]+\z/

  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable,
         :rememberable, :trackable, :validatable, :recoverable

  # Setup accessible (or protected) attributes for your model
  attr_accessible :email, :password, :password_confirmation, :remember_me
  attr_accessible :username, :post, :phone, :own_phone, :note, :company_id
  attr_accessible :name, :role_id, :middlename, :surname
  attr_accessible :legacy_password, :user_permissions_attributes

  belongs_to :company
  belongs_to :role

  has_many :comments, :dependent => :destroy

  has_many :user_permissions
  has_many :permissions, :through => :user_permissions, :dependent => :destroy, :uniq => true

  scope :with_company, lambda { includes(:company) }
  scope :with_role, lambda { includes(:role) }
  scope :with_all, with_company.with_role.includes(:user_permissions)

  #validates :name, :presence => true, :length => { :minimum => 3, :maximum => 30 }
  validates :surname, :presence => true, :length => { :minimum => 3, :maximum => 50 }
  #validates :middlename, :presence => true, :length => { :minimum => 3, :maximum => 50 }

  validates :email, :presence => true, :format => { :with => EMAIL_REGEXP }, :uniqueness => true
  validates :company, :presence => true

  delegate :id, :name, :to => :company, :prefix => true
  delegate :id, :name, :description, :to => :role, :prefix => true

  accepts_nested_attributes_for :user_permissions, :allow_destroy => true

  acts_as_paranoid

  acts_as_api

  api_accessible :extjs do |t|
    t.add :id
    t.add :email
    t.add :username
    t.add :post
    t.add :phone
    t.add :own_phone
    t.add :note
    t.add :name
    t.add :middlename
    t.add :surname
    t.add :role, :template => :extjs
    t.add :role_id
    t.add :company_id
    t.add :fullname
    t.add :company, :template => :for_user
    t.add :user_permissions, :api_template => :extjs, :as => :user_permissions_attributes
    t.add :deleted
  end

  api_accessible :for_tenders do |t|
    t.add :id
    t.add :fullname
  end

  api_accessible :current do |t|
    t.add :id
    t.add :company, :template => :for_user
    t.add :fullname
    t.add :username
    t.add :permissions, :template => :for_current_user
    t.add lambda { |u| u.admin? }, :as => :admin
  end

  class << self
    def search(query)
      return self.scoped unless query.present?
      where('"users"."name" like ? or "users"."surname" like ? or "users"."middlename" like ?', "%#{query}%", "%#{query}%", "%#{query}%")
    end
  end

  def fullname
    "#{self.surname} #{self.name} #{self.middlename}".strip
  end

  def valid_password?(password)
    if self.legacy_password?
      if Devise.secure_compare(self.encrypted_password, User.legacy_password(password))

        self.password = password
        self.password_confirmation = password
        self.legacy_password = false
        self.save!

      else
        return false
      end
    end

    super(password)
  end

  def email_required?
    false
  end

  def admin?
    self.role_name == 'admin'
  end

  def deleted
    self.deleted_at.present?
  end

  self.per_page = 50

  private
  def self.legacy_password(password)
    return Digest::MD5.hexdigest(Digest::MD5.hexdigest(password))
  end

end
