# == Schema Information
#
# Table name: companies
#
#  id         :integer          not null, primary key
#  name       :string(255)      not null
#  short_name :string(140)
#  address    :string(255)
#  site       :string(255)
#  email      :string(255)
#  phone      :string(20)
#  type       :string(15)
#  area_link  :string(255)
#  hk         :boolean          default(FALSE), not null
#  region_id  :integer
#  activities :text
#  parent_id  :integer
#  lft        :integer
#  rgt        :integer
#  depth      :integer
#  ts_name    :string(255)      default(""), not null
#  inn        :integer
#  org_level  :integer
#  deleted    :boolean          default(FALSE), not null
#

class Company < ActiveRecord::Base
  include Concerns::Searchable

  attr_accessible :name, :short_name, :address, :site, :email, :phone
  attr_accessible :inn, :hk, :region, :region_id, :activities, :parent_id, :type, :org_level, :deleted


  scope :nested_set, order('lft ASC')

  scope :with_regions, lambda { includes(:region) }
  scope :with_parent, lambda { includes(:parent) }

  scope :parties, lambda { joins(:parties).group('"companies"."id"') }

  scope :live, lambda { where(:deleted => false) }

  scope :hk, lambda { where(:hk => true) }

  scope :customers, lambda { where("\"companies\".\"type\" IS NULL OR \"companies\".\"type\" = ?", 'Customer') }
  scope :contractors, lambda { where("\"companies\".\"type\" IS NULL OR \"companies\".\"type\" = ?", 'Contractor') }

  belongs_to :region

  has_many :users, :dependent => :destroy

  # Конкурсы, в которых участвует компания
  has_many :parties
  has_many :competitions, :through => :parties, :source => :tender,
           :dependent => :destroy, :readonly => true

  # contests in which the winner
  has_many :win_tenders, :dependent => :nullify,
           :class_name => 'Tender', :foreign_key => 'winner_id',
           :readonly => true

  has_many :tender_contractors
  has_many :tenders, :through => :tender_contractors, :dependent => :destroy

  validates :name, :presence => true, :length => { :minimum => 3, :maximum => 255 }, :uniqueness => true
  validates :short_name, :allow_blank => true, :length => { :maximum => 140 }
  validates :phone, :allow_blank => true,
                    :format => { :with => /^(?:8(?:(?:21|22|23|24|51|52|53|54|55)|(?:15\d\d))?|\+?7)?(?:(?:3[04589]|4[012789]|8[^89\D]|9\d)\d)?\d{7}$/ }
  validates :email, :allow_blank => true, :format => { :with => /\A[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]+\z/ }
  validates :inn, :allow_blank => true, :length => { :maximum => 12 }, :numericality => { :only_integer => true }

  delegate :name, :code, :to => :region, :prefix => true, :allow_nil => true
  delegate :name, :to => :parent, :prefix => true, :allow_nil => true

  after_save :reindex

  # Full text search
  def self.fts_search(query)
    return self.scoped unless query.present?

    query = query.gsub(/["«»\”\“]/, '').strip

    joins("INNER JOIN (SELECT \"lft\", \"rgt\" FROM \"companies\"
       WHERE \"companies\".\"ts_name\" ILIKE '%#{query}%' ORDER BY \"lft\" ASC) AS \"matches\"
       ON \"companies\".\"lft\" >= \"matches\".\"lft\" AND \"companies\".\"rgt\" <= \"matches\".\"rgt\"")
  end

  acts_as_nested_set

  acts_as_api

  api_accessible :extjs do |t|
    t.add :id
    t.add :name
    t.add :fullname
    t.add :short_name
    t.add :address
    t.add :site
    t.add :email
    t.add :phone
    t.add :inn
    t.add :hk
    t.add :activities
    t.add :region_id
    t.add :region_name
    t.add :region_code
    t.add :parent, :template => :simple
    t.add :type
    t.add :org_level
    t.add :deleted
  end

  api_accessible :simple do |t|
    t.add :id
    t.add :name
    t.add :fullname
  end

  api_accessible :for_user do |t|
    t.add :id
    t.add :hk
    t.add :name
    t.add :fullname
    t.add :type
  end

  api_accessible :for_tenders do |t|
    t.add :id
    t.add :name
    t.add :fullname
  end

  self.per_page = 50

  def fullname
    return self.name if self.parent_name.nil?

    [self.parent_name, self.name].join('-')
  end

  private
  def reindex
    Resque.enqueue(DeltaIndexes)
  end
end
