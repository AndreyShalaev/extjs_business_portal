# == Schema Information
#
# Table name: user_permissions
#
#  id            :integer          not null, primary key
#  permission_id :integer
#  user_id       :integer
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

class UserPermission < ActiveRecord::Base
  attr_accessible :user_id, :permission_id

  belongs_to :user
  belongs_to :permission


  acts_as_api

  api_accessible :extjs do |t|
    t.add :id
    t.add :permission_id
    t.add lambda { |p| 0 }, :as => :_destroy
  end
end
