# == Schema Information
#
# Table name: tenders
#
#  id                       :integer          not null, primary key
#  customer_id              :integer
#  provision_id             :integer
#  winner_id                :integer
#  subject                  :text             not null
#  price_limit              :decimal(26, 2)
#  turnaround_time          :string(140)
#  declared_at              :datetime
#  opening_at               :datetime
#  documentation            :string(140)
#  estimates                :string(140)
#  price_provision          :decimal(26, 2)
#  requirements             :string(255)
#  link                     :string(255)
#  price_cost               :decimal(26, 2)
#  price_our                :decimal(26, 2)
#  cost_at                  :datetime
#  cost_at_green            :boolean          default(FALSE), not null
#  gpr_at                   :datetime
#  gpr_at_green             :boolean          default(FALSE), not null
#  estimates_at             :datetime
#  estimates_at_green       :boolean          default(FALSE), not null
#  ready_at                 :datetime
#  ready_at_green           :boolean          default(FALSE), not null
#  price_winner             :decimal(26, 2)
#  created_at               :datetime         not null
#  updated_at               :datetime         not null
#  archive                  :boolean          default(FALSE), not null
#  tender_contractors_count :integer          default(0), not null
#  fts_subject              :tsvector
#

class Tender < ActiveRecord::Base
  include Concerns::Searchable

  attr_accessible :subject, :price_limit, :turnaround_time, :declared_at, :opening_at
  attr_accessible :documentation, :estimates, :price_provision, :requirements, :link
  attr_accessible :price_cost, :price_our, :price_winner
  attr_accessible :gpr_at_green, :estimates_at_green, :ready_at_green, :cost_at_green
  attr_accessible :customer_id, :winner_id, :archive, :provision_id, :winner
  attr_accessible :tender_contractors_attributes

  # Заказчик
  belongs_to :customer, :class_name => 'Company', :foreign_key => 'customer_id', :include => :parent
  # Обеспечение
  belongs_to :provision
  # Победитель
  belongs_to :winner, :class_name => 'Company', :foreign_key => 'winner_id'

  # Подрядчики ХК
  has_many :tender_contractors
  has_many :contractors_hk, :through => :tender_contractors, :source => :company,
           :dependent => :destroy, :uniq => true

  # Компании-учaстники
  has_many :parties, :include => [ :company, :tender ]
  has_many :participants, :through => :parties, :source => :company, :dependent => :destroy, :uniq => true

  # Файлы
  has_many :tender_attachments
  has_many :attachments, :through => :tender_attachments, :dependent => :destroy, :uniq => true


  scope :currents, lambda { includes([:contractors_hk, :provision, :customer, :comments, :tender_contractors]).current }
  scope :stuffs, lambda { includes([:customer, :provision, :contractors_hk, :comments, :tender_contractors]).stuff }

  scope :include_for_archive, lambda { includes([:contractors_hk, :customer, :winner, :parties, :comments, :attachments]) }
  scope :archives, lambda { include_for_archive.archive }

  validates :subject, :presence => true, :length => { :minimum => 3, :maximum => 2500 }
  validates :declared_at, :presence => true
  validates :opening_at, :presence => true
  validates :customer, :presence => true

  delegate :name, :fullname, :to => :customer, :prefix => true, :allow_nil => true
  delegate :name, :to => :provision, :prefix => true, :allow_nil => true
  delegate :name, :to => :winner, :prefix => true, :allow_nil => true

  accepts_nested_attributes_for :tender_contractors, :allow_destroy => true

  # Расчет сроков предоставления
  before_save :count_time_limits

  class << self
    def archive
      where('"tenders"."opening_at" <= ? OR "tenders"."archive" = ?', due_day, true)
    end

    def stuff
      where('"tenders"."tender_contractors_count" > 0').where('"tenders"."archive" = ?', false)
    end

    def current
      where('"tenders"."opening_at" > ?', due_day).where('"tenders"."archive" = ?', false)
    end

    def fts_search(query)
      return self.scoped unless query.present?
      where("\"tenders\".\"fts_subject\" @@ plainto_tsquery('ru', ?)", query)
    end

    def for_current(user)
      company = user.company
      if company.present?
        joins('LEFT OUTER JOIN "tender_contractors" ON "tender_contractors"."tender_id" = "tenders"."id"')
        .where('"tender_contractors"."company_id" = ?', company.id)
      else
        scoped
      end
    end

    def due_day
      (Time.zone.now.beginning_of_day - 1.days).to_date
    end
  end

  def last_comment
    self.comments.first
  end

  # Audit records
  audited

  # Комментарии
  acts_as_commentable :include => :user

  acts_as_xlsx

  # API v1
  acts_as_api
  api_accessible :extjs do |t|
    t.add :id
    t.add :subject
    t.add :customer, :template => :for_tenders
    t.add :customer_id
    t.add :customer_name
    t.add :price_limit # лимитная цена
    t.add :turnaround_time # это сроки выполнения работ
    t.add :opening_at # вскрытие конвертов
    t.add :declared_at # объявлен
    t.add :contractors_hk, :template => :for_tenders
    t.add :archive
    t.add :last_comment, :template => :extjs
    t.add :comments, :template => :extjs
    t.add :link

    t.add :cost_at_green
    t.add :gpr_at_green
    t.add :estimates_at_green
    t.add :ready_at_green

    t.add :documentation # проектная документация
    t.add :estimates # сметная документация

    t.add :price_cost # это себестоимость
    t.add :price_our # Стоимость заявки
    t.add :price_provision # размер обеспечения

    t.add :provision_id
    t.add :winner_id
    t.add :price_winner

    t.add :tender_contractors, :template => :for_tenders, :as => :tender_contractors_attributes
    t.add :requirements
    t.add :provision_name
  end

  api_accessible :archives, :extend => :extjs do |t|
    t.add :winner_name
    t.add :parties, :template => :extjs
    t.add :attachments, :template => :extjs
    t.remove :tender_contractors_attributes
    t.remove :provision_name
  end

  api_accessible :stuffs, :extend => :extjs do |t|
    t.add :cost_at
    t.add :gpr_at
    t.add :estimates_at
    t.add :ready_at
  end

  self.per_page = 50

  private
  def count_time_limits
    self.ready_at = count_considering_holidays(self.opening_at, 1)
    self.gpr_at = count_considering_holidays(self.opening_at, 3)
    self.estimates_at = count_considering_holidays(self.opening_at, 3)
    self.cost_at = count_considering_holidays(self.opening_at, 5)
  end

  def count_considering_holidays(limit_time_at, diff)
    return nil unless limit_time_at.kind_of?(Date)

    diff.times do |n|
      limit_time_at -= 1.days

      limit_time_at -= 1.days while limit_time_at.saturday? or limit_time_at.sunday?
    end

    limit_time_at
  end

end
