# == Schema Information
#
# Table name: comments
#
#  id               :integer          not null, primary key
#  title            :string(50)       default("")
#  comment          :text
#  commentable_id   :integer
#  commentable_type :string(255)
#  user_id          :integer
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#

class Comment < ActiveRecord::Base
  include ActsAsCommentable::Comment
  include Concerns::Searchable

  attr_accessible :user, :comment

  belongs_to :commentable, :polymorphic => true

  # NOTE: install the acts_as_votable plugin if you
  # want user to vote on the quality of comments.
  #acts_as_voteable

  # NOTE: Comments belong to a user
  belongs_to :user

  delegate :fullname, :to => :user, :prefix => true, :allow_nil => true

  acts_as_api
  api_accessible :extjs do |t|
    t.add :id
    t.add :user, :template => :for_tenders
    t.add :comment
    t.add :created_at
    t.add :commentable_id
  end
end
