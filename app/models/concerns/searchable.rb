require 'active_support/concern'

module Concerns
  module Searchable
    extend ActiveSupport::Concern

    included do
      class_eval do
        def self.db_search(query, query_field='name')
          return self.scoped unless query.present?
          where("regexp_replace(#{query_field}, '\"', '') ILIKE ?", "%#{query}%")
        end

        def self.pg(page=1, limit=50)
          paginate :per_page => limit, :page => page
        end

        def self.ordered(prop, dir='ASC')
          order("#{prop} #{dir} NULLS LAST")
        end

        def self.date_between(start, finish, query_field='created_at')
          where("#{query_field} >= ? and #{query_field} <= ?", start, finish)
        end

        def self.date_great(date, query_field='created_at')
          where("#{query_field} >= ?", date)
        end

        def self.date_less(date, query_field='created_at')
          where("#{query_field} <= ?", date)
        end
      end
    end
  end
end
