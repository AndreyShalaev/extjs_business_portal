# == Schema Information
#
# Table name: permissions
#
#  id            :integer          not null, primary key
#  name          :string(255)
#  subject_class :string(255)
#  subject_id    :integer
#  action        :string(255)
#  description   :text
#

class Permission < ActiveRecord::Base
  include Concerns::Searchable

  attr_accessible :id, :action, :description, :name, :subject_class, :subject_id

  has_many :user_permissions
  has_many :users, :through => :user_permissions, :dependent => :destroy

  scope :by_subject, lambda { |subject| where('"permissions"."subject_class" = ?', subject) }
  scope :by_action, lambda { |action| where('"permissions"."action" = ?', action) }

  acts_as_api

  api_accessible :extjs do |t|
    t.add :id
    t.add :action
    t.add :description
    t.add :name
    t.add :subject_class
    t.add :subject_id
    t.add lambda { |p| false }, :as => :assigned
  end

  api_accessible :for_current_user do |t|
    t.add :id
    t.add :permission
  end

  def permission
    "#{self.subject_class}_#{self.action}".downcase
  end
end
