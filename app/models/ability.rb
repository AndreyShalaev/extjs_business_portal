class Ability
  include CanCan::Ability

  def initialize(user)
    if user.admin?
      can :manage, :all

    else
      # Все привилегии для пользователя хранятся в БД
      # По-умолчанию, сотрудник может войти, если не удален
      can :read, :home unless user.deleted? || user.permissions.size == 0

      can do |action, subject_class, subject|
        user.permissions.find_all_by_action(aliases_for_action(action)).any? do |permission|
          permission.subject_class == subject_class.to_s &&
              (subject.nil? || permission.subject_id.nil? || permission.subject_id == subject.id)
        end
      end

      # Не может удалять комментарии
      cannot :destroy, Comment
    end

    #cannot :manage, :all if user.resource?
  end
end
