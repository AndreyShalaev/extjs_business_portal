# == Schema Information
#
# Table name: provisions
#
#  id   :integer          not null, primary key
#  name :string(255)      not null
#

class Provision < ActiveRecord::Base
  include Concerns::Searchable

  attr_accessible :name

  has_many :tenders, :dependent => :nullify

  validates :name, :presence => true, :uniqueness => true

  acts_as_api

  api_accessible :extjs do |t|
    t.add :id
    t.add :name
  end
end
