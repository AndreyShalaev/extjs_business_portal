# == Schema Information
#
# Table name: parties
#
#  id           :integer          not null, primary key
#  tender_id    :integer
#  company_id   :integer
#  price        :decimal(26, 2)
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  price_finish :decimal(26, 2)
#

class Party < ActiveRecord::Base
  include Concerns::Searchable

  attr_accessor :winner
  attr_accessible :winner

  attr_accessible :price, :tender, :company, :price_finish, :tender_id, :company_id


  belongs_to :tender, :include => :winner
  belongs_to :company, :include => :parent

  validates :company_id, :presence => true, :uniqueness => { :scope => :tender_id }

  delegate :winner, :to => :tender, :prefix => true, :allow_nil => true
  delegate :name, :to => :company, :prefix => true, :allow_nil => true


  after_save :check_winner
  before_destroy :destroy_winner

  acts_as_api

  api_accessible :extjs do |t|
    t.add :id
    t.add :company, :template => :simple
    t.add :tender_id
    t.add :price
    t.add :price_finish
    t.add lambda { |p| p.winner? }, :as => :winner
  end

  def winner?
    self.company == self.tender_winner
  end

  def winner=(bool)
    @winner = bool
  end

  private
  def check_winner
    if @winner
      price_winner = self.price_finish.present? ? self.price_finish : self.price

      self.tender.update_attributes!(:winner => self.company, :price_winner => price_winner)
    elsif self.tender_winner == self.company
      self.tender.update_attributes!(:winner => nil, :price_winner => nil)
    end
  end

  def destroy_winner
    self.tender.update_attributes!(:winner => nil, :price_winner => nil) if self.tender_winner == self.company
  end
end
