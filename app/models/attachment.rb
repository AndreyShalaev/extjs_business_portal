# == Schema Information
#
# Table name: attachments
#
#  id                :integer          not null, primary key
#  name              :string(255)
#  file_file_name    :string(255)
#  file_content_type :string(255)
#  file_file_size    :integer
#  file_updated_at   :datetime
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#

class Attachment < ActiveRecord::Base
  include Concerns::Searchable

  attr_accessible :name, :file

  has_many :tender_attachments
  has_many :tenders, :through => :tender_attachments, :dependent => :destroy

  validates_attachment :file, :presence => true, :size => { :less_than => 20.megabytes }

  has_attached_file :file,
                    :url => "/system/:class/:id/:basename.:extension",
                    :path => ":rails_root/public/system/:class/:id/:filename"

  acts_as_api

  api_accessible :extjs do |t|
    t.add :id
    t.add :name
    t.add lambda {|a| a.file.url }, :as => :file
  end

  self.per_page = 50
end
