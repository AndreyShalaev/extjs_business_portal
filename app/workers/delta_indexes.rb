class DeltaIndexes
  @queue = :delta_index

  def self.perform
    `rake ts:rebuild RAILS_ENV=#{Rails.env}`
  end
end