ThinkingSphinx::Index.define :tender, :with => :active_record do
  indexes subject, :sortable => true
end