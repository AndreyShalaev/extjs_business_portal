ThinkingSphinx::Index.define :company, :with => :active_record, :delta => true do
  indexes name, :sortable => true
  indexes region.name, :as => :region_name
  indexes inn

  #where sanitize_sql(["published", true])
end