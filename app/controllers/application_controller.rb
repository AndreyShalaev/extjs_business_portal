class ApplicationController < ActionController::Base
  protect_from_forgery

  before_filter :prepare_for_mobile

  MOBILE_USER_AGENTS =  'palm|blackberry|nokia|phone|midp|mobi|symbian|chtml|ericsson|minimo|' +
      'audiovox|motorola|samsung|telit|upg1|windows ce|ucweb|astel|plucker|' +
      'x320|x240|j2me|sgh|portable|sprint|docomo|kddi|softbank|android|mmp|' +
      'pdxgw|netfront|xiino|vodafone|portalmmm|sagem|mot-|sie-|ipod|up\\.b|' +
      'webos|amoi|novarra|cdm|alcatel|pocket|ipad|iphone|mobileexplorer|' +
      'mobile'

  rescue_from CanCan::AccessDenied do |exception|
    render :file => "#{Rails.root}/public/403", :status => 403, :layout => false, :formats => [:html]
  end

  private
  def prepare_for_mobile
    session[:mobile_param] = params[:mobile] if params[:mobile]
  end

  def mobile?
    if session[:mobile_param]
      session[:mobile_param] == '1'
    else
      request.user_agent.to_s.downcase =~ Regexp.new(MOBILE_USER_AGENTS)
    end
  end
  helper_method :mobile?
end
