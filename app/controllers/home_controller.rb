class HomeController < ApplicationController

  before_filter :authenticate_user!
  
  layout :mobile_layout

  def index
    authorize! :read, :home
  end

  private
  def mobile_layout
    mobile? ? 'mobile' : 'application'
  end
end
