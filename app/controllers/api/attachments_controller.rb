class Api::AttachmentsController < ApiController

  before_filter :find_tender

  # GET /api/tenders/:tender_id/attachments
  def index
    @attachments = @tender.attachments.pg(params[:page], params[:limit]).ordered(@field, @dir)

    respond_with @attachments, :api_template => :extjs, :meta => { :total => @attachments.total_entries }
  end

  # POST /api/tenders/:tender_id/attachments
  def create
    @attachment = Attachment.new(params[:attachment])

    @attachment_tender = @tender.tender_attachments.build(:attachment => @attachment)

    if @attachment_tender.save
      respond_with @attachment, :api_template => :extjs, :meta => { :success => true }, :location => nil
    else
      respond_with @attachment.errors, :meta => { :success => false }
    end
  end

  # DELETE /api/tenders/:tender_id/attachments/:id
  def destroy
    @attachment = @tender.attachments.find(params[:id])

    @attachment.destroy

    respond_with @attachment, :api_template => :extjs, :meta => { :success => true }, :location => nil
  end

  private
  def find_tender
    @tender = Tender.find(params[:tender_id])
  end
end
