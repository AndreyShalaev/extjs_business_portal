class Api::TendersController < ApiController

  before_filter :build_filter, :except => [ :create, :update ]

  # GET /api/tenders(.xlsx)
  def index

    @days = params[:days].present? ? params[:days].to_i : 30

    current_date = Time.zone.now

    @currents = Tender.currents
                      .where("tenders.opening_at <= ?", current_date + @days.days)
                      .order('opening_at ASC')

    @stuffs = Tender.stuffs.where("tenders.opening_at <= ?", current_date + @days.days).order('opening_at ASC')
    @archives = Tender.archives.where("tenders.opening_at >= ?", current_date - @days.days).order('opening_at DESC')


    respond_to do |format|
      format.xlsx
    end
  end

  # GET /api/tenders/currents
  def currents
    @tenders = @filtered.currents

    respond_to do |format|
      format.json do
        @tenders =  @tenders.ordered(@field, @dir).pg(params[:page], params[:limit])
        respond_with @tenders, :api_template => :extjs, :meta => { :total => @tenders.total_entries }
      end

      format.xlsx do
        @tenders =  @tenders.order('opening_at ASC')
      end
    end
  end

  # GET /api/tenders/stuffs
  def stuffs
    @tenders = params[:show_all].present? && params[:show_all].to_i > 0 ? @filtered : @filtered.for_current(current_user)

    @tenders = @tenders.stuffs

    respond_to do |format|
      format.json do
        @tenders = @tenders.ordered(@field, @dir).pg(params[:page], params[:limit])
        respond_with @tenders, :api_template => :stuffs, :meta => { :total => @tenders.total_entries }
      end

      format.xlsx do
        @tenders =  @tenders.order('opening_at ASC')
      end
    end

  end

  # GET /api/tenders/archives
  def archives

    @tenders = @filter_all ? @filtered.include_for_archive : @filtered.archives

    respond_to do |format|
      format.json do
        @tenders = @tenders.ordered(@field, @dir).pg(params[:page], params[:limit])
        respond_with @tenders, :api_template => :archives, :meta => { :total => @tenders.total_entries }
      end

      format.xlsx do
        @tenders =  @tenders.order('opening_at DESC')
      end
    end
  end

  # POST /api/tenders
  def create

    @tender = Tender.new(params[:tender])

    respond_to do |format|
      if @tender.save
        format.json { render_for_api :extjs, :json => @tender, :meta => { :success => true } }
      else
        format.json do
          render :json => { :errors => [ @tender.errors ],
                            :success => false,
                            :message => I18n.t('users.not_saved') },
                 :status => :unprocessable_entity
        end
      end
    end
  end

  # PUT /api/tenders/:id
  def update
    @tender = Tender.find(params[:id])

    respond_to do |format|
      if @tender.update_attributes(params[:tender].except('id'))
        format.json { render_for_api :extjs, :json => @tender, :meta => { :success => true } }
      else
        format.json do
          render :json => { :errors => [ @tender.errors ],
                            :success => false,
                            :message => I18n.t('users.not_saved') },
                 :status => :unprocessable_entity
        end
      end
    end
  end

  # DLETE /api/tenders/:id
  def destroy
    @tender = Tender.find(params[:id])

    @tender.destroy

    respond_to do |format|
      format.json { render :json => { :success => true, :message => I18n.t('tenders.deleted') } }
    end
  end

  # /api/tenders/:id/send
  def sending
    @tender = Tender.find(params[:id])

    TenderMailer.item_email(params[:email], current_user.id, @tender.id, params[:comment]).deliver

    respond_to do |format|
      format.json { render :json => { :success => true, :message => I18n.t('tenders.sended') } }
    end
  end


  private
  def build_filter
    @filtered = Tender.scoped
    @filter_all = false

    filter = nil

    if params[:filter].present?
      if params[:filter].kind_of?(Hash)
        filter = params[:filter].values
      else
        filter = JSON.parse(params[:filter])
      end
    end

    if filter.kind_of?(Array) && filter.size > 0

      filter.each do |v|

        property = v['property']

        @filter_all = true if property == 'taking_current' && v['value'].to_i > 0

        @filtered = @filtered.fts_search(v['value']) if property == 'subject'
        @filtered = @filtered.date_great(v['value'], "\"tenders\".\"#{property.gsub(/([a-z0-9_])_start$/, '\\1')}\"") if /[a-z0-9_]+_at_start$/ =~ property
        @filtered = @filtered.date_less(v['value'], "\"tenders\".\"#{property.gsub(/([a-z0-9_])_end$/, '\\1')}\"") if /[a-z0-9_]+_at_end$/ =~ property

        # filter by party
        @filtered = @filtered.joins('LEFT OUTER JOIN "parties" ON "parties"."tender_id" = "tenders"."id"')
                             .where('"parties"."company_id" = ?', v['value']) if property == 'parties.company_id'

        # filter by customer
        @filtered = @filtered.where('"tenders"."customer_id" = ?', v['value']) if property == 'customer_id'
        # filter by winner
        @filtered = @filtered.where('"tenders"."winner_id" = ?', v['value']) if property == 'winner_id'

        # filter by contractor
        @filtered = @filtered.joins('LEFT OUTER JOIN "tender_contractors" ON "tender_contractors"."tender_id" = "tenders"."id"')
                             .where('"tender_contractors"."company_id" = ?', v['value']) if property == 'tender_contractors.contractor_id'
        # filter by provision
        @filtered = @filtered.where('"tenders"."provision_id" = ?', v['value']) if property == 'provision_id'
      end
    end
  end

end
