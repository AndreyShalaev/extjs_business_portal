class Api::CompaniesController < ApiController

  # GET /api/companies
  def index

    @companies = Company.with_regions.with_parent

    @companies = @companies.live if params[:live].present? && params[:live].to_i > 0
    @companies = @companies.fts_search(params[:query]) if params[:query].present?

    @companies = @companies.pg(params[:page], params[:limit]).ordered(@field, @dir)

    respond_with @companies, :api_template => :extjs, :meta => { :total => @companies.total_entries }
  end

  # GET /api/companies/search
  def search
    @companies = Company.search Riddle::Query.escape(params[:query]), :page => params[:page], :per_page => params[:limit], :order => 'name ASC'

    respond_with @companies, :api_template => :extjs, :root => :companies, :meta => { :total => @companies.total_entries }
  end

  # POST /api/companies
  def create
    @company = Company.new(params[:company])

    respond_to do |format|
      if @company.save
        format.json { render_for_api :extjs, :json => @company, :root => :company, :meta => { :success => true } }
      else
        format.json do
          render :json => { :errors => [ @company.errors ],
                            :success => false,
                            :message => I18n.t('not_saved') },
                 :status => :unprocessable_entity
        end
      end
    end
  end

  # PUT /api/companies/:id
  def update
    @company = Company.find(params[:id])

    respond_to do |format|
      if @company.update_attributes(params[:company])
        format.json { render_for_api :extjs, :json => @company, :root => :company, :meta => { :success => true } }
      else
        format.json do
          render :json => { :errors => [ @company.errors ],
                            :success => false,
                            :message => I18n.t('not_saved') },
                 :status => :unprocessable_entity
        end
      end
    end
  end

  # DELETE /api/companies/:id
  def destroy
    @company = Company.find(params[:id])

    if @company.update_attributes(:deleted => true)
      respond_with @company, :api_template => :extjs, :meta => { :success => true }, :location => nil
    else
      respond_with @company.errors, :meta => { :success => false }
    end
  end

  def show
    @company = Company.find(params[:id])

    render :json => { :companies => [ @company ] }
  end

  def customers
    @customers = Company.customers.with_parent.fts_search(params[:query])

    @customers = @customers.live if params[:live].present? && params[:live].to_i > 0

    @customers = @customers.pg(params[:page], params[:limit]).ordered(@field, @dir)

    respond_with @customers, :api_template => :for_tenders, :root => :customers, :meta => { :total => @customers.total_entries }
  end

  def contractors

    @contractors = Company.hk

    @contractors = @contractors.live if params[:live].present? && params[:live].to_i > 0

    if params[:query].present?
      @contractors = @contractors.fts_search(params[:query])
    end

    @contractors = @contractors.pg(params[:page], params[:limit]).ordered(@field, @dir)

    respond_with @contractors, :api_template => :for_tenders, :root => :contractors, :meta => { :total => @contractors.total_entries }
  end

  def winners

    @winners = Company.scoped

    @winners = @winners.live if params[:live].present? && params[:live].to_i > 0

    if params[:query].present?
      @winners = @winners.fts_search(params[:query])
    end

    @winners = @winners.joins(:win_tenders).group('companies.id')

    @winners = @winners.pg(params[:page], params[:limit]).ordered(@field, @dir)

    respond_with @winners, :api_template => :simple, :root => :winners, :meta => { :total => @winners.total_entries }
  end
end
