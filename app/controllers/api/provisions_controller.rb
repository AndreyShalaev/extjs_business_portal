class Api::ProvisionsController < ApiController

  # GET /api/provisions
  def index
    @provisions = Provision.db_search(params[:query]).pg(params[:page], params[:limit]).ordered(@field, @dir)

    respond_with @provisions, :api_template => :extjs, :meta => { :total => @provisions.total_entries }
  end

  # POST /api/provisions
  def create
    @provision = Provision.new(params[:provision])

    respond_to do |format|
      if @provision.save
        format.json { render_for_api :extjs, :json => @provision, :meta => { :success => true } }
      else
        format.json do
          render :json => { :errors => [ @provision.errors ],
                            :success => false,
                            :message => I18n.t('provisions.not_saved') },
                 :status => :unprocessable_entity
        end
      end
    end
  end

end
