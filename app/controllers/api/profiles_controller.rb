class Api::ProfilesController < ApiController

  # GET /api/profile
  def show
    respond_with current_user, :api_template => :current, :location => nil
  end

end
