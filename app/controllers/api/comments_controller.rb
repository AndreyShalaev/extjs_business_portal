class Api::CommentsController < ApiController

  before_filter :find_commentable

  # GET /api/comments
  def index
    @comments = @commentable.comments.pg(params[:page], params[:limit]).ordered(@field, @dir)

    respond_with @comments, :api_template => :extjs, :meta => { :total => @comments.total_entries }
  end

  # POST /api/comments
  def create

    @comment = @commentable.comments.build(params[:comment].merge(:user => current_user))

    respond_to do |format|
      if @comment.save
        format.json { render :json => { :comments => [ @comment ], :success => true, :message => 'Created a new comment' } }
      else
        format.json do
          render :json => { :data => @comment.errors, :success => false, :message => 'Errors when create company' }, :status => :unprocessable_entity
        end
      end
    end
  end

  # DELETE /api/comments/:id
  def destroy
    @comment = @commentable.comments.find(params[:id])

    authorize! :destroy, @comment

    @comment.destroy

    respond_with @comment, :api_template => :extjs, :meta => { :success => true }
  end

  private
  def find_commentable
    raise ActionController::RoutingError.new('Not Found') unless params[:type].present? && params[:commentable_id].present?


    class_name = params[:type].camelize.constantize

    @commentable = class_name.find(params[:commentable_id])

    rescue NameError
      raise ActionController::RoutingError.new('Not Found')
  end

end
