class Api::PermissionsController < ApiController

  def index

    @avilable_permissions = Permission.scoped

    if params[:query]
      @avilable_permissions = @avilable_permissions.db_search(params[:query], '"permissions"."description"')
    end

    @avilable_permissions = @avilable_permissions.pg(params[:page], params[:limit]).ordered(@field, @dir)

    respond_with @avilable_permissions, :api_template => :extjs, :meta => { :total => @avilable_permissions.total_entries }
  end

end
