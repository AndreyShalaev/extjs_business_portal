class Api::RegionsController < ApiController

  def index
    @regions = Region.scoped

    if params[:query].present?
      @regions = @regions.db_search(params[:query])
    end

    @regions = @regions.pg(params[:page], params[:limit]).ordered(@field, @dir)

    respond_with @regions, :api_template => :extjs, :meta => { :total => @regions.total_entries }
  end
end
