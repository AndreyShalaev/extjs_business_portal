class Api::AuditTendersController < ApiController
	# GET /api/tenders/audits
	def index
		@audits = Audited.audit_class.unscoped
                     .where(:auditable_type => 'Tender')
                     .includes(:user, :auditable)
                     .order("#{@field} #{@dir} NULLS LAST")
                     .paginate(:per_page => params[:limit], :page => params[:page])

    respond_with @audits, :api_template => :extjs, :root => :audits, :meta => { :total => @audits.total_entries }
	end
end