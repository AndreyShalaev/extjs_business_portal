class Api::PartiesController < ApiController

  before_filter :find_tender

  # GET /api/tenders/:tender_id/parties
  def index
    @parties = @tender.parties.pg(params[:page], params[:limit]).ordered(@field, @dir)

    respond_with @parties, :api_template => :extjs, :meta => { :total => @parties.total_entries }
  end

  # POST /api/tenders/:tender_id/parties
  def create
    @party = @tender.parties.build(params[:party])

    if @party.save
      respond_with @party, :api_template => :extjs, :meta => { :success => true }, :location => nil
    else
      respond_with @party, :meta => { :success => false }, :location => nil
    end
  end

  # PUT /api/tenders/:tender_id/parties/:id
  def update
    @party = @tender.parties.find(params[:id])

    respond_to do |format|
      if @party.update_attributes(params[:party])
        format.json { render_for_api :extjs, :json => @party, :meta => { :success => true } }
      else
        format.json do
          render :json => { :errors => [ @party.errors ],
                            :success => false,
                            :message => I18n.t('users.not_saved') },
                 :status => :unprocessable_entity
        end
      end
    end
  end

  # DELETE /api/tenders/:tender_id/parties/:id
  def destroy
    @party = @tender.parties.find(params[:id])

    @party.destroy

    respond_with @party, :api_template => :extjs, :meta => { :success => true }, :location => nil
  end

  private
  def find_tender
    @tender = Tender.find(params[:tender_id])
  end
end
