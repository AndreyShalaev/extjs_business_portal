class Api::UsersController < ApiController

  before_filter :check_live, :only => [ :index ]

  # GET /api/users
  def index
    @users = @proxy.db_search(params[:query], '"users"."name"').pg(params[:page], params[:limit]).ordered(@field, @dir)

    respond_with @users, :api_template => :extjs, :meta => { :total => @users.total_entries }
  end

  # POST /api/companies/:company_id/users
  def create
    @company = Company.find(params[:company_id])

    @user = @company.users.build(params[:user])

    respond_to do |format|
      if @user.save
        format.json { render_for_api :extjs, :json => @user, :meta => { :success => true } }
      else
        format.json do
          render :json => { :errors => [ @user.errors ],
                            :success => false,
                            :message => I18n.t('users.not_saved') },
                 :status => :unprocessable_entity
        end
      end
    end
  end

  # PUT /api/companies/:company_id/users/:id
  def update
    @company = Company.find(params[:company_id])

    @user = @company.users.find(params[:id])

    user_params = params[:user]['password'].present? ? params[:user].merge(:legacy_password => false) : params[:user]

    respond_to do |format|
      if @user.update_attributes(user_params)
        format.json { render_for_api :extjs, :json => @user, :meta => { :success => true } }
      else
        format.json do
          render :json => { :errors => [ @user.errors ],
                            :success => false,
                            :message => I18n.t('users.not_saved') },
                 :status => :unprocessable_entity
        end
      end
    end
  end

  # DELETE /api/companies/:company_id/users/:id
  def destroy
    @company = Company.find(params[:company_id])

    @user = @company.users.find(params[:id])

    @user.destroy

    respond_to do |format|
      format.json { render :json => { :success => true } }
    end
  end

  private
  def check_live
    @proxy = User.with_all
    @proxy = @proxy.with_deleted unless params[:live].present? && params[:live].to_i > 0
  end
end
