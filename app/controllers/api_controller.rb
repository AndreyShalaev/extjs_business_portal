class ApiController < ApplicationController
  self.responder = ActsAsApi::Responder

  respond_to :json

  before_filter :authenticate_user!, :set_sort_options

  private
  def set_sort_options
    @field = 'id'
    @dir = 'ASC'

    if params[:sort] && (sort = JSON.parse(params[:sort])).size > 0

      @field = sort[0]['property'].gsub(/0/, ".")
      @dir = sort[0]['direction']
    end
  end
end
