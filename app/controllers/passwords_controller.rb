class PasswordsController < Devise::PasswordsController
  layout :mobile_layout

  private
  def mobile_layout
    mobile? ? 'login_mobile' : 'login'
  end
end