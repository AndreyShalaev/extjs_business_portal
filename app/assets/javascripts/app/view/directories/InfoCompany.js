// todo: Переписать на coffeescript
Ext.define("IPortal.view.directories.InfoCompany", {
  extend: "Ext.form.Panel",

  title: "Информация",

  xtype: "companyInfo",

  bodyPadding: 5,

  flex: 1,

  layout: "anchor",

  defaultType: "displayfield",

  icon: "/assets/information.png",

  items: [{
    xtype: "fieldset",
    title: "О компании",
    defaultType: "displayfield",
    layout: "anchor",
    items: [{
      fieldLabel: "Название",
      name: "name"
    },{
      fieldLabel: "Основная компания",
      name: "parent_name"
    },{
      fieldLabel: "Регион",
      name: "region_id",
      renderer: function () {

        var record = this.up("form").getRecord();
        var code = record.get("region_code");
        var name = record.get("region_name");

        if (code) {
          return code + " - " + name;
        } else {
          return "";
        }
      }
    },{
      fieldLabel: "Тип",
      name: "type",
      renderer: function (value) {
        switch (value) {
          case "Customer": return "Заказчик"; break;
          case "Contractor": return "Подрядчик"; break;
          default: return "Заказчик и подрядчик";
        }
      }
    },{
      fieldLabel: "ИНН",
      name: "inn",
      renderer: function (value) {
          if (Number(value) > 0) {
              return value;
          } else {
              return "";
          }
      }
    },{
      fieldLabel: "ХК",
      name: "hk",
      renderer: function (value) {
        return value == "true" ? "ХК" : "";
      }
    },{
        fieldLabel: I18n.t("activerecord.attributes.company.deleted"),
        name: "deleted",
        renderer: function (value) {
            return value == "true" ? "Да" : "Нет";
        }
    },{
      fieldLabel: "Деятельность",
      name: "activities",
      renderer: function (value) {
          return value.replace(/\n/g, "<br />");
      }
    },{
        fieldLabel: "Орг. структура",
        name: "org_level",
        renderer: function (value) {
            var val = Number(value);
            if (val > 0) {
                return "Уровень " + val;
            } else {
                return "";
            }
        }
    }]
  },{
    xtype: "fieldset",
    title: "Контакты",
    defaultType: "displayfield",
    layout: "anchor",
    items: [{
      fieldLabel: "Адрес",
      name: "address"
    },{
      fieldLabel: "Email",
      name: "email",
      renderer: function (value) {
        if (value.length > 0) {
          return "<a href=\"mailto:" + value + "\">" + value + "</a>";
        } else {
          return "";
        }
      }
    },{
      fieldLabel: "Сайт",
      name: "site",
      renderer: function (value) {
        if (value.length > 0) {
          return "<a href=\"" + value + "\" target=\"_blank\">" + value + "</a>";
        } else {
          return "";
        }
      }
    },{
      fieldLabel: "Телефон",
      name: "phone"
    }]
  }],

  dockedItems: [{
    xtype: "toolbar",
    dock: "top",
    permissions: ["company_manage"],
    plugins: ["can"],
    items: [{
      xtype: "button",
      text: "Изменить",
      id: "companyInfo__editBtn",
      icon: "/assets/building_edit.png"
    }]
  }]
});
