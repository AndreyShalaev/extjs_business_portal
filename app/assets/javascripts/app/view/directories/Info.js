Ext.define("IPortal.view.directories.Info", {
  extend: "Ext.Container",

  xtype: "containerInfo",

  /*requires: [
    "Ext.layout.container.HBox"
  ],*/

  layout: {
    type: "hbox",
    pack: "start",
    align: "stretch"
  },
  flex: 1,

  items: [{
    xtype: "panel",
    title: "Информация",
    flex: 1,
    dockedItems: [{
      xtype: "toolbar",
      dock: "top",
      items: [{
        xtype: "button",
        text: "Изменить",
        disabled: true
      }]
    }]
  }]
});