Ext.define "IPortal.view.directories.form.Company",
  extend: "IPortal.view.form.Base"

  xtype: "formCompany"

  items: [
    xtype: "form"
    layout: "anchor"
    defaultType: "textfield"
    bodyPadding: 10
    id: "formCompany__form"
    defaults:
      anchor: "100%"
    buttons: [
      text: I18n.t "save"
      disabled: true
      formBind: true
      id: "formCompany__saveBtn"
    ,
      text: I18n.t "reset"
      id: "formCompany__resetBtn"
      handler: () ->
        @up("form").getForm().reset()
        @up("form").ownerCt.doLayout()
    ,
      text: I18n.t "close"
      handler: () ->
        @up("form").ownerCt.close()
    ]
    items: [
      name: "id"
      hidden: true
    ,
      fieldLabel: I18n.t "activerecord.attributes.company.name"
      name: "name"
      maxLength: 255
      allowBlank: false
    ,
      fieldLabel: I18n.t "activerecord.attributes.company.short_name"
      name: "short_name"
      maxLength: 140
    ,
      fieldLabel: I18n.t "activerecord.attributes.company.region"
      name: "region_id"
      xtype: "combo"
      store: "regionStore"
      id: "formCompany__selectRegion"
      emptyText: "Выберите регион..."
      typeAhead: true
      pageSize: 50
      valueField: "id"
      displayField: "name"
      queryMode: "remote"
    ,
      fieldLabel: I18n.t "activerecord.attributes.company.type"
      xtype: "radiogroup"
      id: "formCompany__selectType"
      vertical: true
      items: [
        boxLabel: I18n.t "activerecord.models.customer.one"
        name: "type"
        inputValue: "Customer"
        checked: true
      ,
        boxLabel: I18n.t "activerecord.models.contractor.one"
        name: "type"
        inputValue: "Contractor"
      ,
        boxLabel: I18n.t "companies.default_type"
        name: "type"
        inputValue: ""
      ]
    ,
      fieldLabel: I18n.t "activerecord.attributes.company.inn"
      name: "inn"
      maxLength: 10
      regex: /\d{10,12}/
      regexText: "ИНН не соответствует формату"
    ,
      fieldLabel: I18n.t "activerecord.attributes.company.hk"
      name: "hk"
      xtype: "checkboxfield"
      id: "formCompany__fieldHk"
      inputValue: true
    ,
      xtype: "fieldset"
      title: I18n.t "companies.contacts"
      defaultType: "textfield"
      layout: "anchor"
      defaults:
        anchor: "100%"
      items: [
        fieldLabel: I18n.t "activerecord.attributes.company.address"
        maxLength: 255
        name: "address"
      ,
        fieldLabel: I18n.t "activerecord.attributes.company.email"
        maxLength: 255
        name: "email"
        vtype: "email"
      ,
        fieldLabel: I18n.t "activerecord.attributes.company.site"
        maxLength: 255
        name: "site"
        vtype: "url"
      ,
        fieldLabel: I18n.t "activerecord.attributes.company.phone"
        name: "phone"
        maxLength: 20
        regex: /^(?:8(?:(?:21|22|23|24|51|52|53|54|55)|(?:15\d\d))?|\+?7)?(?:(?:3[04589]|4[012789]|8[^89\D]|9\d)\d)?\d{7}$/g
        regexText: "Правильный формат: +7xxxxxxxxxx или 8xxxxxxxxxx"
      ]
    ,
      fieldLabel: I18n.t "activerecord.attributes.company.activities"
      xtype: "textarea"
      name: "activities"
      grow: true
      maxLength: 3000
      id: "formCompany__activities"
    ,
      fieldLabel: I18n.t "activerecord.attributes.company.org_level"
      xtype: "radiogroup"
      vertical: true
      items: [
        boxLabel: I18n.t("companies.level") + " 1"
        name: "org_level"
        inputValue: "1"
        checked: true
      ,
        boxLabel: I18n.t("companies.level") + " 2"
        name: "org_level"
        inputValue: "2"
      ]
    ,
      fieldLabel: I18n.t "activerecord.attributes.company.deleted"
      xtype: "checkbox"
      name: "deleted"
      id: "formCompany__deleted"
      hidden: true
    ]
  ]
