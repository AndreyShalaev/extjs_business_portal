Ext.define("IPortal.view.directories.Companies", {
  extend: "Ext.grid.Panel",

  xtype: "gridCompanies",

  border: true,

  icon: "/assets/building.png",

  flex: 1,
  title: I18n.t("activerecord.models.company.other"),
  /*features: [{
    ftype: "filters",
    autoReload: false,
    local: true,
    menuFilterText: "Фильтр (по текущей стр.)",
    filters: [{
      type: "string",
      dataIndex: "name"
    },{
      type: "string",
      dataIndex: "inn"
    },{
      type: "list",
      dataIndex: "region_id",
      options: Ext.data.StoreManager.lookup("regionStore").data.getRange().map(function (item) { return {id: item.getId(), name: item["name"]}; })
    }]
  }],*/
  columns: [{
    text: I18n.t("activerecord.attributes.company.name"),
    flex: 2,
    dataIndex: "name",
    hideable: false,
    resizable: false,
    renderer: function(value, meta, record) {
      return record.get("fullname");
    }
  },{
    text: I18n.t("activerecord.attributes.company.region"),
    flex: 1,
    dataIndex: "region_id",
    renderer: function (value, meta, record) {
      if (record.get("region_code")) {
        return record.get("region_code") + " - " + record.get("region_name");
      } else {
        return "";
      }
    },
    hideable: false,
    resizable: false
  },{
    text: I18n.t("activerecord.attributes.company.type"),
    flex: 1,
    dataIndex: "type",
    renderer: function (value, meta, record) {
      switch (value) {
        case "Customer": return "Заказчик"; break;
        case "Contractor": return "Подрядчик"; break;
        default: return "Заказчик и подрядчик";
      }
    },
    hideable: false,
    resizable: false
  },{
    text: I18n.t("activerecord.attributes.company.inn"),
    flex: 1,
    dataIndex: "inn",
    hideable: false,
    resizable: false
  }],

  store: "companyStore", //обращаемся по уникальному storeId

  initComponent: function () {

    this.bbar = Ext.create("Ext.toolbar.Paging", {
      store: this.getStore(),
      displayInfo: true,
      displayMsg: "Показано c {0} по {1} из {2} записей",
      emptyMsg: "Нет данных для отображения"
    });


    this.callParent(arguments);

  },

  dockedItems: [{
    xtype: "toolbar",
    dock: "top",
    permissions: ["company_manage"],
    plugins: ["can"],
    items: [{
      xtype: "button",
      text: I18n.t("add"),
      id: "newCompany",
      icon: "/assets/building_add.png"
    },{
      xtype: "button",
      text: I18n.t("delete"),
      id: "deleteCompany",
      disabled: true,
      icon: "/assets/building_delete.png"
    },{
      xtype: "tbfill"
    },{
      xtype: "button",
      text: "Сбросить",
      id: "gridCompanies__searchResetBtn"
    },{
      xtype: "textfield",
      id: "gridCompanies__searchField",
      emptyText: "Поиск...Например: Компан*",
      flex: 2
    }]
  }]
});
