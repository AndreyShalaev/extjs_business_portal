Ext.define("IPortal.view.directories.UsersAndCompanies", {
  extend: "IPortal.view.MainContainer",

  xtype: "dirMainContainer",

  /*requires: [
    //"IPortal.view.directories.Companies",
    //"IPortal.view.directories.Users",
    //"IPortal.view.directories.Info"
  ],*/

  items: [{
    xtype: "gridCompanies"
  },{
    xtype: "containerInfo"
  }]
});
