Ext.define "IPortal.view.manage.UserPrivileges",
  extend: "Ext.grid.Panel"

  xtype: "gridUserPrivileges"

  layout: "fit"

  flex: 1

  viewConfig:
    getRowClass: ->
      "enable-cell-borders"
    preserveScrollOnRefresh: true

  border: true

  autoScroll: true

  store: "userStore"

  columns: [
    xtype: "checkcolumn"
    dataIndex: "assigned"
    sortable: false
    resizable: false
    hideable: false
    flex: 1
  ,
    text: I18n.t "activerecord.attributes.user.fullname"
    dataIndex: "surname"
    sortable: false
    resizable: false
    hideable: false
    flex: 10
    renderer: (value, meta, record) ->
      record.get("fullname")
  ]

  initComponent: () ->
    @bbar = Ext.create "Ext.toolbar.Paging",
      store: @getStore()
      displayInfo: true
      displayMsg: "Показано c {0} по {1} из {2} записей"
      emptyMsg: "Нет данных для отображения"

    @callParent arguments
