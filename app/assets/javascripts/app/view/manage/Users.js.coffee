Ext.define "IPortal.view.manage.Users",
  extend: "Ext.grid.Panel"

  xtype: "gridUsers"

  flex: 1

  icon: "/assets/users.png"

  viewConfig:
    getRowClass: () ->
      "enable-cell-borders"

  border: true

  autoScroll: true

  title: I18n.t "activerecord.models.user.other"

  store: "userStore"

  columns: [
    text: I18n.t "activerecord.attributes.user.fullname"
    flex: 2
    dataIndex: "surname"
    renderer: (value, metaData, record) ->
      record.get("fullname")
  ,
    text: I18n.t "activerecord.attributes.user.email"
    flex: 1
    dataIndex: "email"
  ,
    text: I18n.t "activerecord.attributes.user.company"
    flex: 1
    dataIndex: "companies.name"
    renderer: (value, meta, record) ->
      record.get("company_name")
  ,
    text: I18n.t "activerecord.attributes.user.post"
    flex: 1
    dataIndex: "post"
  ,
    text: I18n.t "activerecord.attributes.user.role"
    flex: 1
    dataIndex: "roles.description"
    renderer: (value, meta, record) ->
      record.get("role_description")
  ,
    text: I18n.t "activerecord.models.permission.other"
    xtype: "actioncolumn"
    align: "center"
    items: [
      icon: "/assets/application_key.png"
      handler: (view, rowIdx, colIdx, item, event, record) ->
        @fireEvent("itemclick", view, rowIdx, colIdx, item, event, record)
      getClass: (v, meta, record) ->
        if record.get("role_name") == "admin" or IPortal.user.get("id") == record.get("id")
          return "hideAcceptBtn"
        else
          return ""
    ]
  ]

  initComponent: () ->
    @bbar = Ext.create "Ext.toolbar.Paging",
      store: @getStore()
      displayInfo: true
      displayMsg: "Показано c {0} по {1} из {2} записей"
      emptyMsg: "Нет данных для отображения"

    @callParent arguments

  dockedItems: [
    xtype: "toolbar"
    dock: "top"
    items: [
      xtype: "button"
      text: I18n.t "add"
      id: "gridUsers__addBtn"
      icon: "/assets/user_add.png"
    ,
      xtype: "button"
      text: I18n.t "delete"
      id: "gridUsers__deleteBtn"
      disabled: true
      icon: "/assets/user_delete.png"
    ]
  ]
