Ext.define "IPortal.view.manage.Permission",
  extend: "Ext.grid.Panel"

  xtype: "gridPermissions"

  layout: "fit"

  flex: 1

  viewConfig:
    getRowClass: () ->
      "enable-cell-borders"

  border: true

  autoScroll: true

  store: "permissionStore"

  columns: [
    xtype: "checkcolumn"
    dataIndex: "assigned"
    sortable: false
    resizable: false
    hideable: false
    flex: 1
  ,
    text: I18n.t "activerecord.attributes.permission.description"
    dataIndex: "description"
    sortable: false
    resizable: false
    hideable: false
    flex: 10
  ]

  initComponent: () ->
    @bbar = Ext.create "Ext.toolbar.Paging",
      store: @getStore()
      displayInfo: true
      displayMsg: "Показано c {0} по {1} из {2} записей"
      emptyMsg: "Нет данных для отображения"

    @callParent arguments
