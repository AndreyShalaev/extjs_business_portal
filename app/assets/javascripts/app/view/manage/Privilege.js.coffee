Ext.define "IPortal.view.manage.Privilege",
  extend: "Ext.grid.Panel"

  xtype: "gridPrivileges"

  flex: 1

  icon: "/assets/application_key.png"

  viewConfig:
    getRowClass: () ->
      "enable-cell-borders"

  border: true

  autoScroll: true

  title: I18n.t "activerecord.models.permission.other"

  store: "permissionStore"

  columns: [
    text: I18n.t "activerecord.attributes.permission.name"
    dataIndex: "description"
    flex: 10
    hideable: false
    resizable: false
  ,
    text: I18n.t "activerecord.models.user.other"
    xtype: "actioncolumn"
    flex: 1
    align: "center"
    sortable: false
    hideable: false
    resizable: false
    items: [
      icon: "/assets/users.png"
      handler: (view, rowIdx, colIdx, item, event, record) ->
        @fireEvent("itemclick", view, rowIdx, colIdx, item, event, record)
      getClass: (v, meta, record) ->
        if record.get("role_name") == "admin" or IPortal.user.get("id") == record.get("id")
          return "hideAcceptBtn"
        else
          return ""
    ]
  ]

  initComponent: () ->
    @bbar = Ext.create "Ext.toolbar.Paging",
      store: @getStore()
      displayInfo: true
      displayMsg: "Показано c {0} по {1} из {2} записей"
      emptyMsg: "Нет данных для отображения"

    @callParent arguments

  dockedItems: [
    xtype: "toolbar"
    dock: "top"
    items: [
      xtype: "button"
      text: I18n.t "add"
      id: "gridPrivileges__addBtn"
      icon: "/assets/key_add.png"
      disabled: true
    ,
      xtype: "button"
      text: I18n.t "delete"
      id: "gridPrivileges__deleteBtn"
      disabled: true
      icon: "/assets/key_delete.png"
    ]
  ]
