Ext.define "IPortal.view.manage.form.Permission",
  extend: "IPortal.view.form.Base"

  xtype: "formPermission"

  icon: "/assets/application_key.png"

  height: 480

  items: [
    xtype: "gridPermissions"
    dockedItems: [
      xtype: "toolbar"
      dock: "bottom"
      items: [
        xtype: "tbtext"
        text: "Перед переходом на другую стр. не забудьте сохранить изменения"
      ,
        xtype: "tbfill"
      ,
        text: I18n.t "save"
        id: "gridPermissions__saveBtn"
        icon: "/assets/disk.png"
      ,
        text: I18n.t "close"
        icon: "/assets/cancel.png"
        handler: (cmp) ->
          cmp.up("formPermission").close()
      ]
    ]
  ]
