Ext.define "IPortal.view.manage.form.Privilege",
  extend: "IPortal.view.form.Base"

  xtype: "formPrivilege"

  icon: "/assets/users.png"

  height: 480

  items: [
    xtype: "gridUserPrivileges"
    dockedItems: [
      xtype: "toolbar"
      dock: "bottom"
      items: [
        xtype: "tbtext"
        text: "Перед переходом на другую стр. не забудьте сохранить изменения"
      ,
        xtype: "tbfill"
      ,
        text: I18n.t "save"
        id: "gridUserPrivileges__saveBtn"
        icon: "/assets/disk.png"
      ,
        text: I18n.t "close"
        icon: "/assets/cancel.png"
        handler: (cmp) ->
          cmp.up("formPrivilege").close()
      ]
    ]
  ]
