Ext.define "IPortal.view.manage.form.User",
  extend: "IPortal.view.form.Base"

  xtype: "formUser"

  items: [
    xtype: "form"
    layout: "anchor"
    defaultType: "textfield"
    bodyPadding: 10
    trackResetOnLoad: true

    fieldDefaults:
      labelAlign: "left"

    id: "formUser__form"

    defaults:
      anchor: "100%"

    items: [
      name: "id"
      hidden: true
    ,
      fieldLabel: I18n.t "activerecord.attributes.user.surname"
      name: "surname"
      maxLength: 50
      allowBlank: false
    ,
      fieldLabel: I18n.t "activerecord.attributes.user.name"
      name: "name"
      maxLength: 30
      allowBlank: false
    ,
      fieldLabel: I18n.t "activerecord.attributes.user.middlename"
      name: "middlename"
      maxLength: 50
    ,
      fieldLabel: I18n.t "activerecord.attributes.user.company"
      name: "company_id"
      xtype: "combo"
      id: "selectCompany"
      valueField: "id"
      displayField: "name"
      pageSize: 50
      queryMode: "remote"
      allowBlank: false
      emptyText: "Выберите организацию..."
      typeAhead: true
      store: "companyStore"
    ,
      fieldLabel: I18n.t "activerecord.attributes.user.username"
      name: "username"
      allowBlank: false
      maxLength: 30
    ,
      fieldLabel: I18n.t "activerecord.attributes.user.password"
      name: "password"
      allowBlank: false
      id: "formUser__passwordField"
      maxLength: 255
      inputType: "password"
    ,
      fieldLabel: I18n.t "activerecord.attributes.user.password_confirmation"
      name: "password_confirmation"
      allowBlank: false
      id: "formUser__passwordConfirmField"
      maxLength: 255
      inputType: "password"
    ,
      fieldLabel: I18n.t "activerecord.attributes.user.email"
      name: "email"
      vtype: "email"
      maxLength: 255
      allowBlank: false
    ,
      fieldLabel: I18n.t "activerecord.attributes.user.phone"
      name: "phone"
    ,
      fieldLabel: I18n.t "activerecord.attributes.user.own_phone"
      name: "own_phone"
    ,
      fieldLabel: I18n.t "activerecord.attributes.user.post"
      name: "post"
    ,
      xtype: "radiogroup"
      fieldLabel: I18n.t "activerecord.attributes.user.role"
      vertical: true
      id: "formUser__role"
      items: [
        boxLabel: I18n.t "users.admin"
        name: "role_id"
        inputValue: "1"
      ,
        boxLabel: I18n.t "activerecord.models.user.one"
        name: "role_id"
        inputValue: "2"
        checked: true
      ]
    ,
      fieldLabel: I18n.t "activerecord.attributes.user.note"
      xtype: "textarea"
      name: "note"
      grow: true
      id: "formUser__note"
    ]

    buttons: [
      text: I18n.t("save")
      disabled: true
      formBind: true
      id: "formUser__saveBtn"
    ,
      text: I18n.t("reset")
      id: "formUser__resetBtn"
      handler: () ->
        this.up("form").getForm().reset()
        this.up("form").ownerCt.doLayout()
    ,
      text: I18n.t("close")
      handler: () ->
        this.up("form").ownerCt.close()
    ]
  ]



