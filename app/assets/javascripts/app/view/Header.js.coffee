Ext.define "IPortal.view.Header",
  extend: "Ext.Toolbar"
  xtype : "pageHeader"
  height: 53

  items: [
    xtype: "tbtext"
    cls  : "x-logo"
    text : "<h2>Внутренний портал</h2>"
  ,
    xtype: "tbfill"
  ,
    xtype: "tbtext",
    id: "pageHeader__username"
  ,
    xtype: "tbseparator"
  ,
    xtype: "tbtext"
    text: "Выбрать оформление: "
  ,
    xtype: "combo"
    store: [ "classic", "gray" ]
    mode: "local"
    triggerAction: "all"
    selectOnFocus: true
    id: "pageHeader__switchTheme"
    emptyText: "Сменить тему..."
  ,
    xtype: "tbseparator"
  ,
    xtype: "button"
    text: I18n.t "logout"
    href: "/users/logout"
    hrefTarget: "_self"
    icon: "/assets/door-open-out.png"
  ]
