Ext.define "IPortal.view.MainContainer",
  extend: "Ext.Container"

  layout:
    type: "hbox"
    pack: "start"
    align: "stretch"
  flex: 1