Ext.define "IPortal.view.Menu",
  extend: "Ext.tree.Panel"
  xtype: "portalMenu"
  title: "Меню"

  rootVisible: false
  lines: false
  useArrows: true

  store: Ext.create("IPortal.store.MainMenu")