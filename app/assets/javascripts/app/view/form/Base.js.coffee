Ext.define "IPortal.view.form.Base",
  extend: "Ext.window.Window"

  title: "&nbsp;"

  layout: "fit"

  width: 640

  modal: true

  closeAction: "destroy"

  shadow: false

  ghost: false

  autoScroll: true

  icon: "/assets/page.png"

  # Сохранение формы
  submitForm: (callback) ->
    form = @down("form").getForm()
    form.updateRecord()
    record = form.getRecord()

    # Вешаем обработчик событий
    exceptionListener = record.getProxy().on "exception", (proxy, response, operation) ->
      #console.log arguments

      responseJson =
        message: "Обратитесь к администратору или повторите попытку"
        errors: []

      errors = []

      try
        responseJson = Ext.decode response.responseText

      # Обрабатываем ошибки
      for index of responseJson.errors
        for error of responseJson.errors[index]
          textError = "<ul>"
          textError += responseJson.errors[index][error].map((msg) ->
            "<li>" + msg + "</li>"
          ).join("")
          textError += "</ul>"

          errors.push
            id: error
            msg: textError

      form.markInvalid errors

      Ext.IPortal().msg("Ошибка при сохранении", responseJson.message)
      # После срабатывания удаляем
      exceptionListener.destroy()
    , @, destroyable: true

    record.save(
      success: (record, operation) ->
        form.reset() unless operation.action is "update"

        Ext.IPortal().msg("", "Успешно сохранено.")
        exceptionListener.destroy()

        callback(record)
    )
    return @
