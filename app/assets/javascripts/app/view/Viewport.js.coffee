Ext.define "IPortal.view.Viewport",
  extend: "Ext.container.Viewport"

  xtype: "mainViewport"

  layout: "border"

  autoDestroy: true #По-умолчанию при вызове remove дочернего компонента, для этого дочернего к-а вызывается деструктор

  hidden: true

  items: [
    region: "north"
    xtype : "pageHeader"
  ,
    region: "west"
    layoutConfig:
      animate: true
    ,
    width: 250
    bodyPadding: 5
    xtype: "portalMenu"
    collapsible: true
    animCollapse: true
    split: true
  ,
    region: "center"
    layout: "fit"
    items: [
        border: false
        title: "&nbsp;"
        id   : "mainPanel"
        layout: "fit"
        bodyPadding: 0
        icon: "/assets/folder.png"
    ]
  ,
    region: "east"
    id: "eastPanel"
    title: "&nbsp"
    hidden: true
    layoutConfig:
      animate: true
    ,
    width: 350
    collapsible: true
    animCollapse: true
    split: true
    collapsed: true
    layout: "fit"
  ,
    region: "south"
    height: 14
    xtype: "component"
    html: "Разработано в &copy; DeeProSoft"
  ]
