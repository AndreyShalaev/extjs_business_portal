Ext.define "IPortal.view.comments.Form",
  extend: "Ext.window.Window"

  xtype: "formComments"

  title: "Комментарий"

  layout: "fit"

  width: 640

  modal: true

  closeAction: "destroy"

  shadow: false

  ghost: false

  autoScroll: true

  items: [
    xtype: "form"
    layout: "anchor"
    defaultType: "textarea"
    bodyPadding: 10
    defaults:
      anchor: "100%"
    fieldDefaults:
      labelAlign: "top"
    buttons: [
      text: "Отправить"
      disabled: true
      formBind: true
      id: "formComments__saveBtn"
    ,
      text: "Сброс"
      id: "formComments__resetBtn"
      handler: () ->
        @up("form").getForm().reset()
        @up("form").ownerCt.doLayout()
    ]
    items: [
      fieldLabel: "Комментарий"
      name: "comment"
      allowBlank: false
      maxLength: 2500
    ]
  ]
