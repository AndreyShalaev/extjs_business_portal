Ext.define "IPortal.view.comments.Comment",
  extend: "Ext.grid.Panel"

  xtype: "gridComments"

  layout: "fit"

  title: "Комментарии"

  flex: 1

  autoscroll: true

  height: 320

  permissions: ["comment_read"]
  plugins: ["can"]

  viewConfig:
    getRowClass: () ->
      "enable-word-wrap enable-cell-borders"

  border: true

  shadow: false

  columns: [
    text: "Комментарий"
    flex: 2
    dataIndex: "comment"
    sortable: false
    hideable: false
  ,
    text: "Пользователь"
    flex: 1
    dataIndex: "users.surname"
    renderer: (value, meta, record) ->
      record.get("user_name")
    sortable: false
    hideable: false
  ,
    text: "Дата"
    flex: 1
    dataIndex: "created_at"
    renderer: (value) ->
      Ext.util.Format.date(value, "d-M-Y H:i")
    sortable: false
    hideable: false
  ]

  dockedItems: [
    dock: "top"
    xtype: "toolbar"
    items: [
      text: "Оставить комментарий"
      id: "gridComments__addBtn"
      icon: "/assets/comment_add.png"
      permissions: ["comment_manage"]
      plugins: ["can"]
    ,
      text: I18n.t "delete"
      id: "gridComments__deleteBtn"
      icon: "/assets/minus-circle.png"
      disabled: true
      permissions: ["comment_destroy"]
      plugins: ["can"]
    ]
  ]

  store: "commentStore"

  initComponent: () ->
    @bbar = Ext.create "Ext.toolbar.Paging",
      store: @getStore()
      displayInfo: true
      displayMsg: "Показано c {0} по {1} из {2} записей"
      emptyMsg: "Нет данных для отображения"

    @callParent(arguments)
