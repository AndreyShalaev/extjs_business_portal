Ext.define "IPortal.view.tenders.JournalInfo",
  extend: "Ext.window.Window"

  xtype: "infoJournal"

  title: "&nbsp;"

  layout: "fit"

  width: 640

  height: 480

  modal: true

  closeAction: "destroy"

  shadow: false

  ghost: false

  autoScroll: true

  icon: "/assets/page.png"

  items: [
    xtype: "grid"
    layout: "fit"
    cls: "grid-tenders"

    viewConfig:
      getRowClass: () ->
        "enable-word-wrap enable-cell-borders"
    columns: [
      text: "Поле"
      dataIndex: "field"
      sortable: false
      hideable: false
      flex: 1
    ,
      text: "Было"
      dataIndex: "was"
      sortable: false
      hideable: false
      flex: 1
    ,
      text: "Стало"
      dataIndex: "been"
      sortable: false
      hideable: false
      flex: 1
    ]
  ]