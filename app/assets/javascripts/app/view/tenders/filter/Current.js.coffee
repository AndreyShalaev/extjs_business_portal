Ext.define "IPortal.view.tenders.filter.Current",
  extend: "IPortal.view.tenders.filter.Base"

  xtype: "tenderCurrentFilter"

  initComponent: () ->
    @callParent arguments

    @add([
      xtype: "combo"
      fieldLabel: "Обеспечение"
      name: "provision_id"
      store: "provisionStore"
      emptyText: "Выберите обеспечение..."
      typeAhead: true
      pageSize: 50
      valueField: "id"
      displayField: "name"
      queryMode: "remote"
    ])
