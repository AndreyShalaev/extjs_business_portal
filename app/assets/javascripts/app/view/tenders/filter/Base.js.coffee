Ext.define "IPortal.view.tenders.filter.Base",
  extend: "Ext.form.Panel"

  autoDestroy: true
  shadow: false
  autoScroll: true

  layout: "anchor"

  defaults:
    anchor: "100%"

  bodyPadding: 10

  tbar: [
    xtype: "button"
    id: "filter__sendBtn"
    text: "Применить"
    icon: "/assets/filter.png"
  ,
    xtype: "button"
    id: "filter__resetBtn"
    text: "Сброс"
  ]

  initComponent: () ->
    @callParent(arguments)

    @add([
      xtype: "textfield"
      fieldLabel: "Предмет лота"
      name: "subject"
    ,
      xtype: "combo"
      id: "filter__customerSelect"
      fieldLabel: "Заказчик"
      name: "customer_id"
      store: "customerStore"
      emptyText: "Выберите заказчика..."
      typeAhead: true
      pageSize: 50
      valueField: "id"
      displayField: "fullname"
      queryMode: "remote"
    ,
      xtype: "combo"
      id: "filter__contractorSelect"
      fieldLabel: "Подрядчик ХК"
      name: "tender_contractors.contractor_id"
      store: "contractorStore"
      emptyText: "Выберите подрядчика..."
      typeAhead: true
      pageSize: 50
      valueField: "id"
      displayField: "name"
      queryMode: "remote"
    ,
      xtype: "fieldset"
      title: "Дата объявления"
      defaultType: "datefield"
      items: [
        fieldLabel: "C"
        name: "declared_at_start"
        format: "Y-m-d"
        editable: false
      ,
        fieldLabel: "По"
        name: "declared_at_end"
        format: "Y-m-d"
        editable: false
      ]
    ,
      xtype: "fieldset"
      title: "Вскрытие конвертов"
      defaultType: "datefield"
      items: [
        fieldLabel: "С"
        name: "opening_at_start"
        format: "Y-m-d"
        editable: false
      ,
        fieldLabel: "По"
        name: "opening_at_end"
        format: "Y-m-d"
        editable: false
      ]
    ])
