Ext.define "IPortal.view.tenders.filter.Archive",
  extend: "IPortal.view.tenders.filter.Base"

  xtype: "tenderArchiveFilter"

  initComponent: () ->
    @callParent arguments

    @add([
      xtype: "combo"
      fieldLabel: "Участник"
      name: "parties.company_id"
      store: "companyStore"
      emptyText: "Выберите организацию..."
      typeAhead: true
      pageSize: 50
      valueField: "id"
      displayField: "name"
      queryMode: "remote"
    ,
      xtype: "combo"
      fieldLabel: "Победитель"
      name: "winner_id"
      store: "winnerStore"
      emptyText: "Выберите организацию..."
      typeAhead: true
      pageSize: 50
      valueField: "id"
      displayField: "name"
      queryMode: "remote"
    ,
      xtype: "checkbox"
      fieldLabel: "С учетом текущих"
      name: "taking_current"
      inputValue: 1
    ])
