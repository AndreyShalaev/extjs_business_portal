Ext.define "IPortal.view.tenders.filter.Stuff",
  extend: "IPortal.view.tenders.filter.Base"

  xtype: "tenderStuffFilter"

  initComponent: () ->
    @callParent arguments

    @add([
      xtype: "combo"
      fieldLabel: "Обеспечение"
      name: "provision_id"
      store: "provisionStore"
      emptyText: "Выберите обеспечение..."
      typeAhead: true
      pageSize: 50
      valueField: "id"
      displayField: "name"
      queryMode: "remote"
    ,
      xtype: "fieldset"
      title: "Предоставление себестоимости"
      defaultType: "datefield"
      items: [
        fieldLabel: "C"
        name: "cost_at_start"
        format: "Y-m-d"
        editable: false
      ,
        fieldLabel: "По"
        name: "cost_at_end"
        format: "Y-m-d"
        editable: false
      ]
    ,
      xtype: "fieldset"
      title: "Предоставление ГПР"
      defaultType: "datefield"
      items: [
        fieldLabel: "C"
        name: "gpr_at_start"
        format: "Y-m-d"
        editable: false
      ,
        fieldLabel: "По"
        name: "gpr_at_end"
        format: "Y-m-d"
        editable: false
      ]
    ,
      xtype: "fieldset"
      title: "Предоставление смет"
      defaultType: "datefield"
      items: [
        fieldLabel: "C"
        name: "estimates_at_start"
        format: "Y-m-d"
        editable: false
      ,
        fieldLabel: "По"
        name: "estimates_at_end"
        format: "Y-m-d"
        editable: false
      ]
    ,
      xtype: "fieldset"
      title: "Срок готовности"
      defaultType: "datefield"
      items: [
        fieldLabel: "C"
        name: "ready_at_start"
        format: "Y-m-d"
        editable: false
      ,
        fieldLabel: "По"
        name: "ready_at_end"
        format: "Y-m-d"
        editable: false
      ]
    ])
