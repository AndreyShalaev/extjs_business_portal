Ext.define "IPortal.view.tenders.Archive",
  extend: "IPortal.view.tenders.Base"

  xtype: "gridTenderArchive"

  title: I18n.t "tenders.archives"

  store: "tenderArchiveStore"

  icon: "/assets/wooden-box.png"

  dockedItems: [
    xtype: "toolbar"
    dock: "top"
    items: [
      xtype: "button"
      text: "Экспорт"
      id: "gridTenderArchive__exportBtn"
      plugins: ["can"]
      permissions: ["tender_export", "tender_manage"]
      icon: "/assets/xls.png"
    ]
  ]

  columns: [
    text: "#"
    width: 60,
    dataIndex: "id"
    locked: true
    hideable: false
  ,
    text: I18n.t "activerecord.attributes.tender.customer"
    width: 200
    dataIndex: "companies.name"
    renderer: (value, meta, record) ->
      record.get "customer_fullname"
    locked: true
    hideable: false
  ,
    text: I18n.t "activerecord.attributes.tender.subject"
    width: 285
    dataIndex: "subject"
    locked: true
    hideable: false
  ,
    text: I18n.t "activerecord.attributes.tender.price_limit"
    dataIndex: "price_limit"
    hideable: false
    renderer: (value) ->
      IPortal.view.tenders.Base.renderMoney(value)
  ,
    text: I18n.t "activerecord.attributes.tender.turnaround_time"
    dataIndex: "turnaround_time"
    hideable: false
  ,
    text: I18n.t "activerecord.attributes.tender.declared_at"
    dataIndex: "declared_at"
    hideable: false
    renderer: (value) ->
      IPortal.view.tenders.Base.renderDate(value)
  ,
    text: I18n.t "activerecord.attributes.tender.opening_at"
    dataIndex: "opening_at"
    hideable: false
    renderer: (value) ->
      IPortal.view.tenders.Base.renderDate(value)
  ,
    text: I18n.t "activerecord.attributes.tender.contractors_hk"
    width: 200
    dataIndex: "contractors_hk"
    sortable: false
    hideable: false
    renderer: (value) ->
      IPortal.view.tenders.Base.renderContractors(value)
  ,
    text: I18n.t "activerecord.attributes.tender.link"
    dataIndex: "link"
    hideable: false
    renderer: (value) ->
      if value? and value.length > 0
        "<a href=\"" + value + "\" target=\"_blank\">" + I18n.t("follow") + "</a>"
      else
        ""
  ,
    text: I18n.t "activerecord.attributes.tender.winner"
    width: 150
    dataIndex: "winner_name"
    sortable: false
    hideable: false
  ,
    text: I18n.t "activerecord.attributes.tender.price_winner"
    dataIndex: "price_winner"
    hideable: false
    renderer: (value) ->
      IPortal.view.tenders.Base.renderMoney(value)
  ,
    text: I18n.t "activerecord.attributes.tender.parties_and_prices"
    width: 350
    dataIndex: "parties"
    sortable: false
    resizable: false
    lockable: false
    hideable: false
    renderer: (value) ->
      if value and value.length > 0
        table = "<table class=\"table-parties\">"
        for v of value
          price = Number(value[v].price)
          price_finish = Number(value[v].price_finish)
          table += "<tr><td><div class=\"company\">" + value[v].company.name + "</div></td>"
          if price_finish > 0
            table += "<td>" + price_finish.formatMoney(2, ",", " ") + "</td>"
          else if price > 0
            table += "<td>" + price.formatMoney(2, ",", " ") + "</td>"
          else
            table += "<td></td>"
          table += "</tr>"
        table += "</table>"
      else
        return ""
  ,
    text: I18n.t "activerecord.attributes.tender.attachments"
    width: 250
    dataIndex: "attachments"
    sortable: false
    resizable: false
    lockable: false
    hideable: false
    renderer: (value) ->
      if value and value.length > 0
        table = "<table class=\"table-parties\">"
        for i of value
          table += "<tr><td><div class=\"company\">"
          table += "<a href=\"" + value[i].file + "\" target\"_blank\">" + value[i].name + "</a>"
          table += "</div></td></tr>"
        table += "</table>"
      else
        return ""

  ,
    text: I18n.t "activerecord.attributes.tender.comments"
    sortable: false
    hideable: false
    width: 150
    dataIndex: "comment"
    renderer: (value, meta, record) ->
      IPortal.view.tenders.Base.renderCommentField(value, meta, record)
  ]
