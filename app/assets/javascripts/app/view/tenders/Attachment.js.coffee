Ext.define "IPortal.view.tenders.Attachment",
  extend: "Ext.grid.Panel"

  xtype: "tenderAttachmentGrid"

  layout: "fit"

  title: I18n.t "activerecord.models.attachment.other"

  autoscroll: true

  height: 320

  viewConfig:
    getRowClass: ->
      "enable-cell-borders"

  border: true

  shadow: false

  columns: [
    text: I18n.t "activerecord.attributes.attachment.name"
    flex: 3
    dataIndex: "name"
  ,
    text: ""
    flex: 1
    dataIndex: "file"
    renderer: (value) ->
      return "<a href=\"" + value + "\" target=\"_blank\"><img src=\"/assets/download.png\" width=\"16\" height=\"16\" alt=\"download\" /></a>"

  ]

  dockedItems: [
    xtype: "toolbar"
    plugins: ["can"]
    permissions: ["tender_manage"]
    dock: "top"
    items: [
      text: I18n.t "add"
      id: "tenderAttachmentGrid__addBtn"
      icon: "/assets/page_add.png"
    ,
      text: I18n.t "delete"
      id: "tenderAttachmentGrid__deleteBtn"
      icon: "/assets/page_delete.png"
      disabled: true
    ]
  ]

  store: "attachmentStore"

  initComponent: () ->
    @bbar = Ext.create "Ext.toolbar.Paging",
      store: @getStore()
      displayInfo: true
      displayMsg: "Показано c {0} по {1} из {2} записей"
      emptyMsg: "Нет данных для отображения"

    @callParent(arguments)
