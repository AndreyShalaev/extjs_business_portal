Ext.define "IPortal.view.tenders.Export",
  extend: "Ext.form.Panel"

  xtype: "formTenderExport"

  title: I18n.t "tenders.export"

  shadow: false

  autoScroll: false

  icon: "/assets/xls.png"

  layout: "anchor"
  defaultType: "numberfield"
  trackResetOnLoad: true
  bodyPadding: 10
  defaults:
    anchor: "15%"
  fieldDefaults:
    labelAlign: "top"

  dockedItems: [
    xtype: "toolbar"
    dock: "top"
    items: [
      xtype: "button"
      text: I18n.t "tenders.export"
      disabled: true
      formBind: true
      id: "formTenderExport__exportBtn"
    ]
  ]

  items: [
    fieldLabel: I18n.t "tenders.export_period"
    id: "formTenderExport__period"
    allowBlank: false
    value: 30
    regex: /^\d{1,3}$/
    regexText: "Только целое не больше 999"
  ]