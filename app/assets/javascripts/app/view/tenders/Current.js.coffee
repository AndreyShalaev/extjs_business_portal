Ext.define "IPortal.view.tenders.Current",
  extend: "IPortal.view.tenders.Base"

  xtype: "gridTenderCurrent"

  title: I18n.t "tenders.currents"

  store: "tenderCurrentStore"

  enableLocking: false

  dockedItems: [
    xtype: "toolbar"
    dock: "top"
    items: [
      xtype: "button"
      text: I18n.t "add"
      id: "gridTenderCurrent__addBtn"
      icon: "/assets/page_add.png"
      plugins: ["can"]
      permissions: ["tender_manage"]
    ,
      xtype: "button"
      text: I18n.t "delete"
      disabled: true
      id: "gridTenderCurrent__deleteBtn"
      icon: "/assets/page_delete.png"
      plugins: ["can"]
      permissions: ["tender_manage"]
    ,
      xtype: "button"
      text: "Экспорт"
      id: "gridTenderCurrent__exportBtn"
      plugins: ["can"]
      permissions: ["tender_export", "tender_manage"]
      icon: "/assets/xls.png"
    ]
  ]

  icon: "/assets/baggage-cart.png"

  columns: [
    text: "#"
    width: 60
    dataIndex: "id"
    hideable: false
  ,
    text: I18n.t "activerecord.attributes.tender.customer"
    width: 200
    hideable: false
    dataIndex: "companies.name"
    renderer: (value, meta, record) ->
      record.get("customer_fullname")
  ,
    text: I18n.t "activerecord.attributes.tender.subject"
    width: 285
    dataIndex: "subject"
    hideable: false
  ,
    text: I18n.t "activerecord.attributes.tender.price_limit"
    dataIndex: "price_limit"
    hideable: false
    renderer: (value) ->
      IPortal.view.tenders.Base.renderMoney(value)
  ,
    text: I18n.t "activerecord.attributes.tender.turnaround_time"
    dataIndex: "turnaround_time"
    hideable: false
  ,
    text: I18n.t "activerecord.attributes.tender.declared_at"
    dataIndex: "declared_at"
    hideable: false
    renderer: (value) ->
      IPortal.view.tenders.Base.renderDate(value)
  ,
    text: I18n.t "activerecord.attributes.tender.opening_at"
    dataIndex: "opening_at"
    hideable: false
    renderer: (value) ->
      IPortal.view.tenders.Base.renderDate(value)
  ,
    text: I18n.t "activerecord.attributes.tender.contractors_hk"
    width: 200
    dataIndex: "contractors_hk"
    sortable: false
    hideable: false
    renderer: (value) ->
      IPortal.view.tenders.Base.renderContractors(value)
  ,
    text: ""
    xtype: "actioncolumn"
    sortable: false
    hideable: false
    permissions: ["tender_participiant"]
    plugins: ["can"]
    width: 30
    items: [
      icon: "/assets/accept.png"
      tooltip: I18n.t "tenders.accept_part"
      align: "center"
      handler: (view, rowIdx, colIdx, item, event, record) ->
        @fireEvent("itemclick", view, rowIdx, colIdx, item, event, record)
      getClass: (v, meta, record) ->
        contractors_hk = record.get("contractors_hk")
        current_user = IPortal.user

        company_hk = current_user.get("company_hk")
        company_id = current_user.get("company_id")
        company_type = current_user.get("company_type")

        if company_hk and company_id? and (not company_type? or company_type == "Contractor" or company_type == "")
          for index of contractors_hk
            if contractors_hk[index]["id"] == company_id
              return "hideAcceptBtn"
        else
          return "hideAcceptBtn"

        return ""
    ]
  ,
    text: I18n.t "activerecord.attributes.tender.provision"
    dataIndex: "provision_name"
    sortable: false
    hideable: false
  ,
    text: I18n.t "activerecord.attributes.tender.price_provision"
    dataIndex: "price_provision"
    sortable: false
    hideable: false
    renderer: (value, meta, record) ->
      IPortal.view.tenders.Base.renderPriceProvision(value, meta, record)
  ,
    text: I18n.t "activerecord.attributes.tender.link"
    dataIndex: "link"
    hideable: false
    sortable: false
    renderer: (value) ->
      if value != null and value.length > 0
        "<a href=\"" + value + "\" target=\"_blank\">" + I18n.t("follow") + "</a>"
      else
        ""
  ,
    text: I18n.t "activerecord.attributes.tender.comments"
    sortable: false
    hideable: false
    width: 150
    dataIndex: "comment"
    renderer: (value, meta, record) ->
      IPortal.view.tenders.Base.renderCommentField(value, meta, record)
  ]
