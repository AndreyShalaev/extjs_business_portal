Ext.define "IPortal.view.tenders.Party",
  extend: "Ext.grid.Panel"

  xtype: "gridParties"

  layout: "fit"

  flex: 1

  title: I18n.t "activerecord.models.party.other"

  autoscroll: true

  height: 320

  viewConfig:
    getRowClass: ->
      "enable-cell-borders"

  border: true

  shadow: false

  columns: [
    text: I18n.t "activerecord.attributes.party.company"
    dataIndex: "companies.name"
    flex: 3
    editor:
      xtype: "combo"
      store: "companyStore"
      typeAhead: true
      pageSize: 50
      valueField: "id"
      displayField: "name"
      queryMode: "remote"
      id: "gridParties__selectCompany"
    renderer: (value, meta, record) ->
      return record.get("company_name")
  ,
    text: I18n.t "activerecord.attributes.party.price"
    dataIndex: "price"
    flex: 2
    editor:
      xtype: "numberfield"
      maxValue: 999999999999
      decimalSeparator: ","
      hideTrigger: true
      listeners:
        change: (cmp) ->
          Ext.Number.priceClean(cmp)
    renderer: (value) ->
      value = Number(value)
      if value > 0
        value.formatMoney(2, ",", " ")
      else
        ""
  ,
    text: I18n.t "activerecord.attributes.party.price_finish"
    dataIndex: "price_finish"
    flex: 2
    editor:
      xtype: "numberfield"
      maxValue: 999999999999
      decimalSeparator: ","
      hideTrigger: true
      listeners:
        change: (cmp) ->
          Ext.Number.priceClean(cmp)
    renderer: (value) ->
      value = Number(value)
      if value > 0
        value.formatMoney(2, ",", " ")
      else
        ""
  ,
    text: I18n.t "activerecord.attributes.party.winner"
    dataIndex: "winner"
    flex: 1
    editor:
      xtype: "checkbox"
    align: "center"
    renderer: (value) ->
      if value
        return "<img src=\"/assets/accept.png\" />"
      else
        return "<img src=\"/assets/minus-circle.png\" />"
  ]

  store: "partyStore"

  dockedItems: [
    xtype: "toolbar"
    dock: "top"
    items: [
      text: I18n.t "add"
      id: "gridParties_addBtn"
      icon: "/assets/page_add.png"
    ,
      text: I18n.t "delete"
      id: "gridParties_deleteBtn"
      icon: "/assets/page_delete.png"
      disabled: true
    ]
  ]

  initComponent: () ->
    @bbar = Ext.create "Ext.toolbar.Paging",
      store: @getStore()
      displayInfo: true
      displayMsg: "Показано c {0} по {1} из {2} записей"
      emptyMsg: "Нет данных для отображения"

    # RowEditor plugin
    @plugins = [
      Ext.create "Ext.grid.plugin.RowEditing",
        clicksToEdit: 2
        errorSummary: false
        pluginId: "gridParties__pluginEdit"
    ]

    @callParent(arguments)
