Ext.define "IPortal.view.tenders.Base",
  extend: "Ext.grid.Panel"

  autoScroll: true
  shadow: false

  cls: "grid-tenders"

  viewConfig:
    getRowClass: ->
      "enable-word-wrap enable-cell-borders"

  enableLocking: true

  initComponent: ->
    @bbar = Ext.create("Ext.toolbar.Paging",
      store: @getStore()
      displayInfo: true
      displayMsg: "Показано c {0} по {1} из {2} записей"
      emptyMsg: "Нет данных для отображения"
    )

    # Statics Methods
    Ext.Object.merge(IPortal.view.tenders.Base,
      # рендер денег
      renderMoney: (value, meta, record) ->
        value = Number(value)
        return "" if value is 0
        value.formatMoney(2, ",", " ")
      # Рендер подрядчиков ХК
      renderContractors: (value, meta, record) ->
        return "" if not (value instanceof Array) or value.length is 0
        Ext.Array.map(value, (item) ->
          item.fullname
        ).join(";<br>")
      # рендер дат
      renderDate: (value, meta, record) ->
        Ext.util.Format.date(value, "d-M-Y")
      # рендер price_provision
      renderPriceProvision: (value, meta, record) ->
        value = Number(value)
        return "" if value is 0
        percent = value / 100
        price = Number(record.get("price_limit"))
        (price * percent).formatMoney(2, ",", " ")
      # рендер дат предоставления
      renderProvisionDates: (value, green_flag) ->
        formated = Ext.util.Format.date(value, "d-M-Y")

        if not green_flag or green_flag is "false"
          cd = new Date()
          cd = new Date(cd.getFullYear(), cd.getMonth(), cd.getDate())
          # два дня
          cd = (new Date(cd.getTime() + 259200000)).getTime()
          v = (new Date(value)).getTime()

          if v <= cd
            "<span style=\"color:red;\">" + formated + "</span>"
          else
            formated
        else
          return "<span style=\"color:green;\">" + formated + "</span>"
      # Рендер поля комментария
      renderCommentField: (value, meta, record) ->
        if value? and value.length > 0
          comment_date = Ext.util.Format.date(record.get("comment_date"), "d-M-Y H:i")
          return comment_date + " / " + record.get("comment_user")
        else
          return "&nbsp;"
    )

    @callParent(arguments)