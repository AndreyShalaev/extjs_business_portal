Ext.define "IPortal.view.tenders.Stuff",
  extend: "IPortal.view.tenders.Base"

  xtype: "gridTenderStuff"

  title: I18n.t "tenders.stuffs"

  store: "tenderStuffStore"

  icon: "/assets/baggage-cart-box.png"

  dockedItems: [
    xtype: "toolbar"
    dock: "top"
    items: [
      text: I18n.t "tenders.show_all"
      id: "gridTenderStuff__showAllBtn"
      icon: "/assets/eye.png"
      permissions: ["tender_show_all"]
      plugins: ["can"]
    ,
      xtype: "button"
      text: "Экспорт"
      id: "gridTenderStuff__exportBtn"
      plugins: ["can"]
      permissions: ["tender_export", "tender_manage"]
      icon: "/assets/xls.png"
    ]
  ]

  columns: [
    text: "#"
    width: 60
    dataIndex: "id"
    locked: true
    hideable: false
  ,
    text: I18n.t "activerecord.attributes.tender.customer"
    width: 200
    dataIndex: "companies.name"
    renderer: (value, meta, record) ->
      record.get("customer_fullname")
    locked: true
    hideable: false
  ,
    text: I18n.t "activerecord.attributes.tender.subject"
    width: 285
    dataIndex: "subject"
    locked: true
    hideable: false
  ,
    text: I18n.t "activerecord.attributes.tender.price_limit"
    dataIndex: "price_limit"
    hideable: false
    renderer: (value) ->
      IPortal.view.tenders.Base.renderMoney(value)
  ,
    text: I18n.t "activerecord.attributes.tender.turnaround_time"
    dataIndex: "turnaround_time"
    hideable: false
  ,
    text: I18n.t "activerecord.attributes.tender.declared_at"
    dataIndex: "declared_at"
    hideable: false
    renderer: (value) ->
      IPortal.view.tenders.Base.renderDate(value)
  ,
    text: I18n.t "activerecord.attributes.tender.opening_at"
    dataIndex: "opening_at"
    hideable: false
    renderer: (value) ->
      IPortal.view.tenders.Base.renderDate(value)
  ,
    text: I18n.t "activerecord.attributes.tender.provision"
    dataIndex: "provision_name"
    sortable: false
    hideable: false
  ,
    text: I18n.t "activerecord.attributes.tender.price_provision"
    dataIndex: "price_provision"
    sortable: false
    hideable: false
    renderer: (value, meta, record) ->
      IPortal.view.tenders.Base.renderPriceProvision(value, meta, record)
  ,
    text: I18n.t "activerecord.attributes.tender.contractors_hk"
    width: 200
    dataIndex: "contractors_hk"
    sortable: false
    hideable: false
    renderer: (value) ->
      IPortal.view.tenders.Base.renderContractors(value)
  ,
    text: ""
    xtype: "actioncolumn"
    sortable: false
    hideable: false
    permissions: ["tender_participiant"]
    plugins: ["can"]
    width: 30
    items: [
      icon: "/assets/minus-circle.png"
      tooltip: I18n.t "tenders.unaccept_part"
      align: "center"
      handler: (view, rowIdx, colIdx, item, event, record) ->
        @fireEvent("itemclick", view, rowIdx, colIdx, item, event, record)
      getClass: (v, meta, record) ->
        contractors_hk = record.get("contractors_hk")
        current_user = IPortal.user

        company_hk = current_user.get("company_hk")
        company_id = current_user.get("company_id")
        company_type = current_user.get("company_type")

        hideBtn = true

        if company_hk and company_id? and (not company_type? or company_type == "Contractor" or company_type == "")
          for index of contractors_hk
            if contractors_hk[index]["id"] == company_id
              hideBtn = false
              break
        else
          return "hideAcceptBtn"

        return (if hideBtn then "hideAcceptBtn" else "")
    ]
  ,
    text: I18n.t "activerecord.attributes.tender.link"
    dataIndex: "link"
    hideable: false
    sortable: false
    renderer: (value) ->
      if value != null and value.length > 0
        "<a href=\"" + value + "\" target=\"_blank\">" + I18n.t("follow") + "</a>"
      else
        ""
  ,
    text: I18n.t "activerecord.attributes.tender.cost_at"
    dataIndex: "cost_at"
    hideable: false
    renderer: (value, meta, record) ->
      green = record.get("cost_at_green")

      IPortal.view.tenders.Base.renderProvisionDates(value, green)
  ,
    text: ""
    xtype: "actioncolumn"
    sortable: false
    hideable: false
    permissions: ["tender_check_dates"]
    plugins: ["can"]
    width: 30
    items: [
      align: "center"
      getClass: (v, meta, record) ->
        cost_at_green = record.get("cost_at_green")
        if not cost_at_green or cost_at_green is "false"
          #@items[0].tooltip = I18n.t "tenders.provisions_at.check"
          return "check_provisions_date"
        else
          #@items[0].tooltip = I18n.t "tenders.provisions_at.uncheck"
          return "uncheck_provisions_date"
      handler: (view, rowIdx, colIdx, item, event, record) ->
        @fireEvent("cost_at_green_click", view, rowIdx, colIdx, item, event, record)
    ]
  ,
    text: I18n.t "activerecord.attributes.tender.gpr_at"
    dataIndex: "gpr_at"
    hideable: false
    renderer: (value, meta, record) ->
      green = record.get("gpr_at_green")

      IPortal.view.tenders.Base.renderProvisionDates(value, green)
  ,
    text: ""
    xtype: "actioncolumn"
    sortable: false
    hideable: false
    permissions: ["tender_check_dates"]
    plugins: ["can"]
    width: 30
    items: [
      align: "center"
      getClass: (v, meta, record) ->
        gpr_at_green = record.get("gpr_at_green")
        if not gpr_at_green or gpr_at_green is "false"
          #@items[0].tooltip = I18n.t "tenders.provisions_at.check"
          return "check_provisions_date"
        else
          #@items[0].tooltip = I18n.t "tenders.provisions_at.uncheck"
          return "uncheck_provisions_date"
      handler: (view, rowIdx, colIdx, item, event, record) ->
        @fireEvent("gpr_at_green_click", view, rowIdx, colIdx, item, event, record)
    ]
  ,
    text: I18n.t "activerecord.attributes.tender.estimates_at"
    dataIndex: "estimates_at"
    hideable: false
    renderer: (value, meta, record) ->
      green = record.get("estimates_at_green")

      IPortal.view.tenders.Base.renderProvisionDates(value, green)
  ,
    text: ""
    xtype: "actioncolumn"
    sortable: false
    hideable: false
    permissions: ["tender_check_dates"]
    plugins: ["can"]
    width: 30
    items: [
      align: "center"
      getClass: (v, meta, record) ->
        estimates_at_green = record.get("estimates_at_green")
        if not estimates_at_green or estimates_at_green is "false"
          #@items[0].tooltip = I18n.t "tenders.provisions_at.check"
          return "check_provisions_date"
        else
          #@items[0].tooltip = I18n.t "tenders.provisions_at.uncheck"
          return "uncheck_provisions_date"
      handler: (view, rowIdx, colIdx, item, event, record) ->
        @fireEvent("estimates_at_green_click", view, rowIdx, colIdx, item, event, record)
    ]
  ,
    text: I18n.t "activerecord.attributes.tender.ready_at"
    dataIndex: "ready_at"
    hideable: false
    renderer: (value, meta, record) ->
      green = record.get("ready_at_green")

      IPortal.view.tenders.Base.renderProvisionDates(value, green)
  ,
    text: ""
    xtype: "actioncolumn"
    sortable: false
    hideable: false
    permissions: ["tender_check_dates"]
    plugins: ["can"]
    width: 30
    items: [
      align: "center"
      getClass: (v, meta, record) ->
        ready_at_green = record.get("ready_at_green")
        if not ready_at_green or ready_at_green is "false"
          #@items[0].tooltip = I18n.t "tenders.provisions_at.check"
          return "check_provisions_date"
        else
          #@items[0].tooltip = I18n.t "tenders.provisions_at.uncheck"
          return "uncheck_provisions_date"
      handler: (view, rowIdx, colIdx, item, event, record) ->
        @fireEvent("ready_at_green_click", view, rowIdx, colIdx, item, event, record)
    ]
  ,
    text: I18n.t "activerecord.attributes.tender.price_cost"
    dataIndex: "price_cost"
    hideable: false
    renderer: (value) ->
      IPortal.view.tenders.Base.renderMoney(value)
  ,
    text: I18n.t "activerecord.attributes.tender.price_our"
    dataIndex: "price_our"
    hideable: false
    renderer: (value) ->
      IPortal.view.tenders.Base.renderMoney(value)
  ,
    text: I18n.t "activerecord.attributes.tender.comments"
    sortable: false
    hideable: false
    width: 150
    dataIndex: "comment"
    renderer: (value, meta, record) ->
      IPortal.view.tenders.Base.renderCommentField(value, meta, record)
  ]
