Ext.define "IPortal.view.tenders.InfoStuff",
  extend: "Ext.window.Window"

  xtype: "infoTenderStuff"

  title: "&nbsp;"

  layout: "fit"

  width: 640

  modal: true

  closeAction: "destroy"

  shadow: false

  ghost: false

  autoScroll: true

  icon: "/assets/page.png"

  items: [
    xtype: "tabpanel"
    activeTab: 0
    shadow: false
    id: "infoTenderStuff__tabPanel"
    items: [
      title: "Информация"
      xtype: "form"
      layout: "anchor"
      defaultType: "displayfield"
      bodyPadding: 10
      defaults:
        anchor: "100%"
      fieldDefaults:
        labelAlign: "top"
        labelStyle: "font-weight:bold;"
      dockedItems: [
        xtype: "toolbar"
        dock: "bottom"
        items: [
          xtype: "tbfill"
        ,
          text: I18n.t "edit"
          id: "infoTenderStuff__editBtn"
          permissions: ["tender_manage"]
          plugins: ["can"]
        ,
          text: I18n.t "tenders.move_archive"
          id: "infoTenderStuff__moveArchiveBtn"
          icon: "/assets/winrar_add.png"
          permissions: ["tender_move_archive", "tender_manage"]
          plugins: ["can"]
        ,
          text: I18n.t "close"
          handler: () ->
            @up("tabpanel").ownerCt.close()
        ]
      ]
      items: [
        fieldLabel: I18n.t "activerecord.attributes.tender.subject"
        name: "subject"
      ,
        fieldLabel: I18n.t "activerecord.attributes.tender.customer"
        name: "customer_name"
      ,
        fieldLabel: I18n.t "activerecord.attributes.tender.price_limit"
        name: "price_limit"
        renderer: (value) ->
          if value > 0
            Number(value).formatMoney(2, ",", " ")
          else
            return ""
      ,
        fieldLabel: I18n.t "activerecord.attributes.tender.turnaround_time"
        name: "turnaround_time"
      ,
        xtype: "fieldcontainer"
        layout: "hbox"
        defaultType: "displayfield"
        items: [
          fieldLabel: I18n.t "activerecord.attributes.tender.declared_at"
          name: "declared_at"
          flex: 1
          renderer: (value) ->
            Ext.util.Format.date(value, "d-M-Y")
        ,
          fieldLabel: I18n.t "activerecord.attributes.tender.opening_at"
          name: "opening_at"
          flex: 1
          renderer: (value) ->
            Ext.util.Format.date(value, "d-M-Y")
        ]
      ,
        xtype: "fieldcontainer"
        layout: "hbox"
        items: [
          fieldLabel: I18n.t "activerecord.attributes.tender.provision"
          name: "provision_name"
          xtype: "displayfield"
          flex: 1
        ,
          fieldLabel: I18n.t "activerecord.attributes.tender.price_provision"
          name: "price_provision"
          xtype: "displayfield"
          flex: 1
          renderer: (value, field) ->
            return "" if Number(value) == 0
            percent = Number(value) / 100
            record = field.up("form").getRecord()
            price = record.get("price_limit")
            (Number(price) * percent).formatMoney(2, ",", " ")
        ]
      ,
        xtype: "fieldcontainer"
        layout: "hbox"
        items: [
          fieldLabel: I18n.t "activerecord.attributes.tender.cost_at"
          xtype: "displayfield"
          name: "cost_at"
          flex: 1
          renderer: (value) ->
            Ext.util.Format.date(value, "d-M-Y")
        ,
          fieldLabel: I18n.t "activerecord.attributes.tender.gpr_at"
          xtype: "displayfield"
          name: "gpr_at"
          flex: 1
          renderer: (value) ->
            Ext.util.Format.date(value, "d-M-Y")
        ,
          fieldLabel: I18n.t "activerecord.attributes.tender.estimates_at"
          xtype: "displayfield"
          name: "estimates_at"
          flex: 1
          renderer: (value) ->
            Ext.util.Format.date(value, "d-M-Y")
        ,
          fieldLabel: I18n.t "activerecord.attributes.tender.ready_at"
          xtype: "displayfield"
          name: "ready_at"
          flex: 1
          renderer: (value) ->
            Ext.util.Format.date(value, "d-M-Y")
        ]
      ,
        fieldLabel: I18n.t "activerecord.attributes.tender.price_cost"
        name: "price_cost"
        renderer: (value) ->
          if value > 0
            Number(value).formatMoney(2, ",", " ")
          else
            return ""
      ,
        fieldLabel: I18n.t "activerecord.attributes.tender.price_our"
        name: "price_our"
        renderer: (value) ->
          if value > 0
            Number(value).formatMoney(2, ",", " ")
          else
            return ""
      ]
    ]
  ]


