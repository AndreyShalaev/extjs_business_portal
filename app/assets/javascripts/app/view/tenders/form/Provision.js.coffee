Ext.define "IPortal.view.tenders.form.Provision",
  extend: "IPortal.view.form.Base"

  xtype: "formProvision"

  items: [
    xtype: "form"
    layout: "anchor"
    defaultType: "textfield"
    trackResetOnLoad: true
    bodyPadding: 10
    id: "formProvision__form"
    defaults:
      anchor: "100%"
    fieldDefaults:
      labelAlign: "top"
    buttons: [
      text: I18n.t "save"
      disabled: true
      formBind: true
      id: "formProvision__saveBtn"
    ,
      text: I18n.t "reset"
      id: "formProvision__resetBtn"
      handler: () ->
        @up("form").getForm().reset()
        @up("form").ownerCt.doLayout()
    ,
      text: I18n.t "close"
      handler: () ->
        @up("form").ownerCt.close()
    ]
    items: [
      fieldLabel: I18n.t "activerecord.attributes.provision.name"
      name: "name"
      allowBlank: false
      maxLength: 255
      emptyText: "Должно быть уникальным..."
    ]
  ]