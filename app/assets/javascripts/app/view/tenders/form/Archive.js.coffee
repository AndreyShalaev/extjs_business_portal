Ext.define "IPortal.view.tenders.form.Archive",
  extend: "IPortal.view.tenders.form.Stuff"

  xtype: "formTenderArchive"

  initComponent: ->
    @callParent arguments
    # Remove unecessarry
    form = @down("form").getForm()
    form.findField("price_cost").destroy()
    form.findField("price_our").destroy()

