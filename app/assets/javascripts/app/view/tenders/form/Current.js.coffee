Ext.define "IPortal.view.tenders.form.Current",
  extend: "IPortal.view.form.Base"

  xtype: "formTenderCurrent"

  items: [
    xtype: "form"
    layout: "anchor"
    defaultType: "textfield"
    trackResetOnLoad: true
    bodyPadding: 10
    id: "formTenderCurrent__form"
    defaults:
      anchor: "100%"
    fieldDefaults:
      labelAlign: "top"
    buttons: [
      text: I18n.t "save"
      disabled: true
      formBind: true
      id: "formTenderCurrent__saveBtn"
    ,
      text: I18n.t "reset"
      id: "formTenderCurrent__resetBtn"
      handler: () ->
        @up("form").getForm().reset()
        @up("form").ownerCt.doLayout()
    ,
      text: I18n.t "close"
      handler: () ->
        @up("form").ownerCt.close()
    ]
    items: [
      name: "id"
      hidden: true
    ,
      fieldLabel: I18n.t "activerecord.attributes.tender.subject"
      name: "subject"
      allowBlank: false
      xtype: "textarea"
      maxLength: 2500
    ,
      xtype: "fieldcontainer"
      layout: "hbox"
      items: [
        fieldLabel: I18n.t "activerecord.attributes.tender.customer"
        xtype: "combo"
        name: "customer_id"
        store: "customerStore"
        id: "formTenderCurrent__selectCustomer"
        emptyText: I18n.t "companies.please_choose"
        typeAhead: true
        pageSize: 50
        valueField: "id"
        displayField: "fullname"
        queryMode: "remote"
        allowBlank: false
        flex: 3
      ,
        xtype: "button"
        text: I18n.t "add"
        flex: 1
        margins: "22 0 0 6"
        id: "formTenderCurrent__addCustomerBtn"
      ]
    ,
      fieldLabel: I18n.t "activerecord.attributes.tender.price_limit"
      name: "price_limit"
      xtype: "numberfield"
      maxValue: 999999999999
      decimalSeparator: ","
      hideTrigger: true
      enableKeyEvents: true
      listeners:
        change: (cmp) ->
          window.setTimeout(->
            inputVal = cmp.getRawValue()
            unless /^\d+,\d+$/g.test(inputVal)
              inputVal = inputVal.replace(/[^0-9,]/g, "")
              cmp.setValue(inputVal)

            price_limit = cmp.getValue()

            provision = cmp.up("form").getForm().findField("price_provision")

            percent = provision.getValue()

            label = provision.getFieldLabel().replace(/\s\(.*\)$/g, "")

            if percent > 0 and percent <= 100

              result = (percent / 100) * price_limit

              label += " (" + result.formatMoney(2, ",", " ") + ")"

            provision.setFieldLabel(label)
          , 50)
    ,
      fieldLabel: I18n.t "activerecord.attributes.tender.turnaround_time"
      name: "turnaround_time"
      maxLength: 255
    ,
      xtype: "fieldcontainer"
      layout: "hbox"
      defaultType: "datefield"
      items: [
        fieldLabel: I18n.t "activerecord.attributes.tender.declared_at"
        name: "declared_at"
        format: "d-m-Y"
        editable: false
        flex: 1
        allowBlank: false
      ,
        fieldLabel: I18n.t "activerecord.attributes.tender.opening_at"
        name: "opening_at"
        format: "d-m-Y"
        editable: false
        flex: 1
        margins: "0 0 0 6"
        allowBlank: false
      ]
    ,
      fieldLabel: I18n.t "activerecord.attributes.tender.documentation"
      name: "documentation"
      maxLength: 255
    ,
      fieldLabel: I18n.t "activerecord.attributes.tender.estimates"
      name: "estimates"
      maxLength: 255
    ,
      xtype: "fieldcontainer"
      layout: "hbox"
      items: [
        fieldLabel: I18n.t "activerecord.attributes.tender.provision"
        xtype: "combo"
        name: "provision_id"
        store: "provisionStore"
        id: "formTenderCurrent__selectProvision"
        emptyText: "Выберите обеспечение..."
        typeAhead: true
        pageSize: 50
        valueField: "id"
        displayField: "name"
        queryMode: "remote"
        flex: 2
      ,
        xtype: "button"
        text: I18n.t "add"
        flex: 1
        margins: "22 0 0 6"
        id: "formTenderCurrent__addProvisionBtn"
        plugins: ["can"]
        permissions: ["provision_manage"]
      ,
        fieldLabel: I18n.t "activerecord.attributes.tender.price_provision"
        xtype: "numberfield"
        name: "price_provision"
        emptyText: "% от лимит. цены, не больше 100"
        decimalSeparator: ","
        maxValue: 100
        flex: 2
        margins: "0 0 0 6"
        listeners:
          change: (cmp) ->
            window.setTimeout(->
              inputVal = cmp.getRawValue()
              unless /^\d+,\d+$/g.test(inputVal)
                inputVal = inputVal.replace(/[^0-9,]/g, "")
                cmp.setValue(inputVal)

              value = cmp.getValue()

              label = cmp.getFieldLabel().replace(/\s\(.*\)$/g, "")

              if value > 0 and value <= 100
                price_limit = cmp.up("form").getForm().findField("price_limit").getValue()

                result = (value / 100) * price_limit

                label += " (" + result.formatMoney(2, ",", " ") + ")"

              cmp.setFieldLabel(label)
            , 50)
      ]
    ,
      fieldLabel: I18n.t "activerecord.attributes.tender.requirements"
      name: "requirements"
      maxLength: 255
    ,
      fieldLabel: I18n.t "activerecord.attributes.tender.link"
      name: "link"
      vtype: "url"
      maxLength: 255
      emptyText: "Например, http://www.example.com"
    ]
  ]

