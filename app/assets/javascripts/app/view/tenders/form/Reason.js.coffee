Ext.define "IPortal.tenders.form.Reason",
  extend: "IPortal.view.form.Base"

  xtype: "formTenderStuffReason"

  items: [
    xtype: "form"
    layout: "anchor"
    defaultType: "textarea"
    trackResetOnLoad: true
    bodyPadding: 10
    defaults:
      anchor: "100%"
    fieldDefaults:
      labelAlign: "top"
    buttons: [
      text: I18n.t "save"
      disabled: true
      formBind: true
      id: "formTenderStuffReason__saveBtn"
    ,
      text: I18n.t "reset"
      id: "formTenderStuffReason__resetBtn"
      handler: () ->
        @up("form").getForm().reset()
        @up("form").ownerCt.doLayout()
    ,
      text: I18n.t "close"
      handler: () ->
        @up("form").ownerCt.close()
    ]
    items: [
      fieldLabel: I18n.t "activerecord.attributes.tender.reason"
      name: "comment"
      allowBlank: false
      xtype: "textarea"
      maxLength: 2500
    ]
  ]
