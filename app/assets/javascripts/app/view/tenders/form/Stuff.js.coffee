Ext.define "IPortal.view.tenders.form.Stuff",
  extend: "IPortal.view.form.Base"

  xtype: "formTenderStuff"

  items: [
    xtype: "form"
    layout: "anchor"
    defaultType: "textfield"
    trackResetOnLoad: true
    bodyPadding: 10
    id: "formTender__form"
    defaults:
      anchor: "100%"
    fieldDefaults:
      labelAlign: "top"
      shadow: false
    buttons: [
      text: I18n.t "save"
      disabled: true
      formBind: true
      id: "formTender__saveBtn"
    ,
      text: I18n.t "reset"
      id: "formTender__resetBtn"
      handler: ->
        @up("form").getForm().reset()
        @up("form").ownerCt.doLayout()
    ,
      text: I18n.t "close"
      handler: ->
        @up("form").ownerCt.close()
    ]
    items: [
      name: "id"
      hidden: true
    ,
      fieldLabel: I18n.t "activerecord.attributes.tender.price_limit"
      name: "price_limit"
      xtype: "numberfield"
      maxValue: 999999999999
      decimalSeparator: ","
      hideTrigger: true
      enableKeyEvents: true
      listeners:
        change: (cmp) ->
          Ext.Number.priceClean(cmp)
    ,
      fieldLabel: I18n.t "activerecord.attributes.tender.opening_at"
      name: "opening_at"
      format: "d-m-Y"
      xtype: "datefield"
      editable: false
      allowBlank: false
    ,
      fieldLabel: I18n.t "activerecord.attributes.tender.price_cost"
      name: "price_cost"
      xtype: "numberfield"
      maxValue: 999999999999
      decimalSeparator: ","
      hideTrigger: true
      enableKeyEvents: true
      listeners:
        change: (cmp, event) ->
          Ext.Number.priceClean(cmp)
    ,
      fieldLabel: I18n.t "activerecord.attributes.tender.price_our"
      name: "price_our"
      xtype: "numberfield"
      maxValue: 999999999999
      decimalSeparator: ","
      hideTrigger: true
      enableKeyEvents: true
      listeners:
        change: (cmp, event) ->
          Ext.Number.priceClean(cmp)
    ]
  ]
