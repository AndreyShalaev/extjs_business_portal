Ext.define "IPortal.view.tenders.form.Attachment",
  extend: "IPortal.view.form.Base"

  xtype: "formAttachment"

  items: [
    xtype: "form"
    layout: "anchor"
    defaultType: "textfield"
    trackResetOnLoad: true
    bodyPadding: 10
    id: "formAttachment__form"
    defaults:
      anchor: "100%"
    fieldDefaults:
      labelAlign: "top"
    buttons: [
      text: I18n.t "save"
      disabled: true
      formBind: true
      id: "formAttachment__saveBtn"
    ,
      text: I18n.t "reset"
      id: "formAttachment__resetBtn"
      handler: () ->
        @up("form").getForm().reset()
        @up("form").ownerCt.doLayout()
    ,
      text: I18n.t "close"
      handler: () ->
        @up("form").ownerCt.close()
    ]
    items: [
      fieldLabel: I18n.t "activerecord.attributes.attachment.name"
      name: "attachment[name]"
      allowBlank: false
      maxLength: 30
    ,
      fieldLabel:I18n.t "activerecord.attributes.attachment.file"
      name: "attachment[file]"
      xtype: "filefield"
      allowBlank: false
    ]
  ]


