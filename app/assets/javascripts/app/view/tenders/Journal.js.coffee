Ext.define "IPortal.view.tenders.Journal",
  extend: "IPortal.view.tenders.Base"

  xtype: "gridTenderJournal"

  title: I18n.t "activerecord.models.audit.one"

  store: "tenderAuditStore"

  icon: "/assets/document-clock.png"

  columns: [
    text: I18n.t "activerecord.attributes.audit.created_at"
    dataIndex: "created_at"
    hideable: false
    flex: 1
    renderer: (value) ->
      Ext.util.Format.date(value, "d-M-Y H:i")
  ,
    text: I18n.t "activerecord.attributes.audit.user"
    dataIndex: "user"
    sortable: false
    hideable: false
    flex: 1
    #renderer: (value) ->
    #  return (if value? then value else "Не возможно определить")
  ,
    text: I18n.t "activerecord.attributes.audit.action"
    dataIndex: "action"
    sortable: false
    hideable: false
    flex: 1
  ,
    text: I18n.t "activerecord.attributes.audit.auditable_id"
    dataIndex: "auditable_id"
    sortable: false
    hideable: false
    flex: 1
  ,
    text: I18n.t "activerecord.attributes.tender.subject"
    dataIndex: "subject"
    flex: 5
    sortable: false
    hideable: false
  ,
    text: "Представление"
    dataIndex: "type_view"
    flex: 1
    sortable: false
    hideable: false
  ,
    text: I18n.t "activerecord.attributes.audit.remote_address"
    flex: 1
    sortable: false
    hideable: false
    dataIndex: "remote_address"
  ]