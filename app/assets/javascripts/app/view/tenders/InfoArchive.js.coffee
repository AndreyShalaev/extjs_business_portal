Ext.define "IPortal.view.tenders.InfoArchive",
  extend: "Ext.window.Window"

  xtype: "infoTenderArchive"

  title: "&nbsp;"

  layout: "fit"

  width: 640

  modal: true

  closeAction: "destroy"

  shadow: false

  ghost: false

  autoScroll: true

  icon: "/assets/page.png"

  items: [
    xtype: "tabpanel"
    activeTab: 0
    shadow: false
    id: "infoTenderArchive__tabPanel"
    items: [
      title: "Информация"
      xtype: "form"
      layout: "anchor"
      defaultType: "displayfield"
      bodyPadding: 10
      defaults:
        anchor: "100%"
      fieldDefaults:
        labelAlign: "top"
        labelStyle: "font-weight:bold;"
      dockedItems: [
        xtype: "toolbar"
        dock: "bottom"
        items: [
          xtype: "tbfill"
        ,
          text: I18n.t "edit"
          id: "infoTenderArchive__editBtn"
          permissions: ["tender_manage"]
          plugins: ["can"]
        ,
          text: I18n.t "tenders.extract_archive"
          id: "infoTenderStuff__extractArchiveBtn"
          icon: "/assets/winrar_extract.png"
          permissions: ["tender_move_archive", "tender_manage"]
          plugins: ["can"]
        ,
          text: I18n.t "close"
          handler: () ->
            @up("tabpanel").ownerCt.close()
        ]
      ]
      items: [
        fieldLabel: "Предмет лота"
        name: "subject"
      ,
        fieldLabel: "Заказчик"
        name: "customer_name"
      ,
        fieldLabel: "Лимитная цена"
        name: "price_limit"
        renderer: (value, field) ->
          if value > 0
            Number(value).formatMoney(2, ",", " ")
          else
            return ""
      ,
        fieldLabel: "Сроки выполнения работ"
        name: "turnaround_time"
      ,
        xtype: "fieldcontainer"
        layout: "hbox"
        defaultType: "displayfield"
        items: [
          fieldLabel: "Объявлен"
          name: "declared_at"
          flex: 1
          renderer: (value) ->
            Ext.util.Format.date(value, "d-M-Y")
        ,
          fieldLabel: "Вскрытие конвертов"
          name: "opening_at"
          flex: 1
          renderer: (value) ->
            Ext.util.Format.date(value, "d-M-Y")
        ]
      ,
        #fieldLabel: "Наличие проектной документации"
        #name: "documentation"
        #renderer: (value, field) ->
        #  field.hide() unless value.length
      #,
        #fieldLabel: "Наличие сметной документации"
        #name: "estimates"
        #renderer: (value, field) ->
        #  field.hide() unless value.length
      #,
        xtype: "fieldcontainer"
        layout: "hbox"
        items: [
          fieldLabel: "Победитель"
          name: "winner_name"
          xtype: "displayfield"
          flex: 1
          renderer: (value, field) ->
            "" if value.length == 0
        ,
          fieldLabel: "Стоимость заявки победителя"
          name: "price_winner"
          xtype: "displayfield"
          flex: 1
          renderer: (value, field) ->
            if value.length > 0
              Number(value).formatMoney(2, ",", " ")
            else
              ""

        ]
      #,
      #  fieldLabel: "Дополнительные требования"
      #  name: "requirements"
      #  renderer: (value, field) ->
      #    field.hide() unless value.length
      #,
        fieldLabel: "Источник"
        name: "link"
        renderer: (value, field) ->
          if value.length > 0
            "<a href=\"" + value + "\" target=\"_blank\">" + value + "</a>"
          else
            ""
      ]
    ]
  ]
