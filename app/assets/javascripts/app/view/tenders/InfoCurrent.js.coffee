Ext.define "IPortal.view.tenders.InfoCurrent",
  extend: "Ext.window.Window"

  xtype: "infoTenderCurrent"

  title: "&nbsp;"

  layout: "fit"

  width: 640

  modal: true

  closeAction: "destroy"

  shadow: false

  ghost: false

  autoScroll: true

  icon: "/assets/page.png"

  items: [
    xtype: "tabpanel"
    activeTab: 0
    shadow: false
    id: "infoTenderCurrent__tabPanel"
    items: [
      title: "Информация"
      xtype: "form"
      layout: "anchor"
      defaultType: "displayfield"
      bodyPadding: 10
      defaults:
        anchor: "100%"
      fieldDefaults:
        labelAlign: "top"
        labelStyle: "font-weight:bold;"
      dockedItems: [
        xtype: "toolbar"
        dock: "bottom"
        permissions: ["tender_manage"]
        plugins: ["can"]
        items: [
          xtype: "tbfill"
        ,
          text: I18n.t "edit"
          id: "infoTenderCurrent__editBtn"
        ,
          text: I18n.t "delete"
          id: "infoTenderCurrent__deleteBtn"
        ]
      ]
      items: [
        fieldLabel: "Предмет лота"
        name: "subject"
      ,
        fieldLabel: "Заказчик"
        name: "customer_name"
      ,
        fieldLabel: "Лимитная цена"
        name: "price_limit"
        renderer: (value) ->
          if value > 0
            Number(value).formatMoney(2, ",", " ")
          else
            return ""
      ,
        fieldLabel: "Сроки выполнения работ"
        name: "turnaround_time"
      ,
        xtype: "fieldcontainer"
        layout: "hbox"
        defaultType: "displayfield"
        items: [
          fieldLabel: "Объявлен"
          name: "declared_at"
          flex: 1
          renderer: (value) ->
            Ext.util.Format.date(value, "d-M-Y")
        ,
          fieldLabel: "Вскрытие конвертов"
          name: "opening_at"
          flex: 1
          renderer: (value) ->
            Ext.util.Format.date(value, "d-M-Y")
        ]
      ,
        fieldLabel: "Наличие проектной документации"
        name: "documentation"
        renderer: (value) ->
          return "" if value.length == 0
      ,
        fieldLabel: "Наличие сметной документации"
        name: "estimates"
        renderer: (value) ->
          return "" if value.length == 0
      ,
        xtype: "fieldcontainer"
        layout: "hbox"
        items: [
          fieldLabel: "Вид обеспечения"
          name: "provision_name"
          xtype: "displayfield"
          flex: 1
        ,
          fieldLabel: "Размер обеспечения"
          name: "price_provision"
          xtype: "displayfield"
          flex: 1
          renderer: (value, field) ->
            return "" if Number(value) == 0
            percent = Number(value) / 100
            record = field.up("form").getRecord()
            price = record.get("price_limit")
            (Number(price) * percent).formatMoney(2, ",", " ")
        ]
      ,
        fieldLabel: "Дополнительные требования"
        name: "requirements"
        renderer: (value, field) ->
          return "" if value.length == 0
      ,
        fieldLabel: "Источник"
        name: "link"
        renderer: (value, field) ->
          if value.length > 0
            "<a href=\"" + value + "\" target=\"_blank\">" + value + "</a>"
          else
            ""
      ]
    ]
  ]


