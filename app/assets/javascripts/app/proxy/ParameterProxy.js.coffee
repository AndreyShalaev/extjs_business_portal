# It`s doesn`t works :(
Ext.define "IPortal.proxy.ParameterProxy",
  extend: "Ext.data.proxy.Ajax"

  alias: "proxy.parameterproxy"

  buildUrl: (request) ->
    @substituteParameters(@callParent(arguments), request)

  substituteParameters: (url, request) ->
    url.replace(/{(.*?)}/g, (full, capture) ->
      index = capture.indexOf(".")

      dataValue = undefined

      if index is -1
        dataValue = request.operation[capture]
      else
        obj = request.operation[capture.slice(0, index)]
        console.log request

        if typeof obj.isModel isnt "undefined" and obj.isModel
          dataValue = obj.get(capture.slice(index + 1))
        else
          dataValue = obj[capture.slice(index + 1)]
      return encodeURIComponent(dataValue)
    )
