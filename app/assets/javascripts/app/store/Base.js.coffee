Ext.define "IPortal.store.Base",
  extend: "Ext.data.Store"

  remoteSort: true

  remoteFilter: true

  pageSize: 50

  autoLoad: false

  sorters: [
    property: "name"
    direction: "ASC"
  ]