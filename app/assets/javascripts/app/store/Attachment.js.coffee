Ext.define "IPortal.store.Attachment",
  extend: "IPortal.store.Base"

  storeId: "attachmentStore"

  model: "IPortal.model.Attachment"

  proxy:
    type: "rest"
    reader:
      type: "json"
      root: "attachments"
      totalProperty: "total"
