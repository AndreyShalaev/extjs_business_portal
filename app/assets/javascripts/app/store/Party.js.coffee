Ext.define "IPortal.store.Party",
  extend: "IPortal.store.Base"

  storeId: "partyStore"


  model: "IPortal.model.Party"

  sorters: [
    property: "price"
    direction: "ASC"
  ]

  proxy:
    type: "rest"
    url: "/api/parties"
    reader:
      type: "json"
      root: "parties"
      totalProperty: "total"

