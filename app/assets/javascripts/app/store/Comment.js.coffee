Ext.define "IPortal.store.Comment",
  extend: "IPortal.store.Base"

  storeId: "commentStore"

  sorters: [
    property: "comments.created_at"
    direction: "DESC"
  ]

  model: "IPortal.model.Comment"

  proxy:
    type: "rest"
    url: "/api/comments"
    reader:
      type: "json"
      root: "comments"
      totalProperty: "total"
    writer:
      type: "json"
      root: "comment"
