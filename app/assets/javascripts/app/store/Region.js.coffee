Ext.define "IPortal.store.Region",
  extend: "IPortal.store.Base"

  model: "IPortal.model.Region"

  storeId: "regionStore"
