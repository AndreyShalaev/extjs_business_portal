Ext.define "IPortal.store.Customer",
  extend: "IPortal.store.Base"

  storeId: "customerStore"

  model: "IPortal.model.Company"

  sorters: [
    property: "lft"
    direction: "ASC"
  ]

  proxy:
    type: "rest"
    url: "/api/companies/customers"
    extraParams:
      live: 1
    reader:
      type: "json"
      root: "customers"
      totalProperty: "total"
