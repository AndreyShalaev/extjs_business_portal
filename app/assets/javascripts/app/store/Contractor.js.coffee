Ext.define "IPortal.store.Contractor",
  extend: "IPortal.store.Base"

  storeId: "contractorStore"

  model: "IPortal.model.Company"

  proxy:
    type: "rest"
    url: "/api/companies/contractors"
    extraParams:
      live: 1
    reader:
      type: "json"
      root: "contractors"
      totalProperty: "total"
