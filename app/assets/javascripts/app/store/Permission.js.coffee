Ext.define "IPortal.store.Permission",
  extend: "IPortal.store.Base"

  storeId: "permissionStore"


  model: "IPortal.model.Permission"

  sorters: [
    property: "permissions.description"
    direction: "ASC"
  ]

  proxy:
    type: "rest"
    url: "/api/permissions"
    reader:
      type: "json"
      root: "permissions"
      totalProperty: "total"
