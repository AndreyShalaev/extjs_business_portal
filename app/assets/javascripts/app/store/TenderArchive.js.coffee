# Конкурсы в работе
Ext.define "IPortal.store.TenderArchive",
  extend: "IPortal.store.Base"

  storeId: "tenderArchiveStore"

  sorters: [
    property: "opening_at"
    direction: "DESC"
  ]

  model: "IPortal.model.Tender"

  proxy:
    type: "rest"
    url: "/api/tenders/archives"
    reader:
      type: "json"
      root: "tenders"
      totalProperty: "total"