# Конкурсы текущие
Ext.define "IPortal.store.TenderCurrent",
  extend: "IPortal.store.Base"

  storeId: "tenderCurrentStore"

  sorters: [
    property: "opening_at"
    direction: "DESC"
  ]

  model: "IPortal.model.Tender"

  proxy:
    type: "rest"
    url: "/api/tenders/currents"
    reader:
      type: "json"
      root: "tenders"
      totalProperty: "total"
