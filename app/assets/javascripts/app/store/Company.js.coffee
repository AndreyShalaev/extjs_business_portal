Ext.define "IPortal.store.Company",
  extend: "IPortal.store.Base"

  storeId: "companyStore"

  model: "IPortal.model.Company"

  sorters: [
    property: "lft"
    direction: "ASC"
  ]

  proxy:
    type: "rest"
    url: "/api/companies"
    extraParams:
      live: 1
    reader:
      type: "json"
      root: "companies"
      totalProperty: "total"
