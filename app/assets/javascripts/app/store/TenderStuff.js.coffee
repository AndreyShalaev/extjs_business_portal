# Конкурсы в работе
Ext.define "IPortal.store.TenderStuff",
  extend: "IPortal.store.Base"

  storeId: "tenderStuffStore"

  sorters: [
    property: "opening_at"
    direction: "ASC"
  ]

  model: "IPortal.model.Tender"

  proxy:
    type: "rest"
    url: "/api/tenders/stuffs"
    extraParams:
      show_all: 0
    reader:
      type: "json"
      root: "tenders"
      totalProperty: "total"
