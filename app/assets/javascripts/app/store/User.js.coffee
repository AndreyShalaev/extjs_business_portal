Ext.define "IPortal.store.User",
  extend: "IPortal.store.Base"

  model: "IPortal.model.User"

  storeId: "userStore"

  sorters: [
    property: "surname"
    direction: "ASC"
  ]

  proxy:
    type: "rest"
    url: "/api/users"
    extraParams:
      live: 1
    reader:
      type: "json"
      root: "users",
      totalProperty: "total"
