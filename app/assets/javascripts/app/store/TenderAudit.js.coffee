Ext.define "IPortal.store.TenderAudit",
  extend: "IPortal.store.Base"

  storeId: "tenderAuditStore"

  model: "IPortal.model.Audit"

  sorters: [
    property: "created_at"
    direction: "DESC"
  ]

  proxy:
    type: "rest"
    url: "/api/tenders/audits"
    reader:
      type: "json"
      root: "audits",
      totalProperty: "total"