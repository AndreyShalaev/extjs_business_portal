Ext.define "IPortal.store.MainMenu",
  extend: "Ext.data.TreeStore"

  model: "IPortal.model.MainMenu"

  root:
    expanded: true
    children: [
      text: "Справочники"
      alias: "Directories"
      expanded: true
      icon: "/assets/folder.png"
      children: [
        leaf: true
        text: "Организации"
        alias: "UsersAndCompanies"
        permissions: ["company_manage"]
        icon: "/assets/building.png"
      ]
    ,
      text: "Конкурсы"
      alias: "Tenders"
      expanded: true
      icon: "/assets/folder.png"
      children: [
        leaf: true
        text: "Текущие"
        alias: "Current"
        permissions: [
          "tender_read"
          "tender_manage"
          "tender_participiant"
          "comment_read"
          "comment_manage"
        ]
        icon: "/assets/baggage-cart.png"
      ,
        leaf: true
        text: "В работе"
        alias: "Stuff"
        permissions: [
          "tender_read"
          "tender_manage"
          "tender_participiant"
          "tender_move_archive"
          "comment_read"
          "comment_manage"
        ]
        icon: "/assets/baggage-cart-box.png"
      ,
        leaf: true
        text: "Архив"
        alias: "Archive"
        permissions: [
          "tender_read"
          "tender_manage"
          "comment_read"
          "comment_manage"
        ]
        icon: "/assets/wooden-box.png"
      ,
        leaf: true
        text: "Экспорт конкурсов"
        permissions: ["tender_export","tender_manage"]
        alias: "Export"
        icon: "/assets/xls.png"
      ,
        leaf: true
        text: I18n.t "activerecord.models.audit.one"
        permissions: ["tender_journal","tender_manage"]
        alias: "Journal"
        icon: "/assets/document-clock.png"
      #,
      #  leaf: true
      #  text: "Настройки"
      #  permissions: ["tender_settings","tender_manage"]
      #  alias: "Settings"
      ]
    ,
      text: "База знаний"
      alias: "Database"
      expanded: true
      icon: "/assets/folder.png"
      children: [
        leaf: true
        text: "Документы и файлы"
        permissions: ["knownbase_read"]
        alias: "Files"
      ]
    ,
      text: "Управление"
      alias: "Manage"
      expanded: true
      icon: "/assets/folder.png"
      children: [
        leaf: true
        text: "Доступ пользователей"
        alias: "Acl"
        permissions: ["admin_manage"]
        icon: "/assets/users.png"
      ,
        leaf: true
        text: "Права доступа"
        alias: "Privilege"
        permissions: ["admin_manage"]
        icon: "/assets/application_key.png"
      ]
    ]
