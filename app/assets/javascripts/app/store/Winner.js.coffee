Ext.define "IPortal.store.Winner",
  extend: "IPortal.store.Base"

  storeId: "winnerStore"

  model: "IPortal.model.Company"

  proxy:
    type: "rest"
    url: "/api/companies/winners"
    extraParams:
      live: 1
    reader:
      type: "json"
      root: "winners"
      totalProperty: "total"
