Ext.define "IPortal.controller.directories.UsersAndCompanies",
  extend: "Ext.app.Controller"

  id: "directories__usersAndCompanies" # Уникальный id контроллера

  refs: [
    ref: "dirMainContainer"
    selector: "dirMainContainer"
  ,
    ref: "gridCompanies"
    selector: "gridCompanies"
  ,
    ref: "deleteCompanyBtn"
    selector: "gridCompanies #deleteCompany"
  ,
    ref: "infoContainer"
    selector: "containerInfo"
  ,
    ref: "formCompany"
    selector: "formCompany"
  ,
    ref: "searchField"
    selector: "gridCompanies #gridCompanies__searchField"
  ]

  destroy: () ->
    @getInfoContainer().clearListeners()
    @clearListeners()
    storeCompany = Ext.data.StoreManager.lookup("companyStore")

    proxyCompany = storeCompany.getProxy()
    proxyCompany.url = "/api/companies"
    proxyCompany.extraParams =
      live: 1

    @callParent(arguments)

  recentPage: 1

  init: () ->
    infoContainerListener = null

    #companyStore = Ext.data.StoreManager.lookup("companyStore")
    #companyStore.getProxy().extraParams = {}

    @control(
      "gridCompanies":
        select: (rowModel, record, index, event) ->
          #здесь мы должны показать пользователей этой компании

          # чистим панель инфо
          infoContainer = @getInfoContainer()

          if typeof infoContainerListener isnt "undefined" and infoContainerListener
            infoContainerListener.destroy()

          infoContainerListener = infoContainer.on("add", (cnt, cmp) ->
            cmp.loadRecord(record)
          , @, destroyable: true)

          infoContainer.removeAll(true)

          #получаем необходимые компоненты
          #gridUsers = @getGridUsers()

          #userStore = gridUsers.getStore()

          #company_id = record.get("id")

          deleteCompanyBtn = @getDeleteCompanyBtn()

          #userStore.load(
          #  params:
          #    company_id: company_id
          #)

          deleteCompanyBtn.setDisabled(false)

          #информация о компании
          companyInfo = Ext.create("IPortal.view.directories.InfoCompany")

          infoContainer.add(companyInfo)

        afterrender: (cmp, event) ->
          #gridUsers = @getGridUsers()

          cmp.getStore().load( (records, operation, success) ->
            #gridUsers.getStore().load()
            return
          )
      # Удаление компании
      "gridCompanies #deleteCompany":
        click: () ->
          record = @getGridCompanies().getSelectionModel().getSelection()
          that = @

          if record and record[0] and window.confirm("Вы уверены?")
            record[0].destroy(
              success: (record) ->
                if typeof infoContainerListener isnt "undefined" and infoContainerListener
                  Ext.destroy(infoContainerListener)

                that.getInfoContainer().removeAll(true)
                that.getInfoContainer().add(Ext.create("IPortal.view.directories.Info"))

                Ext.data.StoreManager.lookup("companyStore").reload()

                Ext.IPortal().msg(record.get("name"), "Организация успешно удалена")
              failure: () ->
                Ext.IPortal().msg("Ошибка", "Ошибка при удалении организации")
            )

          false
      "formCompany #formCompany__activities":
        autosize: () ->
          @getFormCompany().doLayout()
      # Добавление огранизации
      "gridCompanies #newCompany":
        click: () ->
          formCompany = Ext.create "IPortal.view.directories.form.Company"
          formCompany.setTitle "Добавление организации"
          formCompany.down("#formCompany__deleted").show()
          formCompany.down("#formCompany__form").loadRecord(Ext.create("IPortal.model.Company"))
          formCompany.show()

          false
      # Изменение организации
      "companyInfo #companyInfo__editBtn":
        click: () ->
          formCompany = Ext.create "IPortal.view.directories.form.Company"
          formCompany.setTitle "Изменение организации"
          record = @getGridCompanies().getSelectionModel().getSelection()
          form = formCompany.down("#formCompany__form")

          selectRegion = form.down("#formCompany__selectRegion")

          formCompany.show()
          formCompany.setLoading(true)

          selectRegion.getStore().load(
            params:
              query: record[0].get("region_name")
            callback: () ->
              form.loadRecord record[0]
              form.down("#formCompany__resetBtn").setDisabled(true)
              formCompany.setLoading(false)
              form.down("#formCompany__saveBtn").setDisabled(!form.isValid())
          )

          false
      "formCompany #formCompany__saveBtn":
        click: (cmp) ->
          that = @
          @getFormCompany().submitForm (record) ->
            if typeof infoContainerListener isnt "undefined" and infoContainerListener
              Ext.destroy(infoContainerListener)

              that.getInfoContainer().removeAll(true)
              that.getInfoContainer().add(Ext.create("IPortal.view.directories.Info"))

              Ext.data.StoreManager.lookup("companyStore").reload()

          return false
      # Search companies
      "gridCompanies #gridCompanies__searchField":
        specialkey: (field, event) ->
          if event.getKey() == event.ENTER
            storeCompany = Ext.data.StoreManager.lookup("companyStore")
            proxyCompany = storeCompany.getProxy()

            query = field.getValue()

            @recentPage = storeCompany.currentPage

            if query.length >= 3
              proxyCompany.url = "/api/companies/search"
              proxyCompany.extraParams =
                live: 1
                query: field.getValue()

              storeCompany.loadPage(1,
                callback: ->
                  proxyCompany.url = "/api/companies"
                  proxyCompany.extraParams =
                    live: 1
              )
      "gridCompanies #gridCompanies__searchResetBtn":
        click: ->
          storeCompany = Ext.data.StoreManager.lookup("companyStore")
          storeCompany.loadPage(@recentPage,
            callback: ->
              @getSearchField().setValue("")
            scope: @
          )

          return false
    )
