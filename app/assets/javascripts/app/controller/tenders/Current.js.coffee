Ext.define "IPortal.controller.tenders.Current",
  extend: "IPortal.controller.tenders.Base"

  #Уникальный id контроллера
  id: "tenders__current"

  refs: [
    # Таблица
    ref: "gridTenderCurrent"
    selector: "gridTenderCurrent"
  ,
    # Восточная панель (где фильтры)
    ref: "eastPanel"
    selector: "#eastPanel"
  ,
    # Форма фильтра
    ref: "tenderCurrentFilter"
    selector: "tenderCurrentFilter"
  ,
    # Кнопка удаления конкурса
    ref: "deleteTenderBtn"
    selector: "gridTenderCurrent #gridTenderCurrent__deleteBtn"
  ,
    # Форма конкурса
    ref: "formTenderCurrent"
    selector: "formTenderCurrent"
  ,
    # Информация о конкурсе
    ref: "infoTenderCurrent"
    selector: "infoTenderCurrent"
  ,
    # Форма комментария
    ref: "formComments"
    selector: "formComments"
  ,
    # Форма видов обеспечений
    ref: "formProvision"
    selector: "formProvision"
  ,
    ref: "gridComments"
    selector: "gridComments"
  ]

  filterName: "IPortal.view.tenders.filter.Current"

  destroy: ->
    Ext.data.StoreManager.lookup("tenderCurrentStore").clearFilter(true)
    @callParent(arguments)

  init: ->

    that = @

    @control
      "gridTenderCurrent":
        afterrender: (grid) ->
          grid.getStore().load(
            callback: ->
              #добавляем панель фильтров
              that.addPanelFilter()
          )
        select: ->
          deleteBtn = @getDeleteTenderBtn()
          deleteBtn.setDisabled(false)
        # Выделение двойным кликом: информация о конкурсе
        celldblclick: (cell, td, cellIndex, record) ->
          tender_id = record.get("id")

          tenderInfo = Ext.create "IPortal.view.tenders.InfoCurrent"
          title = "Конкурс №" + record.get("id") + ". " + record.get("subject")

          tenderInfo.down("form").loadRecord(record)

          tenderInfo.setTitle title
          tenderInfo.show()

          # Подгружаем комментарии
          commentsGrid = Ext.create "IPortal.view.comments.Comment"
          tenderInfo.down("#infoTenderCurrent__tabPanel").add(commentsGrid)

          storeComments = Ext.data.StoreManager.lookup("commentStore")

          storeComments.getProxy().extraParams =
            type: "Tender"
            commentable_id: tender_id

          @commentReloadListener = storeComments.on "load", ->
            count_comments = storeComments.count()
            commentsGrid.setTitle(I18n.t("activerecord.models.comment.other") + "(" + count_comments + ")")

            record = @getGridTenderCurrent().getView().getSelectionModel().getSelection()[0]

            tender = null

            tender = Ext.data.StoreManager.lookup("tenderCurrentStore").getById(record.getId()) if record?

            if tender?
              if count_comments is 0
                tender.set("comment", null)
              else
                prev_comment = storeComments.first()

                tender.set("comment", prev_comment.get("comment"))
                tender.set("comment_date", prev_comment.get("created_at"))
                tender.set("comment_user", prev_comment.get("user_name"))
          , @, destroyable: true

          storeComments.load()
        # Конец выделения двойным кликом

      # Принятие участия
      "gridTenderCurrent actioncolumn":
        itemclick: (view, rowIdx, colIdx, item, event, record) ->
          view.getSelectionModel().select(rowIdx)
          # todo: Транслетиризация
          sure_string = "Вы уверены, что хотите принять участие в конкурсе №" + record.get("id") + " "
          sure_string += Ext.String.ellipsis(record.get("subject"), 50, true) + "?"

          if window.confirm sure_string
            contractors_hk = record.get("contractors_hk")
            contractors_hk_attrs = record.get("tender_contractors_attributes")

            contractors_hk_attrs = [] unless typeof contractors_hk_attrs.push is "function"

            current_user = IPortal.user

            contractors_hk.push
              id: current_user.get("company_id")
              name: current_user.get("company_name")
              fullname: current_user.get("company_fullname")

            contractors_hk_attrs.push
              company_id: current_user.get("company_id")
              _destroy: 0

            record.set("contractors_hk", contractors_hk)
            record.set("tender_contractors_attributes", contractors_hk_attrs)

            that = @
            record.save(
              success: ->
                Ext.IPortal().msg("", "Вы успешно приняли участие!")
                that.getGridTenderCurrent().getView().refresh()
              failure: ->
                that.getGridTenderCurrent().getStore().reload
                  callback: ->
                    Ext.IPortal().msg("", "Ошибка при добавлении участника, попробуйте позже.")
            )


          return false

      # Экспорт
      "gridTenderCurrent #gridTenderCurrent__exportBtn":
        click: ->
          store = Ext.data.StoreManager.lookup("tenderCurrentStore")
          @exportTenders store
          return false

      # Добавление
      "gridTenderCurrent #gridTenderCurrent__addBtn":
        click: ->

          tenderForm = Ext.create "IPortal.view.tenders.form.Current"

          tenderForm.setTitle "Добавление конкурса"

          tenderForm.down("form").loadRecord(Ext.create("IPortal.model.Tender"))

          tenderForm.show()

          return false
      # Редактирование
      "infoTenderCurrent #infoTenderCurrent__editBtn":
        click: ->
          tenderForm = Ext.create "IPortal.view.tenders.form.Current"

          tenderForm.setTitle "Редактирование конкурса"

          record = @getGridTenderCurrent().getView().getSelectionModel().getSelection()

          tenderForm.show()

          selectCustomer = tenderForm.down("#formTenderCurrent__selectCustomer")

          selectCustomer.getStore().load(
            params:
              query: record[0].get("customer_name")
            callback: ->
              tenderForm.down("form").loadRecord(record[0])
              tenderForm.down("#formTenderCurrent__resetBtn").setDisabled(true)
          )

          return false

      # Сохранение конкурса
      "formTenderCurrent #formTenderCurrent__saveBtn":
        click: ->
          @getFormTenderCurrent().submitForm (record) ->
            Ext.data.StoreManager.lookup("tenderCurrentStore").reload(
              callback: ->
                selection = that.getGridTenderCurrent().getView().getSelectionModel()
                selection.select(record)

                that.getInfoTenderCurrent().down("form").getForm().loadRecord(record) if that.getInfoTenderCurrent()?
            )
          return false
      # Конец сохранения конкурса

      # Удаление конкурса
      "gridTenderCurrent #gridTenderCurrent__deleteBtn":
        click: ->
          if window.confirm("Вы уверены?")
            record = @getGridTenderCurrent().getView().getSelectionModel().getSelection()
            if record
              record[0].destroy()
              #Ext.data.StoreManager.lookup("tenderCurrentStore").reload()
          return false


      # Добавление заказчика в список
      "formTenderCurrent #formTenderCurrent__addCustomerBtn":
        click: ->
          formCompany = Ext.create "IPortal.view.directories.form.Company"
          formCompany.setTitle "Добавление заказчика"
          model = Ext.create("IPortal.model.Company")
          model.set("type", "Customer")
          form = formCompany.down("form").getForm()
          form.loadRecord(model)
          formCompany.down("#formCompany__selectType").hide()
          formCompany.down("#formCompany__fieldHk").hide()
          formCompany.show()
          return false
      # Конец добавления заказчика в список

      # Сохранение заказчика
      "formCompany #formCompany__saveBtn":
        click: (cmp) ->
          tenderForm = @getFormTenderCurrent()

          form = cmp.up("form").getForm()
          form.updateRecord()

          form.getRecord().save(
            success: (record, operation) ->

              selectCustomer = tenderForm.down("#formTenderCurrent__selectCustomer")

              selectCustomer.getStore().load(
                params:
                  query: record.get("name")
                callback: ->
                  selectCustomer.select(record)
              )

              Ext.IPortal().msg(record.get("name"), "Организация успешно сохранена")
              cmp.up("form").ownerCt.close()
            failure: (record) ->
              Ext.IPortal().msg("Ошибка при сохранении", "Проверьте введенные данные")
          )

          false
      # Конец сохранения заказчика

      # Добавление вида обеспечения в список
      "formTenderCurrent #formTenderCurrent__addProvisionBtn":
        click: ->
          formProvision = Ext.create "IPortal.view.tenders.form.Provision"
          formProvision.setTitle "Добавление вида обеспечения"
          model = Ext.create "IPortal.model.Provision"
          form = formProvision.down("form").getForm()
          form.loadRecord(model)
          formProvision.show()
          return false
      # Сохраняем вид обеспечения
      "formProvision #formProvision__saveBtn":
        click: ->
          formProvision = @getFormProvision()
          formTenderCurrent = @getFormTenderCurrent()

          provisionStore = Ext.data.StoreManager.lookup("provisionStore")

          formProvision.submitForm (record) ->
            selectProvision = formTenderCurrent.down("#formTenderCurrent__selectProvision")

            provisionStore.load(
              params:
                query: record.get("name")
              callback: ->
                selectProvision.select(record)
                formProvision.close()
            )

          return false

      # Фильтрация
      "tenderCurrentFilter #filter__sendBtn":
        click: ->
          form = @getTenderCurrentFilter().getForm()
          values = form.getValues()

          store = Ext.data.StoreManager.lookup "tenderCurrentStore"
          filter = []

          for property of values
            if typeof property isnt "undefined" and ("" + values[property]).length > 0
              filter.push(
                property: property
                value: values[property]
              )

          store.clearFilter(true)
          store.filter(filter)

          return false

      "tenderCurrentFilter #filter__resetBtn":
        click: ->
          store = Ext.data.StoreManager.lookup "tenderCurrentStore"
          store.clearFilter()
          form = @getTenderCurrentFilter().getForm()
          form.reset()
          return false
      # Для фильтров сбрасываем флаг live
      "tenderCurrentFilter combo":
        beforequery: (queryEvent) ->
          @clearFilterComboStore(queryEvent)

      "infoTenderCurrent":
        beforeclose: (cmp) ->
          storeComments = Ext.data.StoreManager.lookup("commentStore")
          storeComments.getProxy().extraParams = {}

      # Оставляем комментарий
      "infoTenderCurrent #gridComments__addBtn":
        click: ->
          # Создаем форму комментариев
          formComments = Ext.create "IPortal.view.comments.Form"
          formComments.down("form").loadRecord(Ext.create("IPortal.model.Comment"))
          formComments.show()
          return false
      # Отправка комментария
      "formComments #formComments__saveBtn":
        click: (cmp) ->
          form = cmp.up("form").getForm()
          form.updateRecord()
          record = form.getRecord()
          storeComments = Ext.data.StoreManager.lookup("commentStore")
          storeComments.add([record])
          storeComments.sync
            success: ->
              Ext.IPortal().msg("", "Комментарий успешно добавлен")
              cmp.up("form").ownerCt.close()
              storeComments.load()
            failure: ->
              Ext.IPortal().msg("Ошибка", "Ошибка при добавлении комментария")
            scope: @
          return false
      # Удаление комментария
      "infoTenderCurrent #gridComments__deleteBtn":
        click: (cmp) ->
          record = @getGridComments().getView().getSelectionModel().getSelection()[0]
          store = Ext.data.StoreManager.lookup("commentStore")
          selectedComment = store.getById(record.getId())

          that = @

          if window.confirm(I18n.t("are_you_sure")) and selectedComment?
            selectedComment.getProxy().extraParams =
              type: "Tender"
              commentable_id: selectedComment.get("commentable_id")

            selectedComment.destroy(
              callback: ->
                store.load()
              success: ->
                Ext.IPortal().msg("", I18n.t("comments.deleted"))
                cmp.setDisabled(true)
              failure: ->
                Ext.IPortal().msg("", I18n.t("comments.not_deleted"))
              scope: @
            )

          return false
      "gridComments":
        select: ->
          @getGridComments().down("#gridComments__deleteBtn").setDisabled(false)
