Ext.define "IPortal.controller.tenders.Journal",
  extend: "Ext.app.Controller"

  id: "tenders__journal"

  refs: [
    ref: "gridTenderJournal"
    selector: "gridTenderJournal"
  ,
    ref: "infoJournal"
    selector: "infoJournal"
  ]

  init: ->
    @control
      "gridTenderJournal":
        afterrender: ->
          Ext.data.StoreManager.lookup("tenderAuditStore").load()
        celldblclick: (cell, td, cellIndex, record) ->
          info = Ext.create "IPortal.view.tenders.JournalInfo"
          title = record.get("action") + ": " + "Конкурс №" + record.get("auditable_id")
          info.setTitle title

          audited_changes = record.get("audited_changes")

          action = record.get("action")

          changes_data = []

          for key of audited_changes
            item = audited_changes[key]
            i18n_item = "<b>" + I18n.t("activerecord.attributes.tender." + key) + "</b>"
            
            data_item = []

            if (item instanceof Array) and item[0] != item[1] and item[0] != "" and item[1] != null and (not /missing/g.test(i18n_item))
              data_item.push i18n_item

              item[0] = Ext.util.Format.date(item[0], "d-M-Y") if /^\d{4}\-\d{2}\-\d{2}/g.test(item[0])
              item[1] = Ext.util.Format.date(item[1], "d-M-Y") if /^\d{4}\-\d{2}\-\d{2}/g.test(item[1])
              item[0] = Number(item[0]).formatMoney(2, ",", " ") if /price/g.test(key)
              item[1] = Number(item[1]).formatMoney(2, ",", " ") if /price/g.test(key)

              data_item.push item[0]
              data_item.push item[1]              
            else if not (item instanceof Array) and not /missing/g.test(i18n_item) and item != null and (not /contractor/g.test(key))
              data_item.push i18n_item
              
              item = item + "%" if key == "price_provision"
              item = Number(item).formatMoney(2, ",", " ") if /price/g.test(key) and not key == "price_provision"
              item = Ext.util.Format.date(item, "d-M-Y") if /^\d{4}\-\d{2}\-\d{2}/g.test(item)

              unless action == "Удаление"
                data_item.push ""
                data_item.push item
              else
                data_item.push item
                data_item.push ""
            changes_data.push data_item if data_item.length > 0

          if changes_data.length > 0
            storeChanges = Ext.create "Ext.data.ArrayStore",
              model: "IPortal.model.TenderChanges"
              data: changes_data  

            info.down("grid").reconfigure(storeChanges)

            info.show()