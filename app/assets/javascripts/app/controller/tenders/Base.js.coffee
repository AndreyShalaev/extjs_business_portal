Ext.define "IPortal.controller.tenders.Base",
  extend: "Ext.app.Controller"

  # фльтр по-умолчанию
  filterName: "IPortal.view.tenders.filter.Current"

  refreshListener: null

  commentReloadListener: null

  destroy: () ->
    that = @
    setTimeout(->
      eastPanel = that.getEastPanel()
      eastPanel.removeAll(true)
      eastPanel.hide()
    , 50)
    @commentReloadListener.destroy() if @commentReloadListener?
    @refreshListener.destroy() if @refreshListener?
    @callParent(arguments)

  addPanelFilter: () ->
    filter = Ext.create(@filterName)
    eastPanel = @getEastPanel()
    eastPanel.add(filter)
    eastPanel.setTitle("Фильтр")
    eastPanel.show()

  syncGridRowsHeights: (grid) ->
    lockedView = grid.lockedGrid.getView()
    normalView = grid.normalGrid.getView()
    lockedRowEls = lockedView.all.slice()
    normalRowEls = normalView.all.slice()

    ln = lockedRowEls.length

    i = 0
    while i < ln
      lockedGridRow = Ext.get(lockedRowEls[i])
      normalGridRow = Ext.get(normalRowEls[i])

      lockedRowHeight = lockedGridRow.dom.offsetHeight
      normalRowHeight = normalGridRow.dom.offsetHeight

      if lockedRowHeight > normalRowHeight
        normalGridRow.setHeight(lockedRowHeight)
      else if normalRowHeight > lockedRowHeight
        lockedGridRow.setHeight(normalRowHeight)

      i++

  overrideFinishLayout: () ->
    Ext.override(Ext.view.TableLayout,
      finishedLayout: () ->
        @lockedGrid.syncRowHeight = false if @lockedGrid
        @callParent(arguments)
    )

  clearFilterComboStore: (queryEvent) ->
    if typeof queryEvent.combo isnt "undefined"
      store = queryEvent.combo.getStore()

      storeListener = store.on "load", (store, records, successful) ->
        proxy = store.getProxy()
        if successful and proxy.extraParams and typeof proxy.extraParams.live isnt "undefined" and proxy.extraParams.live == 0
          proxy.extraParams.live = 1
        storeListener.destroy()
      , @, destroyable: true


      proxy = store.getProxy()

      if proxy.extraParams and typeof proxy.extraParams.live isnt "undefined" and proxy.extraParams.live == 1
        proxy.extraParams.live = 0

  exportTenders: (store) ->
    url = store.getProxy().url + ".xlsx"
    params = store.getProxy().extraParams
    filters = []
    store.filters.each (item) ->
      filters.push
        property: item.property
        value: item.value

    params = Ext.Object.merge params,
      filter: filters

    url = url + "?" + Ext.Object.toQueryString params, true

    window.open(url, "_blank")
