Ext.define "IPortal.controller.tenders.Export",
  extend: "Ext.app.Controller"

  id: "tenders__export"

  refs: [
    ref: "formTenderExport"
    selector: "formTenderExport"
  ]

  init: ->
    @control
      "formTenderExport #formTenderExport__exportBtn":
        click: ->
          period = @getFormTenderExport().down("#formTenderExport__period").getValue()
          window.location.href = "/api/tenders.xlsx?days=" + period
          return false
