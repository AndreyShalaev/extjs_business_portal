Ext.define "IPortal.controller.tenders.Stuff",
  extend: "IPortal.controller.tenders.Base"

  id: "tenders__stuff"

  refs: [
    ref: "gridTenderStuff"
    selector: "gridTenderStuff"
  ,
    ref: "eastPanel"
    selector: "#eastPanel"
  ,
    ref: "tenderStuffFilter"
    selector: "tenderStuffFilter"
  ,
    # Информация о конкурсе
    ref: "infoTenderStuff"
    selector: "infoTenderStuff"
  ,
    # Форма комментария
    ref: "formComments"
    selector: "formComments"
  ,
    # Форма причины отказа от участия
    ref: "formTenderStuffReason"
    selector: "formTenderStuffReason"
  ,
    ref: "formTenderStuff"
    selector: "formTenderStuff"
  ,
    # Таблица комментариев
    ref: "gridComments"
    selector: "gridComments"
  ]

  filterName: "IPortal.view.tenders.filter.Stuff"

  destroy: () ->
    store = Ext.data.StoreManager.lookup("tenderStuffStore")
    store.clearFilter(true)
    # todo: Возможно необходимо будет сохранять состояние между переходами
    store.getProxy().extraParams =
      show_all: 0
    @callParent(arguments)

  init: () ->
    that = @
    countRefresh = 0

    @control
      "gridTenderStuff":
        lockcolumn: (grid) ->
          @syncGridRowsHeights(grid)
        unlockcolumn: (grid) ->
          @syncGridRowsHeights(grid)
        columnresize: (ct) ->
          @syncGridRowsHeights(ct.up("gridTenderStuff"))
        afterrender: (grid) ->

          @overrideFinishLayout()

          @refreshListener = grid.getView().on("refresh", (viewGrid) ->
            if countRefresh == 3

              @syncGridRowsHeights(grid)

              @refreshListener.destroy()
              @refreshListener = null
              countRefresh = 0

              @refreshListener = grid.getView().on("refresh", (viewGrid) ->
                if countRefresh == 2
                  @syncGridRowsHeights(grid)
                  countRefresh = 0
                countRefresh++
              , that, destroyable: true)

            countRefresh++

          , @, destroyable: true)

          grid.getStore().load(
            callback: () ->
              #добавляем панель фильтров
              that.addPanelFilter()
          )
        # Выделение двойным кликом: информация о конкурсе
        celldblclick: (cell, td, cellIndex, record) ->
          tender_id = record.get("id")

          tenderInfo = Ext.create "IPortal.view.tenders.InfoStuff"
          title = "Конкурс №" + record.get("id") + ". " + record.get("subject")

          tenderInfo.down("form").loadRecord(record)

          tenderInfo.setTitle title
          tenderInfo.show()

          # Подгружаем комментарии
          commentsGrid = Ext.create "IPortal.view.comments.Comment"
          tenderInfo.down("#infoTenderStuff__tabPanel").add(commentsGrid)

          storeComments = Ext.data.StoreManager.lookup("commentStore")

          storeComments.getProxy().extraParams =
            type: "Tender"
            commentable_id: tender_id

          @commentReloadListener = storeComments.on "load", ->
            count_comments = storeComments.count()

            commentsGrid.setTitle(I18n.t("activerecord.models.comment.other") + "(" + count_comments + ")")

            record = @getGridTenderStuff().getView().getSelectionModel().getSelection()[0]

            tender = null

            tender = Ext.data.StoreManager.lookup("tenderStuffStore").getById(record.getId()) if record?

            if tender?
              if count_comments is 0
                tender.set("comment", null)
              else
                prev_comment = storeComments.first()

                tender.set("comment", prev_comment.get("comment"))
                tender.set("comment_date", prev_comment.get("created_at"))
                tender.set("comment_user", prev_comment.get("user_name"))
              @syncGridRowsHeights(@getGridTenderStuff())
          , @, destroyable: true

          storeComments.load()
        # Конец выделения двойным кликом

      # Показываем все конкурсы
      "gridTenderStuff #gridTenderStuff__showAllBtn":
        click: (cmp) ->

          store = Ext.data.StoreManager.lookup("tenderStuffStore")
          show_all = store.getProxy().extraParams.show_all

          cmp.setDisabled(true)

          store.getProxy().extraParams =
            show_all: (1 - show_all)

          store.reload(
            callback: () ->
              if show_all == 0
                cmp.setIcon("/assets/eye-close.png")
                cmp.setText(I18n.t("tenders.hide_all"))
              else
                cmp.setIcon("/assets/eye.png")
                cmp.setText(I18n.t("tenders.show_all"))
              cmp.setDisabled(false)
          )

          return false

      # Edit
      "infoTenderStuff #infoTenderStuff__editBtn":
        click: () ->
          tenderForm = Ext.create "IPortal.view.tenders.form.Stuff"

          tenderForm.setTitle "Редактирование конкурса"

          record = @getGridTenderStuff().getView().getSelectionModel().getSelection()

          tenderForm.down("form").getForm().loadRecord(record[0])

          tenderForm.show()

          tenderForm.down("#formTender__resetBtn").setDisabled(true)

          return false

      # Save
      "formTenderStuff #formTender__saveBtn":
        click: ->
          that = @
          @getFormTenderStuff().submitForm (record) ->
            Ext.data.StoreManager.lookup("tenderStuffStore").reload(
              callback: ->
                selection = that.getGridTenderStuff().getView().getSelectionModel()
                selection.select(record)

                that.getInfoTenderStuff().down("form").getForm().loadRecord(record)
            )

          return false

      # Эскпорт конкурсов
      "gridTenderStuff #gridTenderStuff__exportBtn":
        click: () ->
          stuffStore = Ext.data.StoreManager.lookup("tenderStuffStore")
          @exportTenders stuffStore

          return false

      "tenderStuffFilter #filter__sendBtn":
        click: () ->

          form = @getTenderStuffFilter().getForm()
          values = form.getValues()

          store = Ext.data.StoreManager.lookup "tenderStuffStore"
          filter = []

          for property of values
            if typeof property isnt "undefined" and ("" + values[property]).length > 0
              filter.push(
                property: property
                value: values[property]
              )

          store.clearFilter(true)
          store.filter(filter)

          return false

      "tenderStuffFilter #filter__resetBtn":
        click: () ->
          store = Ext.data.StoreManager.lookup "tenderStuffStore"
          store.clearFilter()
          form = @getTenderStuffFilter().getForm()
          form.reset()
          return false

      # Для фильтров сбрасываем флаг live
      "tenderStuffFilter combo":
        beforequery: (queryEvent) ->
          @clearFilterComboStore(queryEvent)

      "infoTenderStuff":
        beforeclose: (cmp) ->
          storeComments = Ext.data.StoreManager.lookup("commentStore")
          storeComments.getProxy().extraParams = {}
      "gridComments":
        select: ->
          @getGridComments().down("#gridComments__deleteBtn").setDisabled(false)
      # Оставляем комментарий
      "infoTenderStuff #gridComments__addBtn":
        click: ->
          # Создаем форму комментариев
          formComments = Ext.create "IPortal.view.comments.Form"
          formComments.down("form").loadRecord(Ext.create("IPortal.model.Comment"))
          formComments.show()
          return false
      # Удаление комментария
      "infoTenderStuff #gridComments__deleteBtn":
        click: (cmp) ->
          record = @getGridComments().getView().getSelectionModel().getSelection()[0]
          store = Ext.data.StoreManager.lookup("commentStore")
          selectedComment = store.getById(record.getId())

          that = @

          if window.confirm(I18n.t("are_you_sure")) and selectedComment?
            selectedComment.getProxy().extraParams =
              type: "Tender"
              commentable_id: selectedComment.get("commentable_id")

            selectedComment.destroy(
              callback: ->
                store.load()
              success: ->
                Ext.IPortal().msg("", I18n.t("comments.deleted"))
                cmp.setDisabled(true)
              failure: ->
                Ext.IPortal().msg("", I18n.t("comments.not_deleted"))
              scope: @
            )

          return false
      # Перенос в архив
      "infoTenderStuff #infoTenderStuff__moveArchiveBtn":
        click: (cmp) ->
          record = @getGridTenderStuff().getView().getSelectionModel().getSelection()[0]
          info = cmp.up("tabpanel").ownerCt
          if window.confirm(I18n.t("are_you_sure")) and record
            info.setLoading(true)

            record.set("archive", true)
            record.save
              success: () ->
                Ext.data.StoreManager.lookup("tenderStuffStore").reload(
                  callback: () ->
                    info.close()
                    Ext.IPortal().msg("", I18n.t("tenders.moved_archive"))
                )
              failure: () ->
                Ext.IPortal().msg("", I18n.t("tenders.not_moved_archive"))
          return false
      # Отправка комментария
      "formComments #formComments__saveBtn":
        click: (cmp) ->
          form = cmp.up("form").getForm()
          form.updateRecord()
          record = form.getRecord()
          storeComments = Ext.data.StoreManager.lookup("commentStore")
          storeComments.add([record])
          storeComments.sync
            success: ->
              Ext.IPortal().msg("", "Комментарий успешно добавлен")
              cmp.up("form").ownerCt.close()
              storeComments.load()
            failure: ->
              Ext.IPortal().msg("Ошибка", "Ошибка при добавлении комментария")
            scope: @
          return false
      # Отказ от участия
      "gridTenderStuff actioncolumn":
        itemclick: (view, rowIdx, colIdx, item, event, record) ->
          # Показ формы комментария
          view.getSelectionModel().select(rowIdx)

          formComments = Ext.create "IPortal.tenders.form.Reason"
          formComments.setTitle I18n.t "tenders.type_reasons"
          formComments.down("form").loadRecord(Ext.create("IPortal.model.Comment"))
          formComments.show()

          return false

        # отметка предоставления себестоимости
        cost_at_green_click: (view, rowIdx, colIdx, item, event, record) ->
          record.set("cost_at_green", (not record.get("cost_at_green")))

          record.save(
            success: ->
              Ext.IPortal().msg("", "Успешно сохранено.")
            failure: ->
              # Отмена действия
              record.set("cost_at_green", (not record.get("cost_at_green")))
              Ext.IPortal().msg("Ошибка при сохранении", "Обратитесь к администратору или повторите попытку")
          )

          return false
        # отметка предоставления ГПР
        gpr_at_green_click: (view, rowIdx, colIdx, item, event, record) ->
          record.set("gpr_at_green", (not record.get("gpr_at_green")))

          record.save(
            success: ->
              Ext.IPortal().msg("", "Успешно сохранено.")
            failure: ->
              # Отмена действия
              record.set("gpr_at_green", (not record.get("gpr_at_green")))
              Ext.IPortal().msg("Ошибка при сохранении", "Обратитесь к администратору или повторите попытку")
          )

          return false
        # отметка предоставления смет
        estimates_at_green_click: (view, rowIdx, colIdx, item, event, record) ->
          record.set("estimates_at_green", (not record.get("estimates_at_green")))

          record.save(
            success: ->
              Ext.IPortal().msg("", "Успешно сохранено.")
            failure: ->
              # Отмена действия
              record.set("estimates_at_green", (not record.get("estimates_at_green")))
              Ext.IPortal().msg("Ошибка при сохранении", "Обратитесь к администратору или повторите попытку")
          )

          return false
        # отметка готовности
        ready_at_green_click: (view, rowIdx, colIdx, item, event, record) ->
          record.set("ready_at_green", (not record.get("ready_at_green")))

          record.save(
            success: ->
              Ext.IPortal().msg("", "Успешно сохранено.")
            failure: ->
              # Отмена действия
              record.set("ready_at_green", (not record.get("ready_at_green")))
              Ext.IPortal().msg("Ошибка при сохранении", "Обратитесь к администратору или повторите попытку")
          )

          return false

      "formTenderStuffReason":
        beforeclose: () ->
          storeComments = Ext.data.StoreManager.lookup("commentStore")
          storeComments.getProxy().extraParams = {}

      "formTenderStuffReason #formTenderStuffReason__saveBtn":
        click: (cmp) ->
          that = @
          # todo: Сделать это в транзакции

          # Todo: Брать запись из Store
          tender = that.getGridTenderStuff().getView().getSelectionModel().getSelection()[0]

          return Ext.IPortal().msg("", I18n.t("tenders.not_reasoned")) unless tender


          form = cmp.up("form").getForm()
          form.updateRecord()
          record = form.getRecord()

          additional = IPortal.user.get("fullname") + " " + I18n.t("tenders.refused_to_participate") + ". <br /><br />"

          record.set("comment", additional + record.get("comment"))

          storeComments = Ext.data.StoreManager.lookup("commentStore")

          storeComments.getProxy().extraParams =
            type: "Tender"
            commentable_id: tender.get("id")

          storeComments.add([record])
          storeComments.sync
            success: ->
              cmp.up("form").ownerCt.close()
              # Далее убираем из участников
              contractors_hk = tender.get("contractors_hk")
              contractors_hk_attrs = tender.get("tender_contractors_attributes")
              company_id = IPortal.user.get("company_id")

              contractors_hk_attrs = [] unless typeof contractors_hk_attrs.push is "function"

              for index of contractors_hk
                if contractors_hk[index]["id"] == company_id
                  contractors_hk.splice(index, 1)
                  break

              for index of contractors_hk_attrs
                if contractors_hk_attrs[index]["company_id"] == company_id
                  contractors_hk_attrs[index]["_destroy"] = 1
                  break

              tender.set("contractors_hk", contractors_hk)
              tender.set("tender_contractors_attributes", contractors_hk_attrs)

              tender.save(
                success: ->

                  Ext.IPortal().msg("", I18n.t "tenders.unaccept")

                  # filterBy fire refresh event
                  Ext.data.StoreManager.lookup("tenderStuffStore").filterBy (record) ->
                    record.get("id") != tender.get("id")

                failure: ->
                  that.getGridTenderStuff().getStore().reload
                    callback: ->
                      Ext.IPortal().msg("", I18n.t "tenders.not_reasoned")
              )
            failure: ->
              Ext.IPortal().msg("", I18n.t("tenders.not_reasoned"))

