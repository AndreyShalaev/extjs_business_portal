Ext.define "IPortal.controller.tenders.Archive",
  extend: "IPortal.controller.tenders.Base"

  id: "tenders__archive"

  refs: [
    ref: "gridTenderArchive"
    selector: "gridTenderArchive"
  ,
    ref: "eastPanel"
    selector: "#eastPanel"
  ,
    ref: "tenderArchiveFilter"
    selector: "tenderArchiveFilter"
  ,
    ref: "infoTenderArchive"
    selector: "infoTenderArchive"
  ,
    ref: "gridParties"
    selector: "gridParties"
  ,
    ref: "tenderAttachmentGrid"
    selector: "tenderAttachmentGrid"
  ,
    ref: "formAttachment"
    selector: "formAttachment"
  ,
    ref: "formTenderArchive"
    selector: "formTenderArchive"
  ,
    ref: "gridComments"
    selector: "gridComments"
  ]

  filterName: "IPortal.view.tenders.filter.Archive"

  destroy: () ->
    Ext.data.StoreManager.lookup("tenderArchiveStore").clearFilter(true)
    @callParent(arguments)

  init: () ->

    that = @
    countRefresh = 0

    @control
      "gridTenderArchive":
        # Выделение двойным кликом: информация о конкурсе
        celldblclick: (cell, td, cellIndex, record) ->
          tenderInfo = Ext.create "IPortal.view.tenders.InfoArchive"
          tender_id = record.get("id")
          title = "Конкурс №" + tender_id + ". " + record.get("subject")

          tenderInfo.down("form").loadRecord(record)

          tenderInfo.setTitle title

          # Check archive flag
          tenderInfo.down("#infoTenderStuff__extractArchiveBtn").hide() unless record.get("archive")

          tenderInfo.show()

          # Асинхронная подгрузка
          tabPanel = tenderInfo.down("#infoTenderArchive__tabPanel")
          storeComments = Ext.data.StoreManager.lookup("commentStore")
          storeParties = Ext.data.StoreManager.lookup("partyStore")
          storeAttachments = Ext.data.StoreManager.lookup("attachmentStore")


          commentsGrid = Ext.create "IPortal.view.comments.Comment"
          attachmentsGrid = Ext.create "IPortal.view.tenders.Attachment"
          partiesGrid = Ext.create "IPortal.view.tenders.Party"

          tabPanel.add(commentsGrid)
          tabPanel.add(partiesGrid)
          tabPanel.add(attachmentsGrid)

          storeComments.getProxy().extraParams =
              type: "Tender"
              commentable_id: tender_id

          storeParties.getProxy().extraParams =
            tender_id: tender_id

          @commentReloadListener = storeComments.on "load", ->
            count_comments = storeComments.count()

            commentsGrid.setTitle(I18n.t("activerecord.models.comment.other") + "(" + count_comments + ")")

            record = @getGridTenderArchive().getView().getSelectionModel().getSelection()[0]

            tender = null

            tender = Ext.data.StoreManager.lookup("tenderArchiveStore").getById(record.getId()) if record?

            if tender?
              if count_comments is 0
                tender.set("comment", null)
              else
                prev_comment = storeComments.first()

                tender.set("comment", prev_comment.get("comment"))
                tender.set("comment_date", prev_comment.get("created_at"))
                tender.set("comment_user", prev_comment.get("user_name"))
              @syncGridRowsHeights(@getGridTenderArchive())
          , @, destroyable: true

          storeComments.load()

          storeParties.getProxy().url = "/api/tenders/" + tender_id + "/parties"

          storeParties.load()

          storeAttachments.getProxy().url = "/api/tenders/" + tender_id + "/attachments"

          storeAttachments.load()

        lockcolumn: (grid) ->
          @syncGridRowsHeights(grid)
        unlockcolumn: (grid) ->
          @syncGridRowsHeights(grid)
        columnresize: (ct) ->
          @syncGridRowsHeights(ct.up("gridTenderArchive"))
        afterrender: (grid) ->

          @overrideFinishLayout()

          @refreshListener = grid.getView().on("refresh", (viewGrid) ->
            if countRefresh == 3

              @syncGridRowsHeights(grid)

              @refreshListener.destroy()
              @refreshListener = null
              countRefresh = 0

              @refreshListener = grid.getView().on("refresh", (viewGrid) ->
                if countRefresh == 2
                  @syncGridRowsHeights(grid)
                  countRefresh = 0
                countRefresh++
              , that, destroyable: true)

            countRefresh++

          , @, destroyable: true)

          grid.getStore().load(
            callback: () ->
              that.addPanelFilter()
          )
      # Экспорт
      "gridTenderArchive #gridTenderArchive__exportBtn":
        click: ->
          store = Ext.data.StoreManager.lookup("tenderArchiveStore")
          @exportTenders store
          return false
      # Обработка событий фильтра
      "tenderArchiveFilter #filter__sendBtn":
        click: ->
          form = @getTenderArchiveFilter().getForm()
          values = form.getValues()

          store = Ext.data.StoreManager.lookup "tenderArchiveStore"
          filter = []

          for property of values
            if typeof property isnt "undefined" and ("" + values[property]).length > 0
              filter.push(
                property: property
                value: values[property]
              )

          store.clearFilter(true)
          store.filter(filter)

          return false
      "tenderArchiveFilter #filter__resetBtn":
        click: ->
          store = Ext.data.StoreManager.lookup "tenderArchiveStore"
          store.clearFilter()
          form = @getTenderArchiveFilter().getForm()
          form.reset()
          return false
      # Для фильтров сбрасываем флаг live
      "tenderArchiveFilter combo":
        beforequery: (queryEvent) ->
          @clearFilterComboStore(queryEvent)

      # Конец обработки событий фильтра

      "infoTenderArchive":
        beforeclose: ->
          storeComments = Ext.data.StoreManager.lookup("commentStore")
          storeComments.getProxy().extraParams = {}

      # Extract from archive
      "infoTenderArchive #infoTenderStuff__extractArchiveBtn":
        click: (cmp) ->
          record = @getGridTenderArchive().getView().getSelectionModel().getSelection()[0]
          info = cmp.up("tabpanel").ownerCt
          if window.confirm(I18n.t("are_you_sure")) and record
            info.setLoading(true)

            record.set("archive", false)
            record.save
              success: ->
                Ext.data.StoreManager.lookup("tenderArchiveStore").reload(
                  callback: ->
                    info.close()
                    Ext.IPortal().msg("", I18n.t("tenders.extracted_archive"))
                )
              failure: ->
                Ext.IPortal().msg("", I18n.t("tenders.not_moved_archive"))
          return false

      # Edit
      "infoTenderArchive #infoTenderArchive__editBtn":
        click: ->
          tenderForm = Ext.create "IPortal.view.tenders.form.Archive"

          tenderForm.setTitle "Редактирование конкурса"

          record = @getGridTenderArchive().getSelectionModel().getSelection()

          if record[0]?
            record = record[0]
            tender = Ext.data.StoreManager.lookup("tenderArchiveStore").getById(record.getId())

            tenderForm.down("form").getForm().loadRecord(tender)

            tenderForm.show()

            tenderForm.down("#formTender__resetBtn").setDisabled(true)

          return false

      # Save
      "formTenderArchive #formTender__saveBtn":
        click: ->
          that = @
          @getFormTenderArchive().submitForm (record) ->
            Ext.data.StoreManager.lookup("tenderArchiveStore").reload(
              callback: ->
                selection = that.getGridTenderArchive().getSelectionModel()
                selection.select(record)

                that.getInfoTenderArchive().down("form").getForm().loadRecord(record)
            )
          return false

      # Оставляем комментарий
      "infoTenderArchive #gridComments__addBtn":
        click: ->
          # Создаем форму комментариев
          formComments = Ext.create "IPortal.view.comments.Form"
          formComments.down("form").loadRecord(Ext.create("IPortal.model.Comment"))
          formComments.show()
          return false
      # Отправка комментария
      "formComments #formComments__saveBtn":
        click: (cmp) ->
          form = cmp.up("form").getForm()
          form.updateRecord()
          record = form.getRecord()
          storeComments = Ext.data.StoreManager.lookup("commentStore")
          storeComments.add([record])
          storeComments.sync
            success: ->
              Ext.IPortal().msg("", "Комментарий успешно добавлен")
              cmp.up("form").ownerCt.close()
              storeComments.load()
            failure: ->
              Ext.IPortal().msg("Ошибка", "Ошибка при добавлении комментария")
            scope: @
          return false
      # Удаление комментария
      "infoTenderArchive #gridComments__deleteBtn":
        click: (cmp) ->
          record = @getGridComments().getView().getSelectionModel().getSelection()[0]
          store = Ext.data.StoreManager.lookup("commentStore")
          selectedComment = null

          selectedComment = store.getById(record.getId()) if record?

          if window.confirm(I18n.t("are_you_sure")) and selectedComment?
            selectedComment.getProxy().extraParams =
              type: "Tender"
              commentable_id: selectedComment.get("commentable_id")

            selectedComment.destroy(
              callback: ->
                store.load()
              success: ->
                Ext.IPortal().msg("", I18n.t("comments.deleted"))
                cmp.setDisabled(true)
              failure: ->
                Ext.IPortal().msg("", I18n.t("comments.not_deleted"))
              scope: @
            )

          return false
      "gridComments":
        select: ->
          @getGridComments().down("#gridComments__deleteBtn").setDisabled(false)

      # Before edit parties
      "gridParties":
        beforeedit: (editor, context) ->
          companyStore = Ext.data.StoreManager.lookup("companyStore")
          queryParam = context.record.get("company_name")

          companyStore.load
            params:
              query: queryParam

        canceledit: (editor, context) ->
          Ext.data.StoreManager.lookup("partyStore").remove(context.record) if context.record.phantom

        # Save party
        edit: (editor, context) ->
          record = context.record
          record.set("company_id", record.get("companies.name"))

          that = @
          partyStore = Ext.data.StoreManager.lookup("partyStore")

          tenderRecord = @getGridTenderArchive().getView().getSelectionModel().getSelection()[0]

          if tenderRecord?
            tender = Ext.data.StoreManager.lookup("tenderArchiveStore").getById(tenderRecord.getId())
          else
            tender = null

          if tender?
            record.getProxy().url = "/api/tenders/" + tender.getId() + "/parties"

            ########## Exception Listener
            exceptionListener = record.getProxy().on "exception", (proxy, response, operation) ->
              responseJson = null
              errors = ""

              try
                responseJson = Ext.decode response.responseText


              if responseJson.errors?
                for i of responseJson.errors
                  errors += "<ul>"
                  error = I18n.t("activerecord.attributes.party." + i)
                  errors += "<li><b>" + error + ":</b> "
                  for idx of responseJson.errors[i]
                    errors += "<ul>"
                    errors += "<li>" + responseJson.errors[i][idx] + "</li>"
                    errors += "</ul>"
                  errors += "</ul>"

              Ext.IPortal().msg(I18n.t("parties.not_saved"), errors)
              Ext.data.StoreManager.lookup("partyStore").remove(record) if record.phantom

              exceptionListener.destroy()
            , @, destroyable: true
            ########### End exception listener

            # Save record
            record.save(
              success: (record, operation) ->

                exceptionListener.destroy()

                partyStore.reload(
                  callback: ->
                    rawParties = partyStore.getProxy().getReader().rawData

                    jsonObject = undefined

                    try
                      jsonObject = Ext.decode operation.response.responseText
                    catch e
                      Ext.IPortal().msg("", I18n.t("parties.not_saved"))
                      console.log "Error with parsing JSON object"

                    if jsonObject
                      party = jsonObject.party

                      tenderRecord.set("parties", rawParties.parties)
                      if party.winner is true

                        price_winner = null

                        if party.price_finish != null
                          price_winner = party.price_finish
                        else
                          price_winner = party.price

                        tenderRecord.set("winner_name", party.company.name)
                        tenderRecord.set("winner_id", party.company.id)
                        tenderRecord.set("price_winner", price_winner)

                      else if party.company.id == tenderRecord.get("winner_id")
                        tenderRecord.set("winner_name", null)
                        tenderRecord.set("winner_id", null)
                        tenderRecord.set("price_winner", null)

                      that.getGridTenderArchive().getView().refresh()

                      # Reselect grid row
                      selection = that.getGridTenderArchive().getSelectionModel()
                      selection.select(tender)

                      Ext.IPortal().msg("", I18n.t("parties.saved"))
                )
            )
          else
            Ext.IPortal().msg("", I18n.t("parties.not_saved"))

        selectionchange: (selModel, selections) ->
          @getGridParties().down("#gridParties_deleteBtn").setDisabled(selections.length is 0)
      # Add party
      "gridParties #gridParties_addBtn":
        click: ->
          partyStore = Ext.data.StoreManager.lookup("partyStore")
          partyStore.insert(0, Ext.create("IPortal.model.Party"))
          rowEditing = @getGridParties().getPlugin("gridParties__pluginEdit")
          rowEditing.startEdit(0, 0)
          return false
      # Delete party
      "gridParties #gridParties_deleteBtn":
        click: (cmp) ->
          if window.confirm(I18n.t("are_you_sure"))

            gridTenders = @getGridTenderArchive()

            record = @getGridParties().getView().getSelectionModel().getSelection()[0]

            tenderRecord = gridTenders.getView().getSelectionModel().getSelection()[0]

            if tenderRecord?
              tender = Ext.data.StoreManager.lookup("tenderArchiveStore").getById(tenderRecord.getId())
            else
              tender = null

            partyStore = Ext.data.StoreManager.lookup("partyStore")

            if record?
              party = partyStore.getById(record.getId())
            else
              party = null

            that = @

            if tender? and party?
              party.getProxy().url = "/api/tenders/" + tender.getId() + "/parties"

              party.destroy(
                success: ->
                  Ext.IPortal().msg("", I18n.t("parties.destroyed"))

                  cmp.setDisabled(true)

                  partyStore.reload(
                    callback: ->
                      rawParties = partyStore.getProxy().getReader().rawData
                      tender.set("parties", rawParties.parties)

                      if party.get("company_id") == tender.get("winner_id")
                        tender.set("winner_name", null)
                        tender.set("winner_id", null)
                        tender.set("price_winner", null)

                      that.getGridTenderArchive().getView().refresh()
                      that.getGridTenderArchive().getView().getSelectionModel().select(tender)
                  )
                failure: ->
                  Ext.IPortal().msg("", I18n.t("parties.not_destroyed"))
              )

          return false
      # Attachment add
      "tenderAttachmentGrid #tenderAttachmentGrid__addBtn":
        click: ->
          attachmentForm = Ext.create "IPortal.view.tenders.form.Attachment"
          attachmentForm.down("form").getForm().loadRecord(Ext.create("IPortal.model.Attachment"))
          attachmentForm.setTitle I18n.t "activerecord.models.attachment.one"
          attachmentForm.show()
          return false
      "tenderAttachmentGrid":
        selectionchange: (selModel, selections) ->
          @getTenderAttachmentGrid().down("#tenderAttachmentGrid__deleteBtn").setDisabled(selections.length is 0)
      # Upload file
      "formAttachment #formAttachment__saveBtn":
        click: ->

          that = @
          # Todo: Брать запись из Store
          tenderRecord = @getGridTenderArchive().getView().getSelectionModel().getSelection()[0]

          form = @getFormAttachment().down("form").getForm()
          form.submit
            clientValidation: false
            url: "/api/tenders/" + tenderRecord.get("id") + "/attachments"
            success: (form, action) ->

              result = action.result

              tender_attachments = tenderRecord.get("attachments")
              tender_attachments.push result.attachment

              that.getFormAttachment().close()
              Ext.data.StoreManager.lookup("attachmentStore").reload(
                callback: ->
                  tenderRecord.set("attachments", tender_attachments)

                  that.getGridTenderArchive().getView().refresh()

                  Ext.IPortal().msg("", I18n.t("attachments.saved"))

                  # Reselect grid row
                  selection = that.getGridTenderArchive().getSelectionModel()
                  selection.select(tenderRecord)
              )
            failure: ->
              Ext.IPortal().msg("", I18n.t("attachments.not_saved"))

          return false
      # remove attachment
      "tenderAttachmentGrid #tenderAttachmentGrid__deleteBtn":
        click: ->
          # Todo: Брать запись из Store
          record = @getTenderAttachmentGrid().getView().getSelectionModel().getSelection()[0]
          tenderRecord = @getGridTenderArchive().getView().getSelectionModel().getSelection()[0]

          store = Ext.data.StoreManager.lookup("attachmentStore")

          that = @

          if record and tenderRecord and window.confirm I18n.t "are_you_sure"
            record.getProxy().url = "/api/tenders/" + tenderRecord.get("id") + "/attachments"
            record.destroy(
              success: ->
                store.reload(
                  callback: ->
                    rawAttachments = store.getProxy().getReader().rawData

                    tenderRecord.set("attachments", rawAttachments.attachments)

                    that.getGridTenderArchive().getView().refresh()

                    Ext.IPortal().msg("", I18n.t("attachments.destroyed"))

                  # Reselect grid row
                  selection = that.getGridTenderArchive().getSelectionModel()
                  selection.select(tenderRecord)
                )
              failure: ->
                Ext.IPortal().msg("", I18n.t("attachments.not_destroyed"))
            )


          return false
