Ext.define "IPortal.controller.Main",
  extend: "Ext.app.Controller"

  refs: [
    ref: "mainPanel"
    selector: "#mainPanel"
  ,
    ref: "portalMenu"
    selector: "portalMenu"
  ,
    ref: "mainViewport"
    selector: "mainViewport"
  ,
    ref: "pageHeader"
    selector: "pageHeader"
  ,
    ref: "themeSwitcher"
    selector: "pageHeader #pageHeader__switchTheme"
  ]

  init: () ->

    oldController = undefined

    # todo: Перенсти инициализацию хранилищ в точку входа приложения
    Ext.create "IPortal.store.Company"
    Ext.create "IPortal.store.User"
    Ext.create "IPortal.store.Customer"
    Ext.create "IPortal.store.Contractor"
    Ext.create "IPortal.store.Provision"
    Ext.create "IPortal.store.TenderCurrent"
    Ext.create "IPortal.store.TenderStuff"
    Ext.create "IPortal.store.TenderArchive"
    Ext.create "IPortal.store.Winner"
    Ext.create "IPortal.store.Party"
    Ext.create "IPortal.store.Region"
    Ext.create "IPortal.store.Comment"
    Ext.create "IPortal.store.Permission"
    Ext.create "IPortal.store.Attachment"
    Ext.create "IPortal.store.TenderAudit"

    @control
      "pageHeader #pageHeader__switchTheme":
        select: (combo, records) ->

          theme = records[0].get("field1")
          css_path = "/assets/theme_" + theme + ".css"
          Ext.util.CSS.swapStyleSheet "theme_switching", css_path
          Ext.util.Cookies.set("theme", theme)

      "viewport portalMenu":
        select: (me, record) ->
          return  unless record.isLeaf()

          if typeof oldController isnt "undefined" and oldController isnt null
            @eventbus.uncontrol [oldController.id]
            oldController.destroy()
            Ext.destroy oldController

          title = record.get("text")
          title_parent = record.parentNode.get("text")

          title_parent = "" if title_parent == "Root"

          path = @classNameFromRecord(record)

          className = "IPortal.view." + path
          controller = "IPortal.controller." + path

          c = Ext.create(controller)

          oldController = c

          @setActiveApp(className, title, title_parent, c)
        afterrender: () ->
          that = @

          # Load current user
          IPortal.model.CurrentUser.load "",
            scope: @,
            success: (user) ->
              window.IPortal.user = user

              that.getPageHeader().down("#pageHeader__username").setText "Вы вошли как <b>" + user.get("username") + "</b>"

              # Get theme
              theme = Ext.util.Cookies.get("theme")

              combo = @getThemeSwitcher()

              if theme and theme != "undefined" and  theme != "classic"

                combo.select theme

                css_path = "/assets/theme_" + theme + ".css"
                Ext.util.CSS.swapStyleSheet "theme_switching", css_path
              else
                combo.select "classic"

              portalMenu = @getPortalMenu()
              @filterMenu(portalMenu)

              Ext.get("loading").remove()

              @getMainViewport().show()

              # Заносим в следующий event-loop
              window.setTimeout(() ->
                className = window.location.hash.substring(1)

                if className
                  name = className.replace(/-/g, " ")
                  record = portalMenu.view.store.find("text", name)
                else
                  record = portalMenu.view.store.find("text", "Текущие")

                that.selectItem(record)

              , 5)
            failure: ->
              window.location.href = "/500.html"

  selectItem: (record) ->
    portalMenu = @getPortalMenu()
    if record
      portalMenu.view.select(record)
    else
      portalMenu.getRootNode().cascadeBy (node) ->
        if node.isLeaf()
          rportalMenu.view.select(node)

  # Фильтрует пункты меню по привилегиям
  filterMenu: (treeStore) ->
    disallowed = []

    if not IPortal.user.get("admin")
      current_permissions = IPortal.user.get("permissions")
      treeStore.getRootNode().cascadeBy (node) ->
        if node.isLeaf()
          node_permissions = node.get("permissions")
          allowed = false
          for index of current_permissions
            if node_permissions and Ext.Array.indexOf(node_permissions, current_permissions[index]["permission"]) >= 0
              allowed = true
              break
          disallowed.push(node) unless allowed
        return true

    for index of disallowed
      disallowed[index].remove()

    if disallowed.length > 0
      removed_root = []
      treeStore.getRootNode().cascadeBy (node) ->
        if not node.isLeaf() and not node.isRoot() and not node.hasChildNodes()
          removed_root.push(node)

      for index of removed_root
        removed_root[index].remove()


  setActiveApp: (className, title, title_parent, controller) ->
    mainPanel = @getMainPanel()

    mainPanel.removeAll(true)

    title = className.split(".").reverse()[0] unless title

    mainPanel.setTitle(title_parent)

    location.hash = title.toLowerCase().replace(/\s/g, "-")

    document.title = title + " - " + title_parent + " - " + document.title.split(" - ")[0]

    app = Ext.create(className)

    controller.init()

    mainPanel.add app

  classNameFromRecord: (record) ->
    @filePathFromRecord(record).replace("/", ".")
  filePathFromRecord: (record) ->
    parentNode = record.parentNode
    path = record.get("alias")

    while parentNode and parentNode.get("text") isnt "Root"
      path = parentNode.get("alias") + "/" + Ext.String.capitalize(path)
      parentNode = parentNode.parentNode
    @formatPath(path)
  formatPath: (string) ->
    result = string.split("_")[0].charAt(0).toLowerCase() + string.split("_")[0].substr(1)
    paths = string.split("_")
    ln = paths.length
    i = 1
    while i < ln
      result = result + Ext.String.capitalize(paths[i])
      i++
    result

