Ext.define "IPortal.controller.manage.Privilege",
  extend: "Ext.app.Controller"

  id: "manage__privilege"

  refs: [
    ref: "gridPrivileges"
    selector: "gridPrivileges"
  ,
    ref: "formPrivilege"
    selector: "formPrivilege"
  ]

  listenerUserStore: undefined

  destroy: ->
    @listenerUserStore.destroy() if @listenerUserStore
    #userStore.clearFilter(true)
    @callParent arguments

  init: ->
    @control
      "gridPrivileges":
        afterrender: ->
          permissionStore = Ext.data.StoreManager.lookup("permissionStore")
          permissionStore.load()

          userStore = Ext.data.StoreManager.lookup("userStore")

          #userStore.filters = [
          #  filterFn: (user) ->
          #    user.get("role_name") == "admin" and false
          #]

          @listenerUserStore = userStore.on "load", ->

            record = @getGridPrivileges().getSelectionModel().getSelection()[0]

            if record
              permissionStore = Ext.data.StoreManager.lookup("permissionStore")
              userStore = Ext.data.StoreManager.lookup("userStore")
              permission = permissionStore.getById(record.getId())

              userStore.each (user) ->
                permission_attrs = user.get("user_permissions_attributes")
                permission_attrs = [] unless typeof permission_attrs.push is "function"

                user.beginEdit()
                Ext.Array.each permission_attrs, (item) ->
                  user.set("assigned", true) if item["permission_id"] == permission.getId()
                user.endEdit(false)
          , @, destroyable: true

      # Users window
      "gridPrivileges actioncolumn":
        itemclick: (view, rowIdx) ->
          view.getSelectionModel().select(rowIdx)

          privilege = Ext.data.StoreManager.lookup("permissionStore").getAt(rowIdx)

          permissionForm = Ext.create "IPortal.view.manage.form.Privilege"
          if privilege
            permissionForm.setTitle privilege.get("description")
          else
            permissionForm.setTitle I18n.t "activerecord.models.user.other"
          permissionForm.show()

          return false
      # Privileges grid
      "formPrivilege grid":
        afterrender: ->
          userStore = Ext.data.StoreManager.lookup("userStore")
          userStore.load()

      # Save privileges
      "formPrivilege #gridUserPrivileges__saveBtn":
        click: ->
          @getFormPrivilege().setLoading(true)

          Ext.defer () ->
            @getFormPrivilege().fireEvent("savepermissions", @)
          , 50, @

          return false
      "formPrivilege":
        savepermissions: ->
          that = @

          editedUsers = Ext.util.MixedCollection.create()

          userStore = Ext.data.StoreManager.lookup("userStore")
          permissionStore = Ext.data.StoreManager.lookup("permissionStore")

          record = @getGridPrivileges().getView().getSelectionModel().getSelection()[0]
          permission = permissionStore.getById(record.getId()) if record

          if permission
            userStore.each (user) ->
              permission_attrs = user.get("user_permissions_attributes")
              permission_attrs = [] unless permission_attrs instanceof Array

              # Mark as destroyed
              permission_attrs = Ext.Array.map permission_attrs, (item) ->
                if permission.getId() == item["permission_id"]
                  is_destroy = Number(not user.get("assigned"))
                  if is_destroy > 0
                    item["_destroy"] = is_destroy
                    editedUsers.add(user)
                return item

              if user.get("assigned")
                found_for_add = true
                Ext.Array.each permission_attrs, (item) ->
                  if permission.getId() == item["permission_id"]
                    found_for_add = false
                    false

                if found_for_add
                  editedUsers.add(user) unless editedUsers.containsKey(user.getId())
                  permission_attrs.push
                    permission_id: permission.getId()

              unless editedUsers.containsKey(user.getId())
                user.beginEdit()
                user.set("user_permissions_attributes", permission_attrs)
                user.endEdit(false)
            
            count_edited = editedUsers.getCount()

            @getFormPrivilege().setLoading(false) if count_edited is 0
            
            fail = false
            items = 0
            editedUsers.each (user) ->
              user.getProxy().url = "/api/companies/" + user.get("company_id") + "/users"

              user.save(
                success: ->
                  items += 1
                  if items is count_edited
                    userStore.reload(
                      callback: ->
                      Ext.IPortal().msg("", "Успешно сохранено!")
                      that.getFormPrivilege().setLoading(false)
                    )
                failure: ->
                  fail = true
              )
              if fail
                userStore.reload(
                  callback: ->
                    Ext.IPortal().msg("Ошибка", "Ошибка при сохранении")
                    that.getFormPrivilege().setLoading(false)
                )
                return false
