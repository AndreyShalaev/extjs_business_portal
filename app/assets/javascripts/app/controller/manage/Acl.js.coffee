Ext.define "IPortal.controller.manage.Acl",
  extend: "Ext.app.Controller"

  id: "manage__acl"

  refs: [
    ref: "gridUsers"
    selector: "gridUsers"
  ,
    ref: "deleteBtn"
    selector: "gridUsers #gridUsers__deleteBtn"
  ,
    ref: "addBtn"
    selector: "gridUsers #gridUsers__addBtn"
  ,
    ref: "formUser"
    selector: "formUser"
  ,
    ref: "formPermission"
    selector: "formPermission"
  ]

  listenerPermission: undefined

  destroy: ->
    @listenerPermission.destroy() if @listenerPermission

    @callParent arguments

  init: ->

    @control
      "gridUsers":
        afterrender: ->
          Ext.data.StoreManager.lookup("userStore").load()

          permissionStore = Ext.data.StoreManager.lookup("permissionStore")

          # Навешиваем обработчик на событие загрузки хранилища привилегий
          # Здесь мы отмечаем "виртуальное" поле assigned, если у пользователя уже есть
          # такая привилегия
          @listenerPermission = permissionStore.on "load", ->
            user = @getGridUsers().getView().getSelectionModel().getSelection()[0]

            if user
              userStore = Ext.data.StoreManager.lookup("userStore")
              user = userStore.getById(user.getId())

              permission_attrs = user.get("user_permissions_attributes")
              permission_attrs = [] unless typeof permission_attrs.push is "function"

              permissionStore.each (permission) ->
                Ext.Array.each permission_attrs, (item) ->
                  permission.beginEdit()
                  permission.set("assigned", true) if item["permission_id"] == permission.getId()
                  permission.endEdit(false)
          ,  @, destroyable: true

        select: (rowModel, record) ->
          if IPortal.user.get("id") != record.get("id")
            @getDeleteBtn().setDisabled(false)
          else
            @getDeleteBtn().setDisabled(true)
        # Edit user
        celldblclick: (cell, td, cellIndex, record) ->
          formUser = Ext.create "IPortal.view.manage.form.User"
          formUser.setTitle I18n.t "users.edit"

          selectCompany = formUser.down("#selectCompany")


          formUser.show()
          formUser.setLoading(true)

          selectCompany.getStore().load(
            params:
              query: record.get("company_name")
            callback: ->
              formUser.down("form").getForm().loadRecord(record)

              formUser.down("#formUser__passwordField").allowBlank = true
              formUser.down("#formUser__passwordConfirmField").allowBlank = true

              formUser.down("#formUser__resetBtn").setDisabled(true)

              formUser.setLoading(false)

              #record.set("user_permissions_attributes", {}) if record.get("user_permissions_attributes").length is 0
          )

      # Privileges window
      "gridUsers actioncolumn":
        itemclick: (view, rowIdx) ->
          view.getSelectionModel().select(rowIdx)

          permissionForm = Ext.create "IPortal.view.manage.form.Permission"
          permissionForm.setTitle I18n.t "activerecord.models.permission.other"
          permissionForm.show()

          return false

      # Save permissions
      "formPermission #gridPermissions__saveBtn":
        click: ->
          that = @
          userStore = Ext.data.StoreManager.lookup("userStore")
          permissionStore = Ext.data.StoreManager.lookup("permissionStore")

          record = @getGridUsers().getView().getSelectionModel().getSelection()[0]
          user = userStore.getById(record.getId()) if record

          if user
            permission_attrs = user.get("user_permissions_attributes")
            permission_attrs = [] unless typeof permission_attrs.push is "function"

            # Здесь мы просто собираем данные из таблички привилегий
            # и на оснавании отметок устанавливаем привилегии в модель выбранного пользователя
            # и сохраняем

            # Found and mark as destroyed
            permission_attrs = Ext.Array.map permission_attrs, (item) ->
              found_for_destroy = is_destroy = false
              permissionStore.each (permission) ->
                if item["permission_id"] == permission.getId()
                  found_for_destroy = true
                  is_destroy = Number(not permission.get("assigned"))
                  # Break
                  return false
              item["_destroy"] = is_destroy if found_for_destroy
              return item
            # Add new permissions
            permissionStore.each (permission) ->
              found_for_add = true
              Ext.Array.each permission_attrs, (item) ->
                if item["permission_id"] == permission.getId()
                  found_for_add = false
                  false

              if found_for_add and permission.get("assigned")
                permission_attrs.push
                  permission_id: permission.getId()

            user.beginEdit()
            user.set("user_permissions_attributes", permission_attrs)
            user.endEdit(false)

            user.getProxy().url = "/api/companies/" + user.get("company_id") + "/users"
            user.save(
              success: ->
                userStore.reload(
                  callback: ->
                    that.getGridUsers().getView().getSelectionModel().select(user)

                    permissionStore.reload()
                    Ext.IPortal().msg("", "Успешно сохранено!")
                )
              failure: ->
                Ext.IPortal().msg("", "Ошибка при сохранении")
            )
          return false

      "formPermission grid":
        afterrender: ->
          # После отрисовки окна привилегий, подгружаем записи
          # при этом срабатывает событие load у permissionStore (см. выше в инициализации гл. окна)
          permissionStore = Ext.data.StoreManager.lookup("permissionStore")
          permissionStore.load()

      # Добавление пользователя
      "gridUsers #gridUsers__addBtn":
        click: ->
          formUser = Ext.create "IPortal.view.manage.form.User"
          formUser.setTitle I18n.t "users.add"
          formUser.down("form").getForm().loadRecord(Ext.create("IPortal.model.User"))
          formUser.show()

          return false

      # Сохраняем пользователя
      "formUser #formUser__saveBtn":
        click: ->
          # Set model proxy url
          form = @getFormUser().down("form").getForm()
          record = form.getRecord()

          form.updateRecord() unless record.get("company_id")

          record.getProxy().url = "/api/companies/" + record.get("company_id") + "/users"

          @getFormUser().submitForm ->
            Ext.data.StoreManager.lookup("userStore").reload()
          return false

      # User destroy
      "gridUsers #gridUsers__deleteBtn":
        click: ->
          record = @getGridUsers().getView().getSelectionModel().getSelection()[0]
          userStore = Ext.data.StoreManager.lookup("userStore")

          if record and window.confirm I18n.t "are_you_sure"
            record = userStore.getById(record.getId())

            record.getProxy().url = "/api/companies/" + record.get("company_id") + "/users"
            record.destroy(
              success: ->
                Ext.IPortal().msg("", I18n.t "users.destroyed")
              failure: ->
                Ext.IPortal().msg("", I18n.t "Ошибка при удалении. Обратитесь в тех. поддержку.")
            )

          return false

