Ext.define "IPortal.plugin.Can",
  extend: "Ext.AbstractPlugin"

  alias: "plugin.can"

  init: (cmp) ->

    if typeof cmp.permissions isnt "undefined"
      current_user = IPortal.user

      admin = current_user.get("admin")
      permissions = current_user.get("permissions")


      allowed = false

      for index of permissions
        if Ext.Array.indexOf(cmp.permissions, permissions[index]["permission"]) >= 0
          allowed = true
          break

      if typeof cmp.hide is "function" and not admin and not allowed
        if cmp.getXType() == "actioncolumn"
          cmp.on "afterrender", (c) ->
            c.hide()
        else
          cmp.hide()
