Ext.define "IPortal.model.Comment",
  extend: "Ext.data.Model"

  fields: [
    name: "id"
    persist: false
  ,
    name: "comment"
  ,
    name: "created_at"
    persist: false
  ,
    name: "user_id"
    mapping: "user.id"
    persist: false
  ,
    name: "user_name"
    mapping: "user.fullname"
    persist: false
  ,
    name: "commentable_id"
    persist: false
  ]

  proxy:
    type: "rest"
    url: "/api/comments"
    writer:
      type: "json"
      root: "comment"
