Ext.define "IPortal.model.CurrentUser",
  extend: "Ext.data.Model"

  fields: [
    name: "id"
    persist: false
  ,
    name: "fullname"
    type: "string"
    persist: false
  ,
    name: "username"
    type: "string"
  ,
    name: "company"
  ,
    name: "company_id"
    mapping: "company.id"
    type: "int"
    useNull: true
  ,
    name: "company_hk"
    type: "bool"
    mapping: "company.hk"
    persist: false
    defaultValue: false
  ,
    name: "company_name"
    mapping: "company.name"
    type: "string"
    useNull: true
    persist: false
  ,
    name: "company_fullname"
    mapping: "company.fullname"
    type: "string"
    useNull: true
    persist: false
  ,
    name: "company_type"
    mapping: "company.type"
    type: "string"
    persist: false
  ,
    name: "permissions"
    persist: false
    defaultValue: []
  ,
    name: "admin"
    type: "bool"
    persist: false
    defaultValue: false
  ]

  proxy:
    type: "rest"
    url: "/api/profile"
    reader:
      type: "json"
      root: "user"
