Ext.define "IPortal.model.Attachment",
  extend: "Ext.data.Model"

  fields: [
    name: "id"
  ,
    name: "name"
  ,
    name: "file"
  ]

  proxy:
    type: "rest"
    writer:
      type: "json"
      root: "attachment"
