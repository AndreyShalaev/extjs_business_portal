Ext.define "IPortal.model.Provision",
  extend: "Ext.data.Model"

  fields: [
    name: "id"
    persist: false
  ,
    name: "name"
    type: "string"
  ]

  proxy:
    type: "rest",
    url: "/api/provisions"
    reader:
      type: "json"
      root: "provisions"
      totalProperty: "total"
    writer:
      type: "json"
      root: "provision"