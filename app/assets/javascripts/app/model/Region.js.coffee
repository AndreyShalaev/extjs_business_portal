Ext.define "IPortal.model.Region",
  extend: "Ext.data.Model"

  fields: [
    name: "id"
  ,
    name: "name"
  ,
    name: "code"
  ]

  proxy:
    type: "rest"
    url: "/api/regions"
    reader:
      type: "json"
      root: "regions"
      totalProperty: "total"
