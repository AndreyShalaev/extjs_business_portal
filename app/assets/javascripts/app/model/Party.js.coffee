Ext.define "IPortal.model.Party",
  extend: "Ext.data.Model"

  fields: [
    name: "id"
    persist: false
  ,
    name: "company_name"
    mapping: "company.name"
    persist: false
  ,
    name: "companies.name"
    mapping: "company.id"
    persist: false
  ,
    name: "company_id"
    mapping: "company.id"
  ,
    name: "company"
    persist: false
  ,
    name: "price"
    useNull: true
  ,
    name: "price_finish"
    useNull: true
  ,
    name: "tender_id"
    type: "int"
    persist: false
  ,
    name: "winner"
    type: "bool"
    defaultValue: false
  ]

  proxy:
    type: "rest"
    url: "/api/parties"
    writer:
      type: "json"
      root: "party"
