Ext.define "IPortal.model.Session",
  extend: "Ext.data.Model"

  fields: [
    name: "id"
  ,
    name: "role_name"
    mapping: "role.name"
  ]

  proxy:
    type: "ajax"
    url: "/api/session"
    reader:
      type: "json"
