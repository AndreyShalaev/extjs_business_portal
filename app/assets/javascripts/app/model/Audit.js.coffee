Ext.define "IPortal.model.Audit",
  extend: "Ext.data.Model"

  fields: [
    name: "id"
    type: "int"
  ,
    name: "audited_changes"
  ,
    name: "user"
    type: "string"
    useNull: true
  ,
    name: "created_at"
    type: "date"
    dateFormat: "c"
  ,
    name: "auditable_id"
    type: "int"
    useNull: true
  ,
    name: "action"
    type: "string"
  ,
    name: "remote_address"
    type: "string"
  ,
    name: "subject"
    type: "string"
  ,
    name: "type_view"
    type: "string"
  ]