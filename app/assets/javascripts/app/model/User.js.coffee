Ext.define "IPortal.model.User",
  extend: "Ext.data.Model"

  fields: [
    name: "id"
    persist: false
  ,
    name: "username"
    type: "string"
  ,
    name: "fullname"
    type: "string"
    persist: false
  ,
    name: "name"
    type: "string"
  ,
    name: "surname"
    type: "string"
  ,
    name: "middlename"
    type: "string"
  ,
    name: "password"
    convert: (value) ->
      if typeof value.length is "undefined" or value.length is 0
        return null
      else
        return value
  ,
    name: "password_confirmation"
    convert: (value) ->
      if typeof value.length is "undefined" or value.length is 0
        return null
      else
        return value
  ,
    name: "email"
    type: "string"
  ,
    name: "post"
    type: "string"
  ,
    name: "role_id"
  ,
    name: "role_name"
    mapping: "role.name"
    persist: false # Так поле не будет отправлено на сервер
  ,
    name: "role_description"
    mapping: "role.description"
    persist: false
  ,
    name: "company_id"
  ,
    name: "company_name"
    mapping: "company.name"
    persist: false
  ,
    name: "phone"
    type: "string"
  ,
    name: "own_phone"
    type: "string"
  ,
    name: "note"
  ,
    name: "user_permissions_attributes"
    defaultValue: {}
    convert: (value) ->
      if typeof value.length is "undefined" or value.length is 0
        return {}
      else
        return value
  ,
    name: "deleted"
    type: "bool"
    useNull: true
    persist: false
  ,
    name: "assigned"
    type: "bool"
    defaultValue: false
    persist: false
  ]

  proxy:
    type: "rest"
    url: "/api/users"
    extraParams:
      live: 1
    writer:
      type: "json"
      root: "user"
