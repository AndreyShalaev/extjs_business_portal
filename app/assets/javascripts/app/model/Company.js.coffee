Ext.define "IPortal.model.Company",
  extend: "Ext.data.Model"

  fields: [
    name: "id"
    persist: false
  ,
    name: "name"
    type: "string"
  ,
    name: "fullname"
    type: "string"
    persist: false
    useNull: true
  ,
    name: "short_name"
    type: "string"
    useNull: true
  ,
    name: "address"
    type: "string"
    useNull: true
  ,
    name: "site"
    type: "string"
    useNull: true
  ,
    name: "email"
    type: "string"
    useNull: true
  ,
    name: "phone"
    type: "string"
    useNull: true
  ,
    name: "inn"
    type: "string"
    useNull: true
  ,
    name: "hk"
    type: "bool"
    defaultValue: false
  ,
    name: "activities"
    type: "string"
    useNull: true
  ,
    name: "region_id"
    useNull: true
    type: "int"
  ,
    name: "region_name"
    type: "string"
    persist: false
  ,
    name: "region_code"
    type: "int"
    persist: false
  ,
    name: "type"
    useNull: true
    type: "string"
  ,
    name: "parent_name"
    mapping: "parent.name"
    persist: false
  ,
    name: "org_level"
    type: "int"
  ,
    name: "deleted"
    type: "bool"
    defaultValue: false
  ]

  proxy:
    type: "rest"
    url: "/api/companies"
    writer:
      type: "json"
      root: "company"
