Ext.define "IPortal.model.Tender",
  extend: "Ext.data.Model"

  ###convertEmpty: (value) ->
    if typeof value.length is "number" and value.length is 0
      return null
    else
      return value###

  fields: [
    name: "id"
    persist: false
  ,
    name: "provision_id"
    useNull: true
    type: "int"
  ,
    name: "provision_name"
    persist: false
    type: "string"
    useNull: true
  ,
    name: "customer"
    persist: false
    defaultValue: {}
  ,
    name: "customer_id"
    useNull: true
    type: "int"
  ,
    name: "customer_fullname"
    mapping: "customer.fullname"
    type: "string"
    persist: false
  ,
    name: "customer_name"
    mapping: "customer_name"
    type: "string"
    persist: false
  ,
    name: "subject"
    type: "string"
    useNull: true
  ,
    name: "price_limit"
    type: "float"
    useNull: true
  ,
    name: "price_provision"
    type: "int"
    useNull: true
  ,
    name: "winner_id"
    type: "int"
    useNull: true
  ,
    name: "winner_name"
    type: "string"
    useNull: true
    persist: false
  ,
    name: "price_winner"
    type: "float"
    useNull: true
  ,
    name: "requirements"
    type: "string"
    useNull: true
  ,
    name: "link"
    type: "string"
    useNull: true
  ,
    name: "declared_at"
    type: "date"
    dateFormat: "Y-m-d"
  ,
    name: "opening_at"
    type: "date"
    dateFormat: "Y-m-d"
  ,
    name: "documentation"
    type: "string"
    useNull: true
  ,
    name: "turnaround_time"
    type: "string"
    useNull: true
  ,
    name: "price_cost"
    type: "float"
    useNull: true
  ,
    name: "price_our"
    type: "float"
    useNull: true
  ,
    name: "cost_at"
    type: "date"
    dateFormat: "Y-m-d"
    useNull: true
    persist: false
  ,
    name: "cost_at_green"
    type: "bool"
    defaultValue: false
  ,
    name: "gpr_at"
    type: "date"
    dateFormat: "Y-m-d"
    persist: false
    useNull: true
  ,
    name: "gpr_at_green"
    type: "bool"
    defaultValue: false
  ,
    name: "estimates_at"
    type: "date"
    dateFormat: "Y-m-d"
    persist: false
    useNull: true
  ,
    name: "estimates_at_green"
    type: "bool"
    defaultValue: false
  ,
    name: "ready_at"
    type: "date"
    dateFormat: "Y-m-d"
    persist: false
    useNull: true
  ,
    name: "ready_at_green"
    type: "bool"
    defaultValue: false
  ,
    name: "contractors_hk"
    persist: false
  ,
    name: "tender_contractors_attributes"
    defaultValue: {}
    convert: (value) ->
      if not (value instanceof Array) or value.length is 0
        return {}
      else
        return value
  ,
    name: "parties"
    useNull: true
    persist: false
  ,
    name: "comment"
    mapping: "last_comment.comment"
    useNull: true
    persist: false
  ,
    name: "comment_user"
    mapping: "last_comment.user.fullname"
    useNull: true
    persist: false
  ,
    name: "comment_date"
    mapping: "last_comment.created_at"
    type: "date"
    useNull: true
    persist: false
    dateFormat: "c"
  ,
    name: "archive"
    type: "bool"
    useNull: false
    defaultValue: false
  ,
    name: "attachments"
    useNull: true
    persist: false
  ]

  proxy:
    type: "rest"
    url: "/api/tenders"
    writer:
      type: "json"
      root: "tender"
