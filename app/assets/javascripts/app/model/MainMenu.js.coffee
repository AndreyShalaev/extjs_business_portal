Ext.define "IPortal.model.MainMenu",
  extend: "Ext.data.Model"

  fields: [
    name: "text"
  ,
    name: "alias"
  ,
    name: "permissions"
    useNull: true
  ]
