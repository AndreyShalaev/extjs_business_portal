Ext.define "IPortal.model.TenderChanges",
  extend: "Ext.data.Model"
  idProperty: "field"
  fields: [
    name: "field"
    type: "string"
  ,
    name: "was"
    type: "string"
  ,
    name: "been"
    type: "string"
  ]