Ext.define "IPortal.model.Permission",
  extend: "Ext.data.Model"

  fields: [
    name: "id"
  ,
    name: "description"
  ,
    name: "action"
  ,
    name: "assigned"
    type: "bool"
    defaultValue: false
  ]
