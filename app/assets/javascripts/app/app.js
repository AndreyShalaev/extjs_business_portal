//= require ./overrides
//= require ./ux/manifest
//= require_tree ./plugin
//= require_tree ./proxy
//= require ./controller/manifest
//= require_tree ./model
//= require ./store/manifest
//= require ./view/manifest
//= require_self

// Refernce to current user
window.IPortal.user = void 0;

Ext.application({
  name: "IPortal",
  autoCreateViewport: true,
  controllers: [
    "Main"
  ]
});
