#Это вызывается один раз
#Требуется, чтобы осводить память, когда старый контроллер выгружен
#Для удаления всех созданных старым контроллером обработчиков событий
#Тем самым мы устраняем утечку памяти и баги с возрасатющим кол-ом event listeners
#Теперь у каждого контроллера в объекте eventbus присутствует этот метод
Ext.override(Ext.app.EventBus,
  #Что воспользоваться им, необходимо иметь у каждого контроллера уникальный id
  # usage: this.eventbus.uncontrol([controller.id]);
  uncontrol: (controllerArray) ->
    bus = @bus
    Ext.iterate(bus, (ev, controllers) ->
      Ext.iterate(controllers, (query, controller) ->
        deleteThis = false

        Ext.iterate(controller, (controlName) ->
          idx = controllerArray.indexOf(controlName)
          deleteThis = true if idx >= 0
        )

        delete controllers[query] if deleteThis
      )
    )
)

#Меняем поведение combobox при наведении его на другую страницу
Ext.override(Ext.form.field.ComboBox,
  findRecord: (field, value) ->
    foundRec = null
    Ext.each(@lastSelection, (rec) ->
      if rec.get(field) == value
        foundRec = rec
        false
    )

    return foundRec if foundRec
    @callParent(arguments)
)

# Утилита для клонирования Stores
Ext.override(Ext,
  cloneStore: (source) ->
    target = Ext.create("Ext.data.Store",
      model: source.model
      remoteSort: true
      pageSize: source.pageSize
      autoLoad: false
      sorters: [
        property: "name"
        direction: "ASC"
      ]
    )
    Ext.each source.getRange(), (record) ->
      newRecordData = Ext.clone(record.copy().data)
      model = new source.model(newRecordData, newRecordData.id)
      target.add model

    target
)

# Названия месяцев
Ext.Date.monthNames = ["Января", "Февраля", "Марта", "Апреля", "Мая", "Июня", "Июля", "Августа", "Сентября", "Октября", "Ноября", "Декабря"]

Number::formatMoney = (c, d, t) ->
  n = this
  c = (if isNaN(c = Math.abs(c)) then 2 else c)
  d = (if d is `undefined` then "." else d)
  t = (if t is `undefined` then "," else t)
  s = (if n < 0 then "-" else "")
  i = parseInt(n = Math.abs(+n or 0).toFixed(c)) + ""
  j = (if (j = i.length) > 3 then j % 3 else 0)
  s + ((if j then i.substr(0, j) + t else "")) + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + ((if c then d + Math.abs(n - i).toFixed(c).slice(2) else ""))

# Csrf
Ext.Ajax.on("beforerequest", (conn, options) ->
  unless /^http:.*/.test(options.url) or /^https:.*/.test(options.url)
    csrfToken = Ext.select("meta[name='csrf-token']").elements[0].getAttribute("content")
    if typeof options.headers is "undefined"
      options.headers =
        "X-CSRF-Token": csrfToken
    else
      Ext.Object.merge options.headers,
        "X-CSRF-Token": csrfToken
, @)

Ext.Ajax.on("requestexception", (conn, response) ->
  window.location.href = "/" if response.status is 401
, @)

# Quick tips hack
delete Ext.tip.Tip.prototype.minWidth

# Message box
Ext.IPortal = () ->
  msgCt = undefined

  createBox = (t, s) ->
    "<div class=\"msg\"><h3>" + t + "</h3><p>" + s + "</p></div>"

  msg: (title, format) ->
    unless msgCt
      msgCt = Ext.DomHelper.insertFirst(document.body,
        id: "msg-div"
      , true)

      s = Ext.String.format.apply(String, Array::slice.call(arguments, 1))
      m = Ext.DomHelper.append(msgCt, createBox(title, s), true)
      m.hide()
      m.slideIn("t").ghost("t",
        delay: 2500
        remove: true
      )

Ext.Object.merge(Ext.util.CSS,
  swapStyleSheet : (id, url) ->
    Ext.util.CSS.removeStyleSheet(id);
    ss = document.createElement("link")
    ss.setAttribute("rel", "stylesheet")
    ss.setAttribute("type", "text/css")
    ss.setAttribute("id", id)
    ss.setAttribute("href", url)
    header = document.getElementsByTagName("head")[0]
    header.insertBefore(ss, header.firstChild)
)

Ext.override Ext.grid.View,
  loadingText: I18n.t "loading"
  stripeRows: false
  enableTextSelection: true
  markDirty: false


Ext.Object.merge(Ext.Number,
  # Clean prices for numberfields
  # Usage:
  #
  # xtype: "numberfield"
  # listeners:
  #   change: (cmp) ->
  #     Ext.Number.priceClean(cmp, event)
  #
  # @param {Object} cmp Listened component (numberfiled)
  priceClean: (cmp) ->
    window.setTimeout(->
      inputVal = cmp.getRawValue()
      unless /^\d+,\d+$/g.test(inputVal)
        inputVal = inputVal.replace(/[^0-9,]/g, "")
        cmp.setValue(inputVal)
    , 50)
)

# Override Ajax upload method for csrf hidden input
Ext.Object.merge(Ext.Ajax,
  upload: (form, url, params, options) ->
    form = Ext.getDom(form)
    options = options or {}
    id = Ext.id()
    frame = document.createElement("iframe")
    hiddens = []
    encoding = "multipart/form-data"
    buf =
      target: form.target
      method: form.method
      encoding: form.encoding
      enctype: form.enctype
      action: form.action



    addField = (name, value) ->
      hiddenItem = document.createElement("input")
      Ext.fly(hiddenItem).set
        type: "hidden"
        value: value
        name: name

      form.appendChild hiddenItem
      hiddens.push hiddenItem

    #hiddenItem = undefined
    obj = undefined
    value = undefined
    name = undefined
    vLen = undefined
    v = undefined
    hLen = undefined
    h = undefined

    csrfToken = undefined

    if typeof options.headers isnt "undefined" and typeof options.headers["X-CSRF-Token"] isnt "undefined"
      csrfToken = options.headers["X-CSRF-Token"]
    else
      csrfToken = Ext.select("meta[name='csrf-token']").elements[0].getAttribute("content")

    #
    #         * Originally this behaviour was modified for Opera 10 to apply the secure URL after
    #         * the frame had been added to the document. It seems this has since been corrected in
    #         * Opera so the behaviour has been reverted, the URL will be set before being added.
    #
    Ext.fly(frame).set
      id: id
      name: id
      cls: Ext.baseCSSPrefix + "hide-display"
      src: Ext.SSL_SECURE_URL

    document.body.appendChild frame

    # This is required so that IE doesn't pop the response up in a new window.
    document.frames[id].name = id  if document.frames
    Ext.fly(form).set
      target: id
      method: "POST"
      enctype: encoding
      encoding: encoding
      action: url or buf.action

    # add CSRF token param
    addField "authenticity_token", csrfToken if csrfToken
    # optional utf-8 encoding
    addField "utf8", "✓"
    # add dynamic params
    if params
      obj = Ext.Object.fromQueryString(params) or {}
      for name of obj
        if obj.hasOwnProperty(name)
          value = obj[name]
          if Ext.isArray(value)
            vLen = value.length
            v = 0
            while v < vLen
              addField name, value[v]
              v++
          else
            addField name, value
    Ext.fly(frame).on "load", Ext.Function.bind(@onUploadComplete, this, [frame, options]), null,
      single: not Ext.isOpera

    form.submit()
    Ext.fly(form).set buf
    hLen = hiddens.length
    h = 0
    while h < hLen
      Ext.removeNode hiddens[h]
      h++
)

