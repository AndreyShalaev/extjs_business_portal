Ext.define "IPortal.store.tenders.Current",
  extend: "Ext.data.Store"

  config:
    storeId: "tenderCurrentStore"

    remoteSort: true

    remoteFilter: true

    pageSize: 50

    autoLoad: true

    sorters: [
      property: "opening_at"
      direction: "DESC"
    ]

    model: "IPortal.model.Tender"

    proxy:
      type: "rest"
      url: "/api/tenders/currents"
      reader:
        type: "json"
        rootProperty: "tenders"
        totalProperty: "total"
