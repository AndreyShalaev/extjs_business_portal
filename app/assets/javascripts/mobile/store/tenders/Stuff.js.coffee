Ext.define "IPortal.store.tenders.Stuff",
  extend: "Ext.data.Store"

  config:
    storeId: "tenderStuffStore"

    remoteSort: true

    remoteFilter: true

    pageSize: 50

    autoLoad: true

    sorters: [
      property: "opening_at"
      direction: "DESC"
    ]

    model: "IPortal.model.Tender"

    proxy:
      type: "rest"
      url: "/api/tenders/stuffs"
      extraParams:
        show_all: 0
      reader:
        type: "json"
        rootProperty: "tenders"
        totalProperty: "total"
