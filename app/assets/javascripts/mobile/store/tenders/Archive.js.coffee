Ext.define "IPortal.store.tenders.Archive",
  extend: "Ext.data.Store"

  config:
    storeId: "tenderArchiveStore"

    remoteSort: true

    remoteFilter: true

    pageSize: 50

    autoLoad: true

    sorters: [
      property: "opening_at"
      direction: "DESC"
    ]

    model: "IPortal.model.Tender"

    proxy:
      type: "rest"
      url: "/api/tenders/archives"
      reader:
        type: "json"
        rootProperty: "tenders"
        totalProperty: "total"
