Ext.define "IPortal.store.tenders.Tender",
  extend: "Ext.data.Store"

  config:
    storeId: "tenderStore"

    remoteSort: true

    remoteFilter: true

    pageSize: 50

    autoLoad: false

    sorters: [
      property: "opening_at"
      direction: "ASC"
    ]

    filters: [
      property: "taking_current"
      value: 1
    ]

    model: "IPortal.model.Tender"

    proxy:
      type: "rest"
      url: "/api/tenders/archives"
      reader:
        type: "json"
        rootProperty: "tenders"
        totalProperty: "total"
