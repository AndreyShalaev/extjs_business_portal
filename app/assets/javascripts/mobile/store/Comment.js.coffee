Ext.define "IPortal.store.Comment",
  extend: "Ext.data.Store"

  config:
    storeId: "commentStore"

    remoteSort: true

    remoteFilter: true

    pageSize: 50

    autoLoad: false

    sorters: [
      property: "comments.created_at"
      direction: "DESC"
    ]

    model: "IPortal.model.Comment"

    proxy:
      type: "rest"
      url: "/api/comments"
      reader:
        type: "json"
        root: "comments"
        totalProperty: "total"
      writer:
        type: "json"
        rootProperty: "comment"
