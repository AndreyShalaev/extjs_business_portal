Ext.define "IPortal.controller.StuffTender",
  extend: "Ext.app.Controller"

  config:
    refs:
      mainPanel:
        xtype: "mainPanel"
        selector: "mainPanel"
      infoPanel:
        xtype: "infoPanel"
        selector: "infoPanel"
      stuffPanel:
        xtype: "stuffPanel"
        selector: "stuffPanel"
        autoCreate: true
      defaultToolbar:
        xtype: "defaultToolbar"
        selector: "defaultToolbar"
        autoCreate: true
      searchToolbar:
        xtype: "searchToolbar"
        selector: "searchToolbar"
        autoCreate: true

    control:
      stuffPanel:
        activate: (cmp, that, oldCmp) ->
          @getMainPanel().add @getDefaultToolbar() if (oldCmp instanceof IPortal.view.tenders.Search) or not oldCmp
          @getDefaultToolbar().setTitle cmp.getTitle()
          # Activate switcher
          @getDefaultToolbar().down("togglefield").show()
          
          search_value = @getDefaultToolbar().down("searchfield").getValue()

          if not (oldCmp instanceof IPortal.view.tenders.Search) and oldCmp.getStore? and search_value.length > 0
            @getDefaultToolbar().down("searchfield").setValue("")
            oldCmp.getStore().load()
        itemdoubletap: (cmp, index, target, record) ->

          storeComment = Ext.data.StoreManager.lookup("commentStore")

          storeComment.getProxy().setExtraParams(
            type: "Tender"
            commentable_id: record.get("id")
          )

          @getInfoPanel().setRecord(record)

          # Process unaccept part button
          btn = @getInfoPanel().down("#infoPanel__unacceptBtn")

          contractors_hk = record.get("contractors_hk")
          current_user = IPortal.user
          company_hk = current_user.get("company_hk")
          company_id = current_user.get("company_id")

          if company_hk
            for index of contractors_hk
              if contractors_hk[index]["id"] == company_id
                btn.show()
                break

          @getInfoPanel().show()
        deactivate: ->
          @getDefaultToolbar().down("togglefield").hide()
      "defaultToolbar togglefield":
        change: (cmp) ->
          cmp.disable()

          store = Ext.data.StoreManager.lookup("tenderStuffStore")

          proxy = store.getProxy()

          show_all = proxy.getExtraParams()["show_all"]
          proxy.setExtraParams
            show_all: 1 - show_all
          store.load(
            callback: ->
              cmp.enable()
          )
      # Unaccpet part
      "infoPanel #infoPanel__unacceptBtn":
        tap: (cmp) ->
          cmp.disable()
          that = @

          reasonFn = () ->
            Ext.Msg.show
              title: I18n.t "comments.leave_comment"
              message: I18n.t "tenders.type_reasons"
              buttons: Ext.MessageBox.OKCANCEL
              multiLine: true
              promt:
                maxlength: 300
                autocapitalize: true
              fn: (btnId, value) ->
                if btnId is "ok" and value.length > 3
                  # Close window
                  cmp.enable()
                  cmp.hide()
                  that.getInfoPanel().hide()

                  record = that.getInfoPanel().getRecord()
                  store = Ext.data.StoreManager.lookup("tenderStuffStore")
                  storeComments = Ext.data.StoreManager.lookup("commentStore")
                  current_user = IPortal.user

                  # Unaccept part in tender
                  additional = current_user.get("fullname") + " " + I18n.t("tenders.refused_to_participate") + ". <br /><br />"

                  value = additional + value
                  # Create a new comment with reason
                  comment = Ext.create "IPortal.model.Comment",
                    comment: value

                  # Commit comment
                  storeComments.add(comment)
                  storeComments.syncWithListener( ->

                    # Remove from contractors
                    contractors_hk = record.get("contractors_hk")
                    contractors_hk_attrs = record.get("tender_contractors_attributes")
                    company_id = current_user.get("company_id")

                    for index of contractors_hk
                      if contractors_hk[index]["id"] == company_id
                        contractors_hk.splice(index, 1)
                        break

                    for index of contractors_hk_attrs
                      if contractors_hk_attrs[index]["company_id"] == company_id
                        contractors_hk_attrs[index]["_destroy"] = 1
                        break

                    record.set("contractors_hk", contractors_hk)
                    record.set("tender_contractors_attributes", contractors_hk_attrs)
                    # Save tender
                    record.save(
                      success: ->

                        Ext.Msg.alert("", I18n.t "tenders.unaccept")

                        # Reload stores
                        store.load(
                          callback: ->
                            Ext.data.StoreManager.lookup("tenderCurrentStore").load()
                        )
                      failure: ->
                        store.load
                          callback: ->
                            Ext.Msg.alert("", I18n.t "tenders.not_reasoned")
                    )
                  , false)
                else if btnId is "ok"
                  Ext.Msg.show
                    title: ""
                    message: I18n.t "comments.errors.length"
                    buttons: Ext.MessageBox.OK
                    fn: ->
                      reasonFn()
          reasonFn()


