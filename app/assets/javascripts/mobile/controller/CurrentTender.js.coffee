Ext.define "IPortal.controller.CurrentTender",
  extend: "Ext.app.Controller"

  config:
    refs:
      mainPanel:
        xtype: "mainPanel"
        selector: "mainPanel"
      infoPanel:
        xtype: "infoPanel"
        selector: "infoPanel"
      currentPanel:
        xtype: "currentPanel"
        selector: "currentPanel"
        autoCreate: true
      defaultToolbar:
        xtype: "defaultToolbar"
        selector: "defaultToolbar"
        autoCreate: true
      searchToolbar:
        xtype: "searchToolbar"
        selector: "searchToolbar"
        autoCreate: true

    control:
      currentPanel:
        activate: (cmp, that, oldCmp) ->
          @getMainPanel().add @getDefaultToolbar() if (oldCmp instanceof IPortal.view.tenders.Search) or not oldCmp
          @getDefaultToolbar().setTitle cmp.getTitle()
          
          search_value = @getDefaultToolbar().down("searchfield").getValue()

          if not (oldCmp instanceof IPortal.view.tenders.Search) and oldCmp.getStore? and search_value.length > 0
            @getDefaultToolbar().down("searchfield").setValue("")
            oldCmp.getStore().load()
        itemdoubletap: (cmp, index, target, record) ->
          storeComment = Ext.data.StoreManager.lookup("commentStore")

          storeComment.getProxy().setExtraParams(
            type: "Tender"
            commentable_id: record.get("id")
          )

          @getInfoPanel().setRecord(record)

          # Process accept part button
          btn = @getInfoPanel().down("#infoPanel__acceptBtn")

          contractors_hk = record.get("contractors_hk")
          current_user = IPortal.user
          company_hk = current_user.get("company_hk")
          company_id = current_user.get("company_id")

          found = false

          if company_hk
            for index of contractors_hk
              if contractors_hk[index]["id"] == company_id
                found = true
                break

          btn.show() unless found

          @getInfoPanel().show()
      "infoPanel #infoPanel__acceptBtn":
        tap: (cmp) ->
          cmp.disable()

          record = @getInfoPanel().getRecord()
          store = Ext.data.StoreManager.lookup("tenderCurrentStore")
          current_user = IPortal.user

          # Accept part in tender
          sure_string = "Вы уверены, что хотите принять участие в конкурсе №" + record.get("id") + " "
          sure_string += Ext.String.ellipsis(record.get("subject"), 50, true) + "?"

          Ext.Msg.confirm "", sure_string, (btnId) ->
            if btnId is "yes"
              contractors_hk = record.get("contractors_hk")
              contractors_hk_attrs = record.get("tender_contractors_attributes")

              contractors_hk.push
                id: current_user.get("company_id")
                name: current_user.get("company_name")

              contractors_hk_attrs.push
                company_id: current_user.get("company_id")
                _destroy: 0

              record.set("tender_contractors_attributes", contractors_hk_attrs)
              record.set("contractors_hk", contractors_hk)

              record.save(
                success: ->
                  cmp.enable()
                  cmp.hide()

                  store.load(
                    callback: ->

                      # Update stuff store
                      Ext.data.StoreManager.lookup("tenderStuffStore").load(
                        callback: ->
                          Ext.Msg.alert "", I18n.t "tenders.accepted"
                      )
                  )
                failure: ->
                  store.load(
                    callback: ->
                      cmp.enable()
                      Ext.Msg.alert "", I18n.t "tenders.not_accepted"
                  )
              , @)
