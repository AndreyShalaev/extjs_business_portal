Ext.define "IPortal.controller.ArchiveTender",
  extend: "Ext.app.Controller"

  config:
    refs:
      mainPanel:
        xtype: "mainPanel"
        selector: "mainPanel"
      infoPanel:
        xtype: "infoPanel"
        selector: "infoPanel"
      archivePanel:
        xtype: "archivePanel"
        selector: "archivePanel"
        autoCreate: true
      defaultToolbar:
        xtype: "defaultToolbar"
        selector: "defaultToolbar"
        autoCreate: true
      searchToolbar:
        xtype: "searchToolbar"
        selector: "searchToolbar"
        autoCreate: true

    control:
      archivePanel:
        activate: (cmp, that, oldCmp) ->
          @getMainPanel().add @getDefaultToolbar() if (oldCmp instanceof IPortal.view.tenders.Search) or not oldCmp
          
          @getDefaultToolbar().setTitle cmp.getTitle()

          search_value = @getDefaultToolbar().down("searchfield").getValue()

          if not (oldCmp instanceof IPortal.view.tenders.Search) and oldCmp.getStore? and search_value.length > 0
            @getDefaultToolbar().down("searchfield").setValue("")
            oldCmp.getStore().load()
        itemdoubletap: (cmp, index, target, record) ->

          storeComment = Ext.data.StoreManager.lookup("commentStore")

          storeComment.getProxy().setExtraParams(
            type: "Tender"
            commentable_id: record.get("id")
          )

          @getInfoPanel().setRecord(record)
          @getInfoPanel().show()
