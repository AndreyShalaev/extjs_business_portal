Ext.define "IPortal.controller.Search",
  extend: "Ext.app.Controller"

  config:
    refs:
      mainPanel:
        xtype: "mainPanel"
        selector: "mainPanel"
      searchPanel:
        xtype: "searchPanel"
        selector: "searchPanel"
        autoCreate: true
      defaultToolbar:
        xtype: "defaultToolbar"
        selector: "defaultToolbar"
        autoCreate: true
      searchToolbar:
        xtype: "searchToolbar"
        selector: "searchToolbar"
        autoCreate: true
      infoPanel:
        xtype: "infoPanel"
        selector: "infoPanel"

    control:
      searchPanel:
        activate: (cmp, that, oldCmp) ->
          
          search_value = @getDefaultToolbar().down("searchfield").getValue()

          @getMainPanel().remove @getDefaultToolbar(), false

          @getMainPanel().add @getSearchToolbar()

          store = Ext.data.StoreManager.lookup("tenderStore")
          total = store.getTotalCount()

          if total is 0
            @getSearchPanel().down("#searchPanel__headerToolbar").hide()
            @getSearchPanel().down("toolbar").hide()
          
          if oldCmp.getStore? and search_value.length > 0
            @getDefaultToolbar().down("searchfield").setValue("")
            oldCmp.getStore().load()  
        deactivate: ->
          @getMainPanel().remove @getSearchToolbar(), false
        itemdoubletap: (cmp, index, target, record) ->
          storeComment = Ext.data.StoreManager.lookup("commentStore")

          storeComment.getProxy().setExtraParams(
            type: "Tender"
            commentable_id: record.get("id")
          )

          @getInfoPanel().setRecord(record)

          @getInfoPanel().show()
      "searchToolbar searchfield":
        action: (cmp) ->
          that = @

          q = cmp.getValue()

          if q.length >= 3
            store = Ext.data.StoreManager.lookup("tenderStore")

            store.clearFilter(true)
            store.filter([
              property: "subject"
              value: q
            ,
              property: "taking_current"
              value: 1
            ])
            store.load(
              callback: ->
                that.getSearchPanel().down("#searchPanel__headerToolbar").show()
                that.getSearchPanel().down("toolbar").show()
            )
        clearicontap: (cmp) ->
          q = cmp.getValue()

          if q.length >= 3
            store = Ext.data.StoreManager.lookup("tenderStore")
            store.clearFilter(true)
            store.removeAll()
            @getSearchPanel().down("#searchPanel__headerToolbar").hide()
            @getSearchPanel().down("toolbar").hide()
