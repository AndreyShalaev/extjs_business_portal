Ext.define "IPortal.controller.Main",
  extend: "Ext.app.Controller"

  init: () ->
    # Load current user
    IPortal.model.CurrentUser.load "",
      scope: @
      failure: ->
        window.location.href = "/500.html"
      success: (user) ->
        window.IPortal.user = user

        Ext.get("loading").hide()

        Ext.Viewport.add(Ext.create "IPortal.view.MainPanel")

  config:
    refs:
      mainPanel:
        xtype: "mainPanel"
        selector: "mainPanel"
      infoPanel:
        xtype: "infoPanel"
        selector: "infoPanel"
        autoCreate: true
      currentPanel:
        xtype: "currentPanel"
        selector: "currentPanel"
        autoCreate: true
      stuffPanel:
        xtype: "stuffPanel"
        selector: "stuffPanel"
        autoCreate: true
      archivePanel:
        xtype: "archivePanel"
        selector: "archivePanel"
        autoCreate: true
      searchPanel:
        xtype: "searchPanel"
        selector: "searchPanel"
        autoCreate: true
      defaultToolbar:
        xtype: "defaultToolbar"
        selector: "defaultToolbar"
        autoCreate: true
      searchToolbar:
        xtype: "searchToolbar"
        selector: "searchToolbar"
        autoCreate: true

    # Control events of components
    control:
      mainPanel:
        initialize: (cmp) ->
          # Initializing views
          cmp.add [
            @getCurrentPanel(),
            @getStuffPanel(),
            @getArchivePanel() ,
            @getSearchPanel(),
            @getInfoPanel()
          ]
      "defaultToolbar #defaultToolbar__logoutBtn":
        tap: ->
          window.location.href = "/users/logout"
      "searchToolbar #searchToolbar__logoutBtn":
        tap: ->
          window.location.href = "/users/logout"
      "infoPanel #infoPanel__closeBtn":
        tap: ->
          @getInfoPanel().hide()
      # Search
      "defaultToolbar searchfield":
        action: (cmp) ->

          q = cmp.getValue()

          if q.length >= 3
            itemPanel = @getMainPanel().getActiveItem()
            store = itemPanel.getStore()

            store.clearFilter(true)
            store.filter("subject", q)
            store.load(
              callback: ->
                store.clearFilter(true)
            )
        clearicontap: (cmp) ->
          q = cmp.getValue()

          if q.length >= 3
            itemPanel = @getMainPanel().getActiveItem()
            store = itemPanel.getStore()
            store.clearFilter(true)
            store.load()

      # Send comment
      "infoPanel #infoPanel__commentBtn":
        tap: ->
          tender = @getInfoPanel().getRecord()

          store = Ext.data.StoreManager.lookup("commentStore")

          Ext.Msg.show
            title: I18n.t "comments.leave_comment"
            message: I18n.t "activerecord.models.comment.one"
            buttons: Ext.MessageBox.OKCANCEL
            multiLine: true
            promt:
              maxlength: 300
              autocapitalize: true
            fn: (btnId, value) ->
              if btnId is "ok" && value.length > 3
                record = Ext.create "IPortal.model.Comment",
                  comment: value

                store.add(record)
                store.syncWithListener ->
                  Ext.Msg.alert "", I18n.t "comments.saved"
                  # Reload comments in info panel
                  store.load(
                    callback: ->
                      jsonComments = store.getProxy().getReader().rawData

                      # Dirty Hack
                      if typeof store.data.items[0] isnt "undefined"
                        model = store.data.items[0]
                        model.phantom = false

                      comments = jsonComments.comments
                      # Yau! It`s works! ^_^
                      tender.set("comments", comments)
                  )
                , false
              else if btnId is "ok"
                Ext.Msg.alert "", I18n.t "comments.errors.length"
      # Send Email
      "infoPanel #infoPanel__emailBtn":
        tap: ->
          that = @
          emailWindow = ->
            Ext.Msg.show
              title: I18n.t "tenders.send_email"
              message: I18n.t "email"
              buttons: Ext.MessageBox.OKCANCEL
              multiLine: true
              promt:
                maxlength: 255
                autocapitalize: false
              fn: (btnId, value) ->
                if btnId is "ok" && /^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]+$/ig.test(value)
                  tender = that.getInfoPanel().getRecord()
                  email = value

                  Ext.Msg.show
                    title: I18n.t "activerecord.models.comment.one"
                    buttons: Ext.MessageBox.OKCANCEL
                    multiLine: true
                    promt:
                      maxlength: 255
                      autocapitalize: false
                    fn: (btnId, value) ->
                      if btnId is "ok"
                        Ext.Ajax.request
                          url: "/api/tenders/" + tender.get("id") + "/sending"
                          method: "POST"
                          params:
                            comment: value
                            email: email
                          success: ->
                            Ext.Msg.alert "", I18n.t "tenders.sended"
                          failure: ->
                            Ext.Msg.show
                              title: ""
                              message: I18n.t "tenders.not_sended"
                              buttons: Ext.MessageBox.OK
                              fn: ->
                                emailWindow()

                else if btnId is "ok"
                  Ext.Msg.show
                    title: ""
                    message: I18n.t "email_invalid"
                    buttons: Ext.MessageBox.OK
                    fn: ->
                      emailWindow()
          emailWindow()







