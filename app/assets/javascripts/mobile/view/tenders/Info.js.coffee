Ext.define "IPortal.view.tenders.Info",
  extend: "Ext.Panel"

  xtype: "infoPanel"

  config:
    showAnimation: "fadeIn"
    hideAnimation: "fadeOut"
    modal: true
    scrollable:true,
    layout:
      type: "vbox"
    hidden: true
    hideOnMaskTap: true
    width: "90%"
    height: "90%"
    centered: true

    items: [
      xtype: "toolbar"
      docked: "top"
      id: "infoPanel__toolbar"
      items: [
        xtype: "button"
        text: I18n.t "tenders.send_email"
        iconCls: "action"
        id: "infoPanel__emailBtn"
      ,
        xtype: "button"
        text: I18n.t "comments.leave_comment"
        id: "infoPanel__commentBtn"
        iconCls: "compose"
      ,
        xtype: "button"
        id: "infoPanel__unacceptBtn"
        text: I18n.t "tenders.unaccept_part"
        hidden: true
        iconCls: "team"
      ,
        xtype: "button"
        id: "infoPanel__acceptBtn"
        text: I18n.t "tenders.accept_part"
        hidden: true
        iconCls: "team"
      ,
        xtype: "spacer"
      ,
        xtype: "button"
        #text: I18n.t "close"
        iconCls: "delete"
        id: "infoPanel__closeBtn"
      ]
    ]

    tpl: [
      '<h1 class="infoPanel__title">Конкурс №{id}</h1>',
      '<dl class="infoPanel__infoList">',
      '<tpl for=".">',
      '<dt>{[ I18n.t("activerecord.attributes.tender.subject") ]}</dt><dd>{subject}</dd>',
      '<tpl if="customer_name"><dt>{[ I18n.t("activerecord.attributes.tender.customer") ]}</dt><dd>{customer_name}</dd></tpl>',
      '<dt>{[ I18n.t("activerecord.attributes.tender.price_limit") ]}</dt><dd>{[ Number(values.price_limit).formatMoney(2, ",", " ") ]}</dd>',
      '<dt>{[ I18n.t("activerecord.attributes.tender.declared_at") ]}</dt><dd>{[ I18n.strftime(values.declared_at, "%d-%b-%Y") ]}</dd>',
      '<dt>{[ I18n.t("activerecord.attributes.tender.opening_at") ]}</dt><dd>{[ I18n.strftime(values.opening_at, "%d-%b-%Y") ]}</dd>',
      '<tpl if="turnaround_time"><dt>{[ I18n.t("activerecord.attributes.tender.turnaround_time") ]}</dt><dd>{turnaround_time}</dd></tpl>',
      '<tpl if="contractors_hk.length &gt; 0"><dt>{[ I18n.t("activerecord.attributes.tender.contractors_hk") ]}</dt><dd>{[ values.contractors_hk.map(function (item) { return item.name; }).join(";<br>") ]}</dd></tpl>',
      '<tpl if="cost_at"><dt>{[ I18n.t("activerecord.attributes.tender.cost_at") ]}</dt><dd>{[ I18n.strftime(values.cost_at, "%d-%b-%Y") ]}</dd></tpl>',
      '<tpl if="price_cost"><dt>{[ I18n.t("activerecord.attributes.tender.price_cost") ]}</dt><dd>{[ Number(values.price_cost).formatMoney(2, ",", " ") ]}</dd></tpl>',
      '<tpl if="price_our"><dt>{[ I18n.t("activerecord.attributes.tender.price_our") ]}</dt><dd>{[ Number(values.price_our).formatMoney(2, ",", " ") ]}</dd></tpl>',
      '<tpl if="parties && parties.length &gt; 0"><dt>{[ I18n.t("activerecord.attributes.tender.parties") ]}</dt>',
      '<dd><table style="width: 100% !important;"><thead><tr><th style="padding: 3px;border-bottom: 1px solid #ccc;"><b>{[ I18n.t("activerecord.models.company.one") ]}</b></th>',
      '<th style="padding: 3px;text-align: right;border-bottom: 1px solid #ccc;width: 200px;"><b>{[ I18n.t("activerecord.attributes.tender.price_cost") ]}</b></th></tr></thead>',
      '<tbody>{[ values.parties.map(function (item) {
          var part_price, party_name;

          if(Number(item.price_finish) > 0) {
            part_price = Number(item.price_finish).formatMoney(2, ",", " ");
          } else if(Number(item.price) > 0) {
            part_price = Number(item.price).formatMoney(2, ",", " ");
          } else {
            part_price = "";
          }

          if(values.winner_id == item.company.id) {
            party_name = "<span style=\'color:green;\'>" + item.company.name + "</span>";
            part_price = "<span style=\'color:green;\'>" + part_price + "</span>";
          } else {
            party_name = item.company.name;
          }

          return "<tr><td style=\'padding: 3px;border-bottom: 1px solid #ccc;\'>" + party_name + "</td><td style=\'padding: 3px;text-align: right;border-bottom: 1px solid #ccc;\'>" + part_price + "</td></tr>";
        }).join("") ]}</tbody></table></dd></tpl>'
      '<tpl if="comments.length &gt; 0"><dt>{[ I18n.t("activerecord.attributes.tender.comments") ]}</dt>',
      '<dd>{[ values.comments.map(function (item) {
                var comment_date = new Date(item.created_at);
                var d = comment_date.getDate();
                d = (d < 10) ? "0" + d : d;
                var m = comment_date.getMonth() + 1;
                m = (m < 10) ? "0" + m : m;
                var h = comment_date.getHours();
                h = (h < 10) ? "0" + h : h;
                var mn = comment_date.getMinutes();
                mn = (mn < 10) ? "0" + mn : mn;
                var comment = "<b>" + d + "." + m + "." + comment_date.getFullYear() + " " + h + ":" + mn + " - " + item.user.fullname + "</b><br><br>" + item.comment;
                return comment;
              }).join("<hr>") ]}</dd></tpl>',
      '</tpl>',
      '</dl>'
    ]
