Ext.define "IPortal.view.tenders.Current",
  extend: "IPortal.ux.grid.List"

  xtype: "currentPanel"

  config:
    fullscreen: true

    title: I18n.t "tenders.currents"

    iconCls: "star"

    store: "tenderCurrentStore"

    loadingText: I18n.t("loading")


    features: [
      ftype: "IPortal.ux.grid.feature.Sorter"
      launchFn: "initialize"
    ,
      ftype: "IPortal.ux.grid.feature.Paging"
      launchFn: "initialize"
      goToButton:
        text: I18n.t "paging.go_to_page"
      backButton:
        text: I18n.t "paging.preview"
      forwardButton:
        text: I18n.t "paging.next"
      pageListTpl: I18n.t("page_number",
        number: "{page}"
      )
    ]

    columns: [
      header: "#"
      dataIndex: "id"
      width: "4%"
    ,
      header: I18n.t "activerecord.attributes.tender.customer"
      dataIndex: "customer_name"
      width: "15%"
      sortable: false
    ,
      header: I18n.t "activerecord.attributes.tender.subject"
      dataIndex: "subject"
      width: "30%"
    ,
      header: I18n.t "activerecord.attributes.tender.price_limit"
      dataIndex: "price_limit"
      width: "12%"
      renderer: (value) ->
        value = Number value
        if value > 0
          return value.formatMoney 2, ",", " "
        else
          return ""
    ,
      header: I18n.t "activerecord.attributes.tender.declared_at"
      dataIndex: "declared_at"
      width: "12%"
      renderer: (value) ->
        if value
          I18n.strftime(value, "%d-%b-%Y")
        else
          return ""
    ,
      header: I18n.t "activerecord.attributes.tender.opening_at"
      dataIndex: "opening_at"
      width: "12%"
      renderer: (value) ->
        if value
          I18n.strftime(value, "%d-%b-%Y")
        else
          return ""
    ,
      header: I18n.t "activerecord.attributes.tender.contractors_hk"
      dataIndex: "contractors_hk"
      width: "15%"
      sortable: false
      renderer: (value) ->
        return value.map((item) ->
          item.name
        ).join(";<br>")
    ]


