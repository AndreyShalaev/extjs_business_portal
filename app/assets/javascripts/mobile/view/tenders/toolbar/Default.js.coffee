Ext.define "IPortal.view.tenders.toolbar.Default",
  extend: "Ext.Toolbar"

  xtype: "defaultToolbar"

  config:
    docked: "top"

    items: [
      xtype: "searchfield"
      placeHolder: I18n.t "search"
    ,
      xtype: "spacer"
      flex: 1
    ,
      xtype: "togglefield"
      hidden: true
      cls: "defaultToolbar__switcher"
      label: "Все конкурсы"
    ,
      text: I18n.t "logout"
      id: "defaultToolbar__logoutBtn"
    ]
