Ext.define "IPortal.view.tenders.toolbar.Search",
  extend: "Ext.Toolbar"

  xtype: "searchToolbar"

  config:
    docked: "top"

    items: [
      xtype: "searchfield"
      placeHolder: I18n.t "search"
      flex: 1
    ,
      text: I18n.t "logout"
      id: "searchToolbar__logoutBtn"
    ]
