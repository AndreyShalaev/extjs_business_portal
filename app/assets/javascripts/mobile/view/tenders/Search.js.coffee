Ext.define "IPortal.view.tenders.Search",
  extend: "IPortal.ux.grid.List"

  xtype: "searchPanel"

  config:
    title: "Поиск"

    iconCls: "search"

    fullscreen: true

    loadingText : I18n.t "loading"

    store: "tenderStore"

    features: [
      ftype: "IPortal.ux.grid.feature.Sorter"
      launchFn: "initialize"
    ,
      ftype: "IPortal.ux.grid.feature.Paging"
      launchFn: "initialize"
      goToButton:
        text: I18n.t "paging.go_to_page"
      backButton:
        text: I18n.t "paging.preview"
      forwardButton:
        text: I18n.t "paging.next"
      pageListTpl: I18n.t("page_number",
        number: "{page}"
      )
    ]

    header:
      hidden: true
      xtype: "toolbar"
      id: "searchPanel__headerToolbar"
      docked: "top"
      cls: "x-grid-header"

    columns: [
      header: "#"
      dataIndex: "id"
      width: "4%"
    ,
      header: "Тип заявки"
      width: "10%"
      sortable: false
      renderer: (value, values) ->
        is_archive = values.archive
        opening_at = new Date values.opening_at
        count_contractors = values.contractors_hk.length
        current_time = new Date()

        if count_contractors > 0 and not is_archive
          return "В работе"
        else if opening_at.getTime() <= current_time.getTime() or is_archive
          return "Архив"
        else
          return "Текущие"

    ,
      header: I18n.t "activerecord.attributes.tender.customer"
      dataIndex: "customer_name"
      width: "12%"
      sortable: false
    ,
      header: I18n.t "activerecord.attributes.tender.subject"
      dataIndex: "subject"
      width: "25%"
    ,
      header: I18n.t "activerecord.attributes.tender.price_limit"
      dataIndex: "price_limit"
      width: "12%"
      renderer: (value) ->
        value = Number value
        if value > 0
          return value.formatMoney 2, ",", " "
        else
          return ""
    ,
      header: I18n.t "activerecord.attributes.tender.opening_at"
      dataIndex: "opening_at"
      width: "12%"
      renderer: (value) ->
        if value
          I18n.strftime(value, "%d-%b-%Y")
        else
          return ""
    ,
      header: I18n.t "activerecord.attributes.tender.opening_at"
      dataIndex: "opening_at"
      width: "12%"
      renderer: (value) ->
        if value
          I18n.strftime(value, "%d-%b-%Y")
        else
          return ""
    ,
      header: I18n.t "activerecord.attributes.tender.contractors_hk"
      dataIndex: "contractors_hk"
      width: "13%"
      sortable: false
      renderer: (value) ->
        return value.map((item) ->
          item.name
        ).join(";<br>")
    ]


