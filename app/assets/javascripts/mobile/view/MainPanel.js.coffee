Ext.define "IPortal.view.MainPanel",
  extend: "Ext.tab.Panel"

  xtype: "mainPanel"

  config:
    fullscreen: true
    tabBarPosition: "bottom"
