Ext.define "IPortal.model.Tender",
  extend: "Ext.data.Model"

  config:
    useCache: false
    fields: [
      name: "id"
      type: "int"
      persist: false
    ,
      name: "customer_id"
      useNull: true
      type: "int"
    ,
      name: "customer_name"
      type: "string"
    ,
      name: "subject"
      type: "string"
    ,
      name: "price_limit"
      useNull: true
      type: "float"
    ,
      name: "declared_at"
      type: "date"
      dateFormat: "Y-m-d"
    ,
      name: "opening_at"
      type: "date"
      dateFormat: "Y-m-d"
    ,
      name: "turnaround_time"
      type: "string"
      useNull: true
    ,
      name: "contractors_hk"
    ,
      name: "tender_contractors_attributes"
    ,
      name: "comment"
      mapping: "last_comment.comment"
      useNull: true
    ,
      name: "comment_user"
      mapping: "last_comment.user.fullname"
      useNull: true
    ,
      name: "comment_date"
      mapping: "last_comment.created_at"
      type: "date"
      useNull: true
      dateFormat: "Y-m-d"
    ,
      name: "archive"
      type: "bool"
      defaultValue: false
    ,
      name: "cost_at"
      type: "date"
      dateFormat: "Y-m-d"
      useNull: true
    ,
      name: "price_cost"
      useNull: true
      type: "float"
    ,
      name: "price_our"
      useNull: true
      type: "float"
    ,
      name: "comments"
    ,
      name: "parties"
      useNull: true
      persist: false
    ,
      name: "winner_id"
      type: "int"
      useNull: true
    ]

    proxy:
      type: "rest"
      url: "/api/tenders"
      writer:
        type: "json"
        root: "tender"
