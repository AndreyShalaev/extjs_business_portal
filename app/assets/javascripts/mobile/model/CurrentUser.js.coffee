Ext.define "IPortal.model.CurrentUser",
  extend: "Ext.data.Model"

  config:
    fields: [
      name: "id"
    ,
      name: "fullname"
    ,
      name: "username"
    ,
      name: "company_id"
      mapping: "company.id"
      useNull: true
    ,
      name: "company_hk"
      type: "bool"
      mapping: "company.hk"
    ,
      name: "company_name"
      mapping: "company.name"
      useNull: true
    ,
      name: "permissions"
    ,
      name: "admin"
      type: "bool"
    ]

    proxy:
      type: "rest"
      url: "/api/profile"
      reader:
        type: "json"
        rootProperty: "user"
