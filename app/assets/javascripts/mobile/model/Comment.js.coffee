Ext.define "IPortal.model.Comment",
  extend: "Ext.data.Model"

  config:
    fields: [
      name: "id"
    ,
      name: "comment"
    ,
      name: "created_at"
    ,
      name: "user_id"
      mapping: "user.id"
    ,
      name: "user_name"
      mapping: "user.fullname"
    ,
      name: "commentable_id"
    ]
