
Number::formatMoney = (c, d, t) ->
  n = this
  c = (if isNaN(c = Math.abs(c)) then 2 else c)
  d = (if d is `undefined` then "." else d)
  t = (if t is `undefined` then "," else t)
  s = (if n < 0 then "-" else "")
  i = parseInt(n = Math.abs(+n or 0).toFixed(c)) + ""
  j = (if (j = i.length) > 3 then j % 3 else 0)
  s + ((if j then i.substr(0, j) + t else "")) + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + ((if c then d + Math.abs(n - i).toFixed(c).slice(2) else ""))

# Csrf
Ext.Ajax.on("beforerequest", (conn, options) ->
  unless /^http:.*/.test(options.url) or /^https:.*/.test(options.url)
    csrfToken = Ext.select("meta[name='csrf-token']").elements[0].getAttribute("content")
    if typeof options.headers is "undefined"
      options.headers =
        "X-CSRF-Token": csrfToken
    else
      Ext.Object.merge options.headers,
        "X-CSRF-Token": csrfToken
, @)

Ext.Ajax.on("requestexception", (conn, response) ->
  window.location.href = "/" if response.status is 401
, @)

Ext.override(Ext.data.Store,
  syncWithListener: (callback, syncMethod) ->
    @on("write", callback, @,
      single: true
    )

    syncResult = if syncMethod
      syncMethod.apply(@)
    else
      @sync()

    if syncResult.added.length is 0 and syncResult.updated.length is 0 and syncResult.removed.length is 0
      @un("write", callback, @,
        single: true
      )
      callback(@)

    syncResult
)


