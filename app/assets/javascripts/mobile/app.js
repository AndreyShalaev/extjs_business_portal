//= require ./overrides

//= require ./ux/manifest

//= require_tree ./model
//= require_tree ./store
//= require_tree ./view
//= require_tree ./controller
//= require_self

Ext.Loader.setConfig({ enabled: false });

Ext.application({
    name: "IPortal",
    controllers: [
        "Main",
        "CurrentTender",
        "StuffTender",
        "ArchiveTender",
        "Search"
    ],
    // It allow autocreate needed stores
    stores: [
        "IPortal.store.tenders.Current",
        "IPortal.store.tenders.Stuff",
        "IPortal.store.tenders.Archive",
        "IPortal.store.tenders.Tender",
        "IPortal.store.Comment"
    ]
});
