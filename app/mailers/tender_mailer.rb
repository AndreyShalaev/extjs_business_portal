class TenderMailer < ActionMailer::Base
  include Resque::Mailer

  default :from => 'kb-7.net@yandex.ru'

  def item_email(email, user_id, tender_id, comment)
    @tender = Tender.find(tender_id)
    @comment = comment
    @user = User.find(user_id)
    mail(:to => email, :subject => "#{I18n.t("activerecord.models.tender.one")} ##{@tender.id}: #{@tender.subject}")
  end
end
