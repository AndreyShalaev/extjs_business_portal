# encoding: utf-8

module ApiAudited
	def self.included(klass)
		klass.instance_eval do

			delegate :fullname, :to => :user, :prefix => true, :allow_nil => true

			acts_as_api

			api_accessible :extjs do |t|
				t.add :id
				t.add :audited_changes
				t.add lambda { |a|
					a.user_fullname
				}, :as => :user
				t.add :created_at
				t.add :version
				t.add lambda {|a|
					I18n.t(a.action)
				}, :as => :action
				t.add :auditable_id
				#t.add :auditable
				t.add :remote_address
				t.add :subject, :if => lambda { |a| a.auditable.nil? || a.auditable.kind_of?(Tender) }
				t.add :type_view, :if => lambda { |a| a.auditable.nil? || a.auditable.kind_of?(Tender) }
			end
		end
	end

	def type_view
		if self.auditable.kind_of?(Tender)
			tender = self.auditable

			if tender.tender_contractors_count > 0 && !tender.archive?
				'В работе'
			elsif tender.opening_at <= Tender.due_day || tender.archive?
				'Архив'
			else
				'Текущие'
			end
		else
			'Был удален'
		end
	end

	def subject

		changes = self.audited_changes
		subject = changes.has_key?('subject') ? changes['subject'] : (self.auditable.respond_to?(:subject) ? self.auditable.subject : 'Был удален')

		if subject.kind_of?(Array)
			subject = "<span style=\"color: red;\">-- #{subject[0]}</span><br>------------<br><span style=\"color: green;\">++ #{subject[1]}</span>"
		end

		subject
	end
end

Audited::Adapters::ActiveRecord::Audit.send :include, ApiAudited