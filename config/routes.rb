Ip::Application.routes.draw do

  devise_for  :users,
              :controllers => {
                :sessions => 'sessions',
                :passwords => 'passwords'
              },
              :path_names => {
                :sign_in => 'login',
                :sign_out => 'logout'
              }

  namespace :api, :defaults => { :format => :json } do
    resources :companies, :except => [ :new, :edit ] do
      collection do
        get :customers
        get :contractors
        get :winners
        get :search
      end
      resources :users, :only => [ :create, :update, :destroy ]
    end

    resources :users, :only => [ :index ]

    resources :permissions, :only => [ :index ]

    # Events journal
    scope '/tenders' do
      resources :audits, :controller => 'audit_tenders', :only => [ :index ]
    end

    resources :tenders, :except => [ :new, :edit ] do
      collection do
        get :currents
        get :stuffs
        get :archives
      end
      resources :attachments, :only => [ :index, :create, :destroy ]
      member do
        post :sending
      end
      resources :parties, :only => [ :index, :create, :update, :destroy ]
    end
    resources :provisions, :only => [ :index, :create ]
    resources :regions, :only => [ :index ]

    resources :comments, :only => [ :index, :create, :destroy ]

    resource :profile, :only => [ :show ]


  end

  authenticated :user do
    root :to => 'home#index', :as => :authenticated_root
  end

  root :to => redirect('/users/login')
end
