# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  username               :string(140)      not null
#  encrypted_password     :string(255)      not null
#  email                  :string(255)
#  post                   :string(140)
#  phone                  :string(20)
#  own_phone              :string(20)
#  note                   :text
#  company_id             :integer
#  name                   :string(140)      not null
#  role_id                :integer
#  middlename             :string(140)      not null
#  surname                :string(140)      not null
#  reset_password_token   :string(255)
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0)
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :string(255)
#  last_sign_in_ip        :string(255)
#  failed_attempts        :integer          default(0)
#  unlock_token           :string(255)
#  locked_at              :datetime
#  created_at             :datetime
#  updated_at             :datetime
#  legacy_password        :boolean          default(FALSE), not null
#  deleted_at             :datetime
#

require 'spec_helper'

describe User do
  let!(:user) { FactoryGirl.build(:user) }

  it 'create new instance given valid parameters' do
    user.save
  end

  it 'invalid without name' do
    user.name = ''
    user.should_not be_valid
  end

  it 'invalid without surname' do
    user.surname = ''
    user.should_not be_valid
  end

  it 'invalid without middlename' do
    user.middlename = ''
    user.should_not be_valid
  end

  it 'valid given valid phone' do
    user.phone = '89126576039'
    user.should be_valid

    user.phone = '3325852'
    user.should be_valid

    user.phone = '(343)3325852'
    user.should be_valid
  end

  it 'invalid given invalid phone' do
    user.phone = '8904'
    user.should_not be_valid
  end

  it 'invalid without company' do
    user.company = nil
    user.should_not be_valid
  end

  it 'save with empty permission_attributes' do
    user.user_permissions_attributes.should eql([])
  end
end
