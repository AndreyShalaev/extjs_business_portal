# == Schema Information
#
# Table name: companies
#
#  id         :integer          not null, primary key
#  name       :string(255)      not null
#  short_name :string(140)
#  address    :string(255)
#  site       :string(255)
#  email      :string(255)
#  phone      :string(20)
#  type       :string(15)
#  area_link  :string(255)
#  hk         :boolean          default(FALSE), not null
#  region_id  :integer
#  activities :text
#  parent_id  :integer
#  lft        :integer
#  rgt        :integer
#  depth      :integer
#  ts_name    :string(255)      default(""), not null
#  inn        :integer
#  org_level  :integer
#  deleted    :boolean          default(FALSE), not null
#

require 'spec_helper'

describe Company do
  let!(:company) { FactoryGirl.build(:company) }

  it 'should create a new instance given valid parameters' do
    company.save
  end
end
