# == Schema Information
#
# Table name: parties
#
#  id           :integer          not null, primary key
#  tender_id    :integer
#  company_id   :integer
#  price        :decimal(26, 2)
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  price_finish :decimal(26, 2)
#

require 'spec_helper'

describe Party do

  let!(:company) { FactoryGirl.create(:company) }
  let!(:tender) { FactoryGirl.create(:tender) }

  it 'should create new instance given valid parameters' do
    Party.create(:company => company, :tender => tender, :price => 90000.50)
  end

  it 'should assign new winner' do
    Party.create!(:company => company, :tender => tender, :price => 90000.50, :winner => true)
    tender.winner.should be_eql(company)

    company_2 = FactoryGirl.create(:company)
    Party.create!(:company => company_2, :tender => tender, :price => 90000.50, :winner => true)
    tender.winner.should be_eql(company_2)
  end

  it 'must be only one winner' do
    company_2 = FactoryGirl.create(:company)
    company_3 = FactoryGirl.create(:company)

    part = Party.create!(:company => company, :tender => tender, :price => 90001.50, :winner => true)
    part_2 = Party.create!(:company => company_2, :tender => tender, :price => 90002.50, :winner => true)
    part_3 = Party.create!(:company => company_3, :tender => tender, :price => 90003.50, :winner => true)

    part.should_not be_winner
    part_3.should be_winner
    tender.winner.should be_eql(company_3)
  end

  it '`s tender has no winners' do
    company_2 = FactoryGirl.create(:company)

    part = Party.create!(:company => company, :tender => tender, :price => 90001.50, :winner => true)
    part_2 = Party.create!(:company => company_2, :tender => tender, :price => 90002.50, :winner => true)

    part.update_attributes!(:winner => false)

    tender.winner.should be_eql(company_2)

    part_2.update_attributes!(:winner => false)
    tender.winner.should be_eql(nil)
  end

  describe 'when destroyed' do
    it 'his tender has no winner' do
      part = Party.create!(:company => company, :tender => tender, :price => 90001.50, :winner => true)
      part.destroy

      tender.winner.should be_nil
    end
  end
end
