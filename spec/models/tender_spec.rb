# == Schema Information
#
# Table name: tenders
#
#  id                       :integer          not null, primary key
#  customer_id              :integer
#  provision_id             :integer
#  winner_id                :integer
#  subject                  :text             not null
#  price_limit              :decimal(26, 2)
#  turnaround_time          :string(140)
#  declared_at              :datetime
#  opening_at               :datetime
#  documentation            :string(140)
#  estimates                :string(140)
#  price_provision          :decimal(26, 2)
#  requirements             :string(255)
#  link                     :string(255)
#  price_cost               :decimal(26, 2)
#  price_our                :decimal(26, 2)
#  cost_at                  :datetime
#  cost_at_green            :boolean          default(FALSE), not null
#  gpr_at                   :datetime
#  gpr_at_green             :boolean          default(FALSE), not null
#  estimates_at             :datetime
#  estimates_at_green       :boolean          default(FALSE), not null
#  ready_at                 :datetime
#  ready_at_green           :boolean          default(FALSE), not null
#  price_winner             :decimal(26, 2)
#  created_at               :datetime         not null
#  updated_at               :datetime         not null
#  archive                  :boolean          default(FALSE), not null
#  tender_contractors_count :integer          default(0), not null
#  fts_subject              :tsvector
#

require 'spec_helper'

describe Tender do

  let!(:company) { FactoryGirl.create(:company) }
  let!(:tender) { FactoryGirl.build(:tender) }

  it 'create with given valid parameters without contractors_hk' do
    tender.save
  end

  describe 'with contractors_hk nested attributes' do
    let(:params) { { :tender_contractors_attributes => [{ :company_id => company.id, :_destroy => 0}] } }

    before(:each) do
      tender.save!
    end

    it 'should update contractors_hk attributes' do
      tender.update_attributes!(params)
      tender.reload.contractors_hk.should include(company.reload)
    end

    it 'should destroy contractor_hk relation' do
      tender.update_attributes!(params)

      tender.update_attributes!(params.merge(:tender_contractors_attributes => [{ :id => company.id, :_destroy => 1}]))
      tender.contractors_hk(true).should_not include(company)
    end
  end

  # Представления конкурсов
  # +currents+ текущие
  # +stuffs+ в работе
  # +archives+ в архиве
  describe 'views' do
    let(:params) { { :tender_contractors_attributes => [{ :company_id => company.id, :_destroy => 0}] } }

    before(:each) do
      tender.save!
    end

    it 'place in currents' do
      # Вскрытие конвертов только через три дня
      # Подрядчиков еще нет
      tender.update_attributes!(:opening_at => Time.now + 3.days)

      Tender.currents.should include(tender)
      Tender.stuffs.should_not include(tender)
      Tender.archives.should_not include(tender)
    end

    it 'place in currents and stuffs' do
      # Вскрытие конвертов только через три дня
      # Подрядчик имеется
      tender.update_attributes!(params.merge(:opening_at => Time.now + 3.days))

      Tender.currents.should include(tender)
      Tender.stuffs.should include(tender)
      Tender.archives.should_not include(tender)
    end

    it 'place in stuffs and archives' do
      # Вскрытие конвертов произошло
      # Подрядчик имеется
      tender.update_attributes!(params.merge(:opening_at => Time.now - 2.days))

      Tender.currents.should_not include(tender)
      Tender.stuffs.should include(tender)
      Tender.archives.should include(tender)
    end

    it 'hand move to archive' do
      tender.update_attributes!(params.merge(:archive => true))

      Tender.currents.should_not include(tender)
      Tender.stuffs.should_not include(tender)
      Tender.archives.should include(tender)
    end
  end

  describe 'audited model' do

    before(:each) do
      tender.save!
    end

    it 'save track changes' do
      subject_before = tender.subject

      tender.update_attributes!(:subject => 'Test audited')

      audited = Audited.audit_class.where(:auditable_type => 'Tender', :action => 'update').first

      audited.audited_changes.should eql({'subject' => [subject_before, 'Test audited']})
    end

    # Тестируем, что внедрение acts_as_api прошло успешно в модель audited
    it 'respond to acts_as_api' do
      tender.update_attributes!(:subject => 'Test audited')

      audited = Audited.audit_class.where(:auditable_type => 'Tender', :action => 'update').first

      response = audited.as_api_response(:extjs)

      response.should be_kind_of(Hash)
    end
  end

  # Расчет сроков предоставления
  describe 'time limits' do
    let(:original_date) { Date.new(2013, 7, 25) }

    before(:each) do
      tender.opening_at = original_date
      tender.save!
    end

    it 'count cost_at' do
      tender.cost_at.should eql(Date.new(2013, 7, 18))
    end

    it 'count gpr_at' do
      tender.gpr_at.should eql(Date.new(2013, 7, 22))
    end

    it 'count estimates_at' do
      tender.estimates_at.should eql(Date.new(2013, 7, 22))
    end

    it 'count ready_at' do
      tender.ready_at.should eql(Date.new(2013, 7, 24))
    end
  end
end
