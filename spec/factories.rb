FactoryGirl.define do
  factory :company do
    sequence(:name) { |n| "Test company #{n}" }
    short_name nil
    address nil
    site nil
    email nil
    phone nil
    type nil
    area_link nil
    hk false
    region_id nil
    activities nil
    parent_id nil
    inn nil
    org_level nil
  end

  factory :customer do
    sequence(:name) { |n| "Test company #{n}" }
  end

  factory :tender do
    sequence(:subject) { |n| "Test tender #{n}" }
    opening_at Time.now
    declared_at Time.now - 12.days
    association :customer, :factory => :customer, :strategy => :build
  end

  factory :user do
    sequence(:username) { |n| "User ##{n}" }
    password '12345678'
    password_confirmation '12345678'
    #sequence(:email) { |n| "user#{n}@example.com" }
    sequence(:name) { |n| "Ivan #{n}" }
    sequence(:surname) { |n| "Petrovich #{n}" }
    sequence(:middlename) { |n| "Ivanov #{n}" }
    association :company, :factory => :company, :strategy => :build
  end
end
